shapely
scipy
# PyQt4
# PyYAML
yamlordereddictloader
matplotlib
# Add your requirements here like:
numpy
pandas
pint
netCDF4
pandas
seaborn
progressbar2
hmc_utils>=0.3.4
hmc_marine>=0.1.4
mlab_mdfreader>=0.3.5
LatLon>=1.1.7
