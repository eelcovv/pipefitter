#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pytest
import logging
import os
import pickle

import pandas as pd
from hmc_utils.misc import (create_logger, get_logger)
from numpy import array
from numpy.testing import (assert_almost_equal, assert_equal)
from pandas.testing import assert_frame_equal

import hmc_marine.data_readers as dr

from fatigue_monitoring import (Q_, ureg)
from fatigue_monitoring.fms_utilities import set_default_dimension

DATA_DIR = "data"

FUGRO_FILE_NAME = "Fugro_I0450_20170510_06.csv"
WRB_FILE_NAME = "example 2017}2017-05-10T00h01Z.spt"

logger = create_logger(console_log_level=logging.CRITICAL)


def test_pipe_fitter():
    logger = get_logger(__name__)

