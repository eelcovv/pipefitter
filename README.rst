=================
PipeFitter
=================


Create sequences of series of pipe ends


Description
===========

This program allow to read a series of scanned pipe profiles and sort and match them


Note
====

This project has been set up using PyScaffold 2.5.6. For details and usage
information on PyScaffold see http://pyscaffold.readthedocs.org/.
