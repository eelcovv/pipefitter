=========
Changelog
=========

Version 0.3.2
=============
- Release version by Eelco van Vliet, as transferred to PipeLay and PTL
- Shift feature is working
- Pipe Manipulate feature is working
- Transfer to pyqt5 and python 3.6 established

Version 0.2.x
=============
- Update version tag
- Tested build_installer

Version 0.1.2
=============
- Updated for the translation of pipe ends
- Included visualisation of phi range where fit is ok

Version 0.1
=============
- Initialization of package based on original PipeFitter code

