__author__ = 'eelcovv'
import sys
from cx_Freeze import setup, Executable

sys.setrecursionlimit(5000)
sys.argv.append('build')

from cx_Freeze import setup, Executable
#from gluon.import_all import base_modules, contributed_modules
#from gluon.fileutils import readlines_file
import glob

# get the isnstallation folder
import site;
installation_folder = site.getsitepackages()[0]
from glob import glob
import fnmatch
import os
import shutil
import sys
import re
import os



# Dependencies are automatically detected, but it might need fine tuning.
build_exe_options = dict(
    compressed=True,
    includes=["shapely"],
    packages=["os", "shapely"],
    excludes=["tkinter"],
#     include_files=[
#         'geos.dll',
#         'geos_c.dll'
#     ]
)

# GUI applications require a different base on Windows (the default is for a
# console application).
base = None
if sys.platform == "win32":
    base = "Win32GUI"

setup(name="PipeFitter",
      version="1.0",
      description="A GUI for Pipe Fitting",
      options=dict(build_exe=build_exe_options),
      executables=[Executable("PipeFitter.py", base=base)])
