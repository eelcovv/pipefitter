__author__ = 'eelcovv'

"""
The functions and classes required by pipeslicer
"""
import logging as log
import os
import re
from collections import OrderedDict

import numpy as np
import pandas as pd

from pipe_fitter.pyqt_import import *

from scipy import stats
from scipy.signal import resample
from shapely.geometry import (Polygon, Point)
from shapely.geometry.polygon import LinearRing

import pipe_fitter
from hmc_utils.coordinate_transformations import polar_to_cartesian
from hmc_utils.misc import (Timer, PackageInfo)
from pipe_fitter.pipe_fitter_utilities import (get_slice, shift_radial_profile)

package_info = PackageInfo(pipe_fitter)
__version__ = package_info.package_version

INFO_SHEET_NAME = "Information"
STAT_SHEET_NAME = "Statistics"


class FitPipesAnalyse(QtCore.QObject):
    # define all the signals
    update_status_bar_message = pyqtSignal(str)
    progress_changed = pyqtSignal(int, int, str)
    display_finished = pyqtSignal()
    start_import_data = pyqtSignal(pd.DataFrame)
    stop_batch_execute = pyqtSignal()
    start_generate_data = pyqtSignal()
    stop_generate_data = pyqtSignal()

    def __init__(self,
                 fit_threshold=1.0,
                 parent=None,
                 time_zone="UTC",
                 ):
        super(FitPipesAnalyse, self).__init__(parent)

        self.logger = log.getLogger(__name__)

        self.time_zone = time_zone

        self.pipe_index = None
        self.pipe_info_index = None
        self.sorted_index_list = None
        self.pipe_data_df = None
        self.pipe_sampled_df = None
        self.pipe_fit_matrix_df = None
        self.have_pipe_fit_matrix = False
        self.have_sampled_data = False
        self.pipe_info_df = None
        self.df_info = None

        self.pipe_info_df_in_pool = None
        self.pipe_info_df_sorted_list = None

        # can be used to set the front to true (if side index ==0) or false (if side index==1)
        self.front_flag_list = [True, False]

        # here I impose the side reference to be A and B, however. In case you want to make this
        # name generic this other_side_name should be updated after reading the pipe files
        self.other_side_name = dict([("A", "B"), ("B", "A")])

        self.n_pipes = 0

        self.exclude_phi_range_weld = 0  # make sure that a range around the weld is excluded
        self.exclude_phi_range_six_oclock = 0  # make sure that the weld can not be at six o'clock
        self.optimize_phi_at_translations = 0  # Calculate optimal phi for translations as well
        self.n_shift_points = 5  # The number of discretion over phi interpolation
        self.d_shift_matrix = None  # used to store the matrix of d shift used for LSF
        self.shift_yy = None  # store the shift around COG in y direction
        self.shift_zz = None  # store the shift around COG in z direction
        self.min_hilo_matrix = np.full(shape=(self.n_shift_points, self.n_shift_points),
                                       fill_value=np.nan)
        self.degree = 3

        self.n_non_fitting_pipes = 0  # number of pipes with zero fit
        self.n_trashed_pipes = 0  # number of trashed pipes after on fit round which could not fit
        self.n_restored_pipes = 0  # number of restored pipes which are fitted in afterwards

        self.n_phi_samples = 2000
        # include the end point such that the phi values are symmetry and we can revert phi -> -phi
        self.phi_series = pd.Series(
            np.linspace(0, 2 * np.pi, self.n_phi_samples, endpoint=True), name="Phi")

        # a threshold value below which a pipe is assumed to fit
        self.fit_threshold = fit_threshold

        self.n_total_fitting_pipes = 0

        self.n_phi_resamples = None
        self.phi_resample_serie = None

        pd.set_option('display.multi_sparse', True)

    def other_side(self, side_char):
        """
        Give the name of the other side of the pipe

        Parameters
        ----------
        side_char: str
            Name of this side

        Returns
        -------
        str
            Name of other side
        """
        return self.other_side_name[side_char]

    def update_logger(self, level):
        self.logger.setLevel(level)

    def clear_data_frames(self):
        self.pipe_data_df = None
        self.pipe_sampled_df = None
        self.pipe_info_df = None
        self.pipe_fit_matrix_df = None
        self.pipe_index = None
        self.pipe_index_resampled = None

    @staticmethod
    def filename_to_keys(file_name):
        """
        Generate the keys for the tables from filename

        Parameters
        ----------
        file_name: str
            Name of the file

        Returns
        -------
        tuple
             (file_root, side_char) where *file_root* is the root of the file and *side_char* is the
             side label

        """

        file_base, ext = os.path.splitext(os.path.basename(file_name))

        # extract the direction based on the _A or _B which must be present at the end of the
        # file_name, except for the _aligned extension
        match = re.search("(.*)_([AB])[_aligned]*$", file_base)
        if bool(match):
            file_root = match.group(1)
            side_char = match.group(2)
        else:
            file_root = side_char = None

        return file_root, side_char

    def save_slice_data_to_hdf5(self, filename):
        """
        Write the data to a hdf5 file

        Parameters
        ----------
        filename: str
            Name of the hfd5 file name
        """
        with pd.HDFStore(filename, append=False) as store:
            try:
                store["pipe_data_df"] = self.pipe_data_df
                self.logger.info("stored pipe_data_df")
            except TypeError:
                self.logger.warning("Could not store pipe_data_df")
            try:
                store["pipe_sampled_df"] = self.pipe_sampled_df
                self.logger.info("stored pipe_sampled_df")
            except TypeError:
                self.logger.warning("Could not store pipe_sampled_df")
            try:
                store["pipe_info_df"] = self.pipe_info_df
                self.logger.info("stored pipe_info_df")
            except TypeError:
                self.logger.warning("Could not store pipe_info_df")
            try:
                store["pipe_fit_matrix_df"] = self.pipe_fit_matrix_df
                self.logger.info("stored pipe_fit_matrix_df")
            except TypeError:
                self.logger.warning("Could not store pipe_fit_matrix_df")

    def save_slice_data_to_pfd(self, filename):
        """
        Write the data to a pfd file.

        Parameters
        ----------
        filename: str
            Name of the pfd file name

        Notes
        -----
        Required for py2exe
        """
        df_list = list()
        try:
            df_list.append(dict(pipe_data_df=self.pipe_data_df))
            self.logger.debug("stored pipe_data_df")
        except TypeError:
            self.logger.debug("Could not store store pipe_data_df")

        try:
            df_list.append(dict(pipe_sampled_df=self.pipe_sampled_df))
            self.logger.debug("stored pipe_sampled_df")
        except TypeError:
            self.logger.debug("Could not store store pipe_sampled_df")

        try:
            # multiply the whole dataframe with 1 in order to convert all booleans to 0 and 1, as these result in a
            # a crashed by to_msgpack
            df_list.append(dict(pipe_info_df=self.pipe_info_df * 1))
            self.logger.debug("stored pipe_info_df")
        except TypeError:
            self.logger.debug("Could not store store pipe_info_df")

        try:
            # multiply the whole dataframe with 1 in order to convert all booleans to 0 and 1, as these result in a
            # a crashed by to_msgpack
            df_list.append(dict(pipe_fit_matrix_df=self.pipe_fit_matrix_df * 1))
            self.logger.debug("stored pipe_fit_matrix_df")
        except TypeError:
            self.logger.debug("Could not store store pipe_fit_matrix_df")

        try:
            self.logger.debug("Dumping list of df to file {}".format(filename))
            pd.to_msgpack(filename, df_list)
            self.logger.debug("Done")
        except IOError:
            self.logger.warning("Could not dump the dict list to {}".format(filename))

    def create_info_sheet(self):
        """
        Create the information sheet
        """
        # note that "now" return current utc time with any time_zone. Therefore, first use
        # tz_localize to set the time zone to utc, and then tz_convert to convert to the local time
        # zone
        current_time = pd.to_datetime("now").tz_localize("UTC").tz_convert(self.time_zone)

        info_dict = OrderedDict()
        info_dict["script version"] = __version__
        info_dict["script git revision id"] = package_info.git_sha
        info_dict["working location"] = os.getcwd()
        info_dict["current time"] = "{}".format(current_time)
        info_dict["time zone"] = self.time_zone
        info_dict["PIPE STATS"] = "--------------------------------------"
        n_pipes = self.pipe_info_df.index.levels[0].values.size
        n_sides = self.pipe_info_df.index.values.size
        n_fits = self.pipe_info_df["isFit"].sum()
        info_dict["Number of pipes"] = n_pipes
        info_dict["Number of sides"] = n_sides
        info_dict["Number of fits"] = n_fits
        # minus two because a pipe can not match with it self
        info_dict["Fit Percentage"] = 100 * (n_fits / (n_sides - 2))
        info_dict["Fit Threshold [mm]"] = self.fit_threshold

        self.df_info = pd.DataFrame.from_dict(info_dict, orient="index")
        self.df_info.index.name = "property"
        self.df_info.columns = ["value"]

    def export_info_to_excel(self, filename):
        """
        Export the data of the table to file

        Parameters
        ----------
        filename: str
            Name of the file
        """
        self.logger.debug("exporting table to {}".format(filename))
        with pd.ExcelWriter(filename) as writer:
            self.create_info_sheet()
            try:
                self.df_info.to_excel(writer, sheet_name=INFO_SHEET_NAME)
            except IOError:
                self.logger.warning("failed to write to Infosheet in excel file {}".format(
                    filename))

            try:
                self.pipe_info_df.to_excel(writer, sheet_name=STAT_SHEET_NAME)
            except IOError:
                self.logger.warning("failed to write to excel file {}".format(filename))

    def create_match_up_report(self, filename, label1, side1, label2, side2):
        """
        Create a report of a match of two pipes and write to an excel file

        Parameters
        ----------
        filename: str
            Name of the file to write to
        label1: str
            Label of the first pipe
        side1: str
            Side of the first pipe
        label2: str
            Label of the second pipe
        side2: str
            Side of the second pipe
        """
        self.logger.debug("Creating match up report of {}/{} with {}/{}"
                          "".format(filename, label1, side1, label2, side2))
        filebase, ext = os.path.splitext(filename)
        if ext is None:
            filename += ".xls"
        # create two keys to refer to the pipe sides
        key1 = "{}_{}".format(label1, side1)
        key2 = "{}_{}".format(label2, side2)

        # create a multi index dataframe base on the first pipe
        mi_level_fields = (label1, side1)
        mi_level_names = ["Label", "Side"]
        self.pipe_info_index = pd.MultiIndex.from_tuples([mi_level_fields], names=mi_level_names)
        df = pd.DataFrame(index=self.pipe_info_index, columns=self.pipe_info_df.columns.values)

        # file in the pipes
        df.ix[(label1, side1), :] = self.pipe_info_df.ix[(label1, side1), :]
        df.ix[(label2, side2), :] = self.pipe_info_df.ix[(label2, side2), :]
        df = df.reset_index().transpose()
        self.logger.debug(df.head())
        self.logger.debug(df.info())

        if ext == ".xls":
            with pd.ExcelWriter(filename, append=False) as writer:
                # write the data frame. Also transpose to make the data the rows
                df.to_excel(writer, sheet_name="{}_and_{}".format(key1, key2))
        else:
            log.warning("Can only write to xls")

    def export_profile_data(self, filename):
        self.logger.debug("exporting profiles to {}".format(filename))
        try:
            self.pipe_data_df.transpose().reset_index().transpose().to_excel(filename)
        except IOError:
            self.logger.warning("failed to write to excel file {}".format(filename))

    def save_slice_data(self, filename):
        (filebase, ext) = os.path.splitext(filename)
        if ext == ".pfd":
            # write the data to an excel file. Each slice is stored in a separate page
            self.logger.info("Exporting to pfd file {}".format(filename))
            self.save_slice_data_to_pfd(filename)
        elif ext == ".hdf5":
            try:
                # this should work, otherwise HDFStore does not work as well and stops you process
                # this is the case for the py2exe executable
                import tables
            except ImportError:
                self.logger.warning(
                    "Tables module can not be loaded due to HDF5.ddl not found error. "
                    "Skipping writing the h5 file")
                raise
            else:
                self.logger.info("Exporting to hdf5 file {}".format(filename))
                self.save_slice_data_to_hdf5(filename)
        else:
            self.logger.warning("extension {} not supported".format(ext))

    def load_slice_data_hdf5(self, filename):
        """
        Import the slice data from the hdf5 file

        Parameters
        ----------
        filename: str
            Name of the file to read from

        Notes
        -----
        The tables modules must be available, otherwise this routine wound work
        """
        try:
            # this should work, otherwise HDFStore does not work as well and stops you process
            # this is the case for the py2exe executable
            import tables
        except ImportError:
            self.logger.warning("tables module can not be loaded due to HDF5.ddl not found error. "
                                "Skipping writing the h5 file")
        else:
            with pd.HDFStore(filename) as store:
                try:
                    self.pipe_data_df = store["pipe_data_df"]
                except (TypeError, KeyError):
                    self.logger.warning("Could not load pipe_data_df")
                try:
                    self.have_sampled_data = True
                    self.pipe_sampled_df = store["pipe_sampled_df"]
                except (TypeError, KeyError):
                    self.have_sampled_data = False
                    self.logger.warning("Could not load pipe_sampled_df")
                try:
                    self.pipe_info_df = store["pipe_info_df"]
                except (TypeError, KeyError):
                    self.logger.warning("Could not load pipe_info_df")
                try:
                    self.have_pipe_fit_matrix = True
                    self.pipe_fit_matrix_df = store["pipe_fit_matrix_df"]
                except (TypeError, KeyError):
                    self.have_pipe_fit_matrix = False
                    self.logger.warning("Could not load pipe_fit_matrix_df")

        self.n_pipes = len(self.pipe_info_df.groupby(level=0))

    def load_slice_data_pfd(self, filename):
        """
        Read the pfd data that is stored according to the messagepack format.

        Parameters
        ----------
        filename: str
            Name of the pfd file

        Notes
        -----
        This format can be compiled to an executable (in contrast to hdf5) hence this is the
        preferred format
        """
        try:
            df_dict = pd.read_msgpack(filename)
            self.logger.info("Successfully read message pack pfd file {}".format(filename))
        except IOError:
            self.logger.warning("Read error message pack (pfd) file. Correct format?")
            return

        for dct in df_dict:
            if "pipe_data_df" in dct:
                self.pipe_data_df = dct["pipe_data_df"]
            elif "pipe_sampled_df" in dct:
                self.pipe_sampled_df = dct["pipe_sampled_df"]
            elif "pipe_info_df" in dct:
                self.pipe_info_df = dct["pipe_info_df"]
            elif "pipe_fit_matrix_df" in dct:
                self.pipe_fit_matrix_df = dct["pipe_fit_matrix_df"]
            else:
                self.logger.info("Could not find field {}".format(dct))

        self.n_pipes = len(self.pipe_info_df.groupby(level=0))

    def load_slice_data(self, filename):
        """
        Load the slide data file

        Parameters
        ----------
        filename: str
            Name of the file
        """
        (filebase, ext) = os.path.splitext(filename)
        if ext == ".hdf5":
            try:
                # this should work, otherwise HDFStore does not work as well and stops you process
                # this is the case for the py2exe executable
                import tables
            except ImportError:
                warning = "HDF5 module can not be loaded. Probably running executable version. " \
                          "Please read a PFD file"
                self.logger.warning(warning)
                raise ImportError
            else:
                info_message = "Importing from hdf5 file {}".format(filename)
                self.update_status_bar_message.emit(info_message)
                self.logger.info(info_message)
                self.load_slice_data_hdf5(filename)
        elif ext == ".pfd":
            self.logger.info("Importing from to pfd file {}".format(filename))
            self.load_slice_data_pfd(filename)
        else:
            self.logger.warning("Not yet implemented filetype: {}".format(ext))
            raise TypeError

    def initialise_pipe_index(self, label, side_char):
        """
        Initialise the pipe index for pipe *label* with side *sidechar*
        
        Parameters
        ----------
        label: str  
            Label of the pipe 
        
        side_char: str
            Side of the pipe

        """

        # create the multi level index (for the pipe_info_df) and columns (for the pipe_data_df).
        mi_level_fields = (label, side_char, "Ri")
        mi_level_names = ["Label", "Side", "Data"]
        self.pipe_index = pd.MultiIndex.from_tuples([mi_level_fields], names=mi_level_names)
        self.pipe_info_index = pd.MultiIndex.from_tuples([mi_level_fields[:2]],
                                                         names=mi_level_names[:2])

        # a pipedata data frame which stores the quantities per pipe with label XXXX and a side
        # charactor A or B in the columns and the phi value for the index
        self.pipe_data_df = pd.DataFrame(columns=self.pipe_index, index=self.phi_series)

        # Create a data frame to hold the data per file
        self.pipe_info_df = pd.DataFrame(index=self.pipe_info_index, columns=[])

        self.create_fitmatrix_df()

        self.have_sampled_data = False

        self.n_pipes = 0

    def create_fitmatrix_df(self):
        """
        Create the fitting matrix

        Notes
        -----
        The method creates a data frame with at the indices (rows) the pipe label and for each
        label a side character, and for the columns the same for each label and side + a data field
        """

        label = self.pipe_info_df.index.values[0][0]
        side = self.pipe_info_df.index.values[0][1]
        mi_level_fields = (label, side, "FitMinimum")
        mi_level_names = ["Label", "Side", "Data"]
        mi_columns = pd.MultiIndex.from_tuples([mi_level_fields], names=mi_level_names)
        self.pipe_fit_matrix_df = pd.DataFrame(index=self.pipe_info_df.index, columns=mi_columns)

        # set this flag to false because we need to fill the matrix first
        self.have_pipe_fit_matrix = False
        label_list = []
        for (label1, side_dummy) in self.pipe_info_df.index:
            if not label1 in label_list:
                label_list.append(label1)
            else:
                continue

            pipe_grouped_df = self.pipe_info_df.loc[label1]
            sides_list1 = pipe_grouped_df.index.get_level_values('Side')

            # loop over the two sides
            for cnt, side1 in enumerate(sides_list1):
                self.pipe_fit_matrix_df.ix[:, (label1, side1, "FitMinimum")] = None
                self.pipe_fit_matrix_df.ix[:, (label1, side1, "PhiOptimal")] = None
                self.pipe_fit_matrix_df.ix[:, (label1, side1, "ShiftYOptimal")] = None
                self.pipe_fit_matrix_df.ix[:, (label1, side1, "ShiftZOptimal")] = None
                self.pipe_fit_matrix_df.ix[:, (label1, side1, "isFit")] = False

                self.pipe_info_df.ix[(label1, side1), "N_fits_Side"] = int(0)
                self.pipe_info_df.ix[(label1, side1), "N_fits_Pipe"] = int(0)
                self.pipe_fit_matrix_df.ix[:, (label1, side1, "isFront")] = self.front_flag_list[
                    cnt]

    def radius_to_diameter(self, label, side_char):
        """
        Calculate the outer/inner diameter from the radius

        Parameters
        ----------
        label: str
            Name of pipe
        side_char: str
            Side of the pipe

        Notes
        -----
        Add the radius at phi to the radius at phi+pi by rolling the radius/phi arrow over half the
        samples

        """
        self.logger.debug("Calculating inner diameter")
        nh = int(self.n_phi_samples / 2)
        self.pipe_data_df.ix[:, (label, side_char, "Di")] = \
            self.pipe_data_df[label, side_char].Ri + \
            np.roll(self.pipe_data_df[label, side_char].Ri, nh)
        try:
            self.pipe_data_df.ix[:, (label, side_char, "Do")] = \
                self.pipe_data_df[label, side_char].Ro + \
                np.roll(self.pipe_data_df[label, side_char].Ro, nh)
        except AttributeError:
            # outer radius does not excist. Skip it
            self.logger.debug("could not find outer diameter")
            pass

    def import_scan_file(self, filename, pipe_id, side_char, frame_extension):
        """
        Import pipe from an excel data base or csv data base as generated by PTL

        Parameters
        ----------
        filename: str
            Name of the pipe
        pipe_id: str
            Label of the pie
        side_char:
            Name of the side
        frame_extension: str
            Name of the sheet that contains the data

        """

        file_base, file_extension = os.path.splitext(filename)

        self.logger.debug("Import file {}".format(filename))

        # a dataframe is made which contain on the rows the phi data and on the columns the
        # multiindex data with
        # the following structure
        # PIPE NAME 1
        # SIDE A or/and B
        # Properties , radius, thickness, etc
        # and that for each pipe

        if self.pipe_index is None:
            # initialise the dataframes if it does not yet exist
            self.initialise_pipe_index(pipe_id, side_char)

        # read the excel data file
        if file_extension == ".xls":
            data = pd.read_excel(filename, sheetname=frame_extension)
        elif file_extension == ".csv":
            data = pd.read_csv(filename, index_col=False)
        else:
            raise AssertionError("File type {} not yet implemented. Use .xls or .csv"
                                 "".format(file_extension))
        try:
            phi = data.phi.values
        except KeyError:
            try:
                phi = data.Phi.values
                data.drop(["Phi"], axis=1, inplace=True)
            except KeyError:
                raise AssertionError(
                    "Could not find the phi column. Name of angle should be 'phi' or 'Phi'")

        # Take care of negative phi values
        phi = np.where(phi < 0, phi + 2 * np.pi, phi)

        data["phi"] = phi.copy()
        data.set_index(phi, inplace=True)
        data.sort_index(ascending=True, inplace=True)

        data.dropna(axis=1, how='any', inplace=True)

        # select the data in the range 0 ~ 2 * pi
        data = data[data.phi < 2 * np.pi]

        if "Y" not in data.columns or "Z" not in data.columns:
            # we have not yet set the Y and Z column. Do that now. Note that Y is upward, X is into
            # the pipe directory and Z is pointing to the right when looking into the pipe
            # For phi=0, we are along the y+ axis, for phi=pi/2 we are along the z+ axis
            data["Y"] = np.cos(data.index) * data.radius
            data["Z"] = np.sin(data.index) * data.radius

        # copy the columns of the imported data to the pipe_data_df dataframe. In order reduce the
        # number of points, also resample the data
        for col in ["X", "Y", "Z", "radius", "radius_out", "Dwall"]:
            try:
                new_data, phi = resample(data[col].values, self.n_phi_samples - 1, data.phi.values)
                self.pipe_data_df.loc[:, (pipe_id, side_char, col)] = \
                    np.hstack((new_data, new_data[0]))
            except KeyError:
                self.logger.info("Could not interpolate column: {}. "
                                 "Probably not present".format(col))

        # store info per filename/side, such as mean position, filename, etc
        self.pipe_info_df.ix[(pipe_id, side_char), "filename"] = filename
        self.pipe_info_df.ix[(pipe_id, side_char), "slice_name"] = frame_extension
        self.pipe_info_df.ix[(pipe_id, side_char), "NeighbourLabel"] = None
        self.pipe_info_df.ix[(pipe_id, side_char), "NeighbourSide"] = None
        self.pipe_info_df.ix[(pipe_id, side_char), "isFit"] = False
        self.pipe_info_df.ix[(pipe_id, side_char), "isFront"] = True

        # initialise the inner radius with the one given in the data, but this can be updated later
        self.pipe_data_df.ix[:, (pipe_id, side_char, "Ri")] = \
            self.pipe_data_df[pipe_id, side_char].radius
        try:
            self.pipe_data_df.ix[:, (pipe_id, side_char, "Ro")] = \
                self.pipe_data_df[pipe_id, side_char].radius_out
        except (KeyError, AttributeError):
            self.logger.info("No radius_out in the excel data.")

        # already create a new column
        self.pipe_data_df.ix[:, (pipe_id, side_char, "HiLo")] = np.zeros(self.n_phi_samples)

        self.radius_to_diameter(pipe_id, side_char)

        # get the number of side of the current fileroot
        n_side_of_pipe = len(self.pipe_info_df.ix[pipe_id].index)
        self.logger.debug("found {} with {} sides".format(pipe_id, n_side_of_pipe))
        if n_side_of_pipe > 1:
            # we have already added a pipe with this label, so assume this is the back
            self.pipe_info_df.ix[(pipe_id, side_char), "isFront"] = False

        if n_side_of_pipe > 2:
            self.logger.warning(
                "Pipe {} occurs more than 2 times! Something is wrong".format(pipe_id,
                                                                              n_side_of_pipe))

    def create_ellipse(self, D_min, D_max):
        """
        Create an ellipse profile based on the min and max diameter

        Parameters
        ----------
        D_min: float
            Minimum diameter
        D_max: float
            Maximum diameter

        Notes
        -----
        An ellipse radius as a function of phi is given by

        .. math::
            r = b/\sqrt{1-\sigma\cos^2(\phi)}

        where :math:`\phi` is the angle with the positive x-axis, :math:`\sigma` follows from

        .. math::
            \sigma = 1 - (b/a)^2

        and :math:`a` and :math:`b` are the minimum and maximum radius, respectively

        Returns
        -------
        ndarray:
            Array with the radial data

        """

        r_min = D_min / 2
        r_max = D_max / 2

        sigma = 1 - (r_max / r_min) ** 2

        # add a random phase to the phi
        phase = 2 * np.pi * np.random.random()

        # radius of ellipse vs phi
        radius = r_max / np.sqrt(1 - sigma * np.cos(self.phi_series + phase) ** 2)

        return radius

    def create_slice_profile(self, profiles_dataframe):
        """

        Generate the data in stead of reading it from a XLS file. The profile_dataframe contains

        Parameters
        ----------
        profiles_dataframe: DataFrame
            DataFrame with the current data

        """
        # the information of the current slices to be made

        filename = profiles_dataframe.PipeNo

        self.logger.debug("Creating new profile for {}".format(filename))

        fileroot, sidechar = self.filename_to_keys(profiles_dataframe.PipeNo)

        if self.pipe_index is None:
            # initialise the dataframes if it does not yet exist
            self.logger.debug("Initialising the data for {} {}".format(fileroot, sidechar))
            self.initialise_pipe_index(fileroot, sidechar)

        # copy the columns of the imported data to the pipe_data_df dataframe. In order reduce the
        # number of points, also resample the data
        radius_in = self.create_ellipse(profiles_dataframe.MinID, profiles_dataframe.MaxID).values
        self.pipe_data_df[fileroot, sidechar, "radius"] = radius_in
        try:
            radius_out = self.create_ellipse(profiles_dataframe.MinOD,
                                             profiles_dataframe.MaxOD).values
            self.pipe_data_df[fileroot, sidechar, "radius_out"] = radius_out
            self.pipe_data_df[fileroot, sidechar, "Dwall"] = np.full(self.n_phi_samples,
                                                                     profiles_dataframe.AvgWT,
                                                                     float)
        except AttributeError:
            self.logger.warning("No outer radius available")

        self.pipe_data_df[fileroot, sidechar, "X"] = np.zeros(self.n_phi_samples)

        phi = self.phi_series.values
        self.pipe_data_df[fileroot, sidechar, "Y"] = radius_in * np.cos(phi)
        self.pipe_data_df[fileroot, sidechar, "Z"] = radius_in * np.sin(phi)

        self.pipe_data_df[fileroot, sidechar, "HiLo"] = np.zeros(self.n_phi_samples)

        # store info per filename/side, such as mean position, filename, etc
        self.pipe_info_df.ix[(fileroot, sidechar), "filename"] = filename
        self.pipe_info_df.ix[(fileroot, sidechar), "NeighbourLabel"] = None
        self.pipe_info_df.ix[(fileroot, sidechar), "NeighbourSide"] = None
        self.pipe_info_df.ix[(fileroot, sidechar), "isFit"] = False
        self.pipe_info_df.ix[(fileroot, sidechar), "isFront"] = True

        # initialise the inner radius with the one given in the data, but this can be updated later
        self.pipe_data_df[fileroot, sidechar, "Ri"] = self.pipe_data_df[fileroot, sidechar].radius

        self.radius_to_diameter(fileroot, sidechar)

        # get the number of side of the current fileroot
        n_side_of_pipe = len(self.pipe_info_df.ix[fileroot].index)

        if n_side_of_pipe > 2:
            self.logger.warning(
                "Pipe {} occurs more than 2 times! Something is wrong".format(fileroot,
                                                                              n_side_of_pipe))

    def centralise_all_profiles(self):
        # loop over all the pipes to centralise them
        n_centralised = 0
        label_list = []
        for (label, side_dummy) in self.pipe_info_df.index:
            if label not in label_list:
                label_list.append(label)
            else:
                continue

            pipe_grouped_df = self.pipe_info_df.loc[label]
            sides_list = pipe_grouped_df.index.get_level_values('Side')
            # loop over the two sides
            for sidechar in sides_list:
                # centralise the current profile
                n_centralised += self.centralise_profile(label, sidechar)

        self.logger.info("centralised {} profiles".format(n_centralised))

    def centralise_profile(self, label, side_char):

        # point to the current profile
        pipe_df = self.pipe_data_df[label, side_char]

        # centralise the current data profile given by label and side_char
        # create circle out of Y Z data (must be order in phi direction)
        circle = LinearRing(zip(pipe_df.Y.values,
                                pipe_df.Z.values))
        polygon = Polygon(circle)

        if np.linalg.norm(polygon.centroid.xy) < 1e-5:
            processed = 0
            self.logger.info("Profile {} {} already centralised. Skipping".format(label, side_char))
        else:
            self.logger.debug("Polygon centroid BEFORE {}".format(polygon.centroid.xy))

            # subtract the centroid x/y from the Y/Z data. Note that the polygon is 2D and only has
            # x and y
            self.pipe_data_df[label, side_char, "Y"] = pipe_df.Y.values - polygon.centroid.x
            self.pipe_data_df[label, side_char, "Z"] = pipe_df.Z.values - polygon.centroid.y

            # calculate polygon again to see if we are in the middle now
            circle = LinearRing(zip(pipe_df.Y.values, pipe_df.Z.values))
            polygon = Polygon(circle)
            self.logger.debug("Polygon centroid AFTER {}".format(polygon.centroid.xy))

            # convert the Y Z coordinates to radius and phi. It is assumed that y is upward, z is to
            # the right, such that with the right hand rule we are looking into the direction of x.
            # The angle is  positive in clock-wise direction with y=0 is phi=0. Therefore, the angle
            # is given by arctan(z/y)
            radius2d = np.linalg.norm(np.vstack((pipe_df.Y.values,
                                                 pipe_df.Z.values)), axis=0)

            # according to the definition above the angle phi is given by arctan(z/y); arctan2 turns
            # this around, so call  it with the Y coordinate first
            phi2d = np.arctan2(pipe_df.Z.values,
                               pipe_df.Y.values)
            # make sure  that phi is in between 0 and 2pi (and not -pi and pi, which is the output
            # of arctan2
            phi2d = np.where(phi2d < 0, phi2d + 2 * np.pi, phi2d)

            self.pipe_data_df[label, side_char, "Ri"] = radius2d

            # not that the phi is actually monitored by the index, therefore we store the new phi to
            # the index if the pipe was already but on the centroid, this should not matter, but if
            # this was  not the case we need to take the newly calculated phi after translating the
            # pipe to the centroid
            self.pipe_data_df.reindex(index=phi2d)

            processed = 1

        return processed

    def calculate_pipe_stats(self, label, sidechar):

        # update the statics based on the current data in the pipe_data_df

        RiMean = np.mean(self.pipe_data_df[label, sidechar].Ri)
        RiMin = np.min(self.pipe_data_df[label, sidechar].Ri)
        RiMax = np.max(self.pipe_data_df[label, sidechar].Ri)
        RiStd = np.std(self.pipe_data_df[label, sidechar].Ri)
        RiSkew = stats.skew(self.pipe_data_df[label, sidechar].Ri)

        self.pipe_info_df.ix[(label, sidechar), "DiMean"] = np.mean(
            self.pipe_data_df[label, sidechar].Di)
        self.pipe_info_df.ix[(label, sidechar), "DiMin"] = np.min(
            self.pipe_data_df[label, sidechar].Di)
        self.pipe_info_df.ix[(label, sidechar), "DiMax"] = np.max(
            self.pipe_data_df[label, sidechar].Di)
        self.pipe_info_df.ix[(label, sidechar), "DiOOR"] = self.pipe_info_df.ix[
                                                               (label, sidechar), "DiMax"] - \
                                                           self.pipe_info_df.ix[
                                                               (label, sidechar), "DiMin"]

        self.pipe_info_df.ix[(label, sidechar), "RiMean"] = RiMean
        self.pipe_info_df.ix[(label, sidechar), "RiMin"] = RiMin
        self.pipe_info_df.ix[(label, sidechar), "RiMax"] = RiMax
        self.pipe_info_df.ix[(label, sidechar), "RiStd"] = RiStd
        self.pipe_info_df.ix[(label, sidechar), "RiSkew"] = RiSkew
        self.pipe_info_df.ix[(label, sidechar), "RiAsym"] = 0.5 * ((RiMax - RiMean) ** 3 +
                                                                   (
                                                                           RiMin - RiMean) ** 3) / RiStd ** 3
        self.pipe_info_df.ix[(label, sidechar), "Ovality"] = 100 * (RiMax - RiMin) / RiMean
        try:
            self.pipe_info_df.ix[(label, sidechar), "DoMean"] = np.mean(
                self.pipe_data_df[label, sidechar].Do)
            self.pipe_info_df.ix[(label, sidechar), "DoMin"] = np.min(
                self.pipe_data_df[label, sidechar].Do)
            self.pipe_info_df.ix[(label, sidechar), "DoMax"] = np.max(
                self.pipe_data_df[label, sidechar].Do)
            self.pipe_info_df.ix[(label, sidechar), "DoOOR"] = self.pipe_info_df.ix[
                                                                   (label, sidechar), "DoMax"] - \
                                                               self.pipe_info_df.ix[
                                                                   (label, sidechar), "DoMin"]
        except AttributeError:
            self.logger.debug(
                "No outer wall information present. skipping the statistics of outer wall")

        try:
            self.pipe_info_df.ix[(label, sidechar), "DwallMean"] = np.mean(
                self.pipe_data_df[label, sidechar].Dwall)
        except AttributeError:
            self.logger.debug("No wall thickness present.")

    def set_fit_quality(self, label, side_char,
                        pipe_q,
                        phi_cumulative=None,
                        set_fit=False,
                        set_none=False):
        """
        Set the quality of the fit on the current pipe end.

        Parameters
        ----------
        label: str
            Name of the current pipe end
        side_char: str
            Side of the current pipe
        pipe_q:
            Pipe
        phi_cumulative
        set_fit
        set_none

        Returns
        -------

        """

        if set_none:
            # if this flag is set, all values are set to nan to show that this is a non-valid fit
            # for the begin or end of a sequence (where there are no neighbours and thus no fit)
            pipe_q.profile_distance_current = np.nan
            pipe_q.profile_distance_maximum = np.nan
            pipe_q.profile_distance_minimum = np.nan
            pipe_q.phi_optimal = 0
            pipe_q.shift_slice = Point(0, 0)
            set_fit = False

        # store the hilo vs phi in the data array
        try:
            self.pipe_data_df.ix[:, (label, side_char, "HiLo")] = pipe_q.hilo_vs_phi

            if pipe_q.hilo_vs_phi is not None:
                self.pipe_data_df.ix[:, (label, side_char, "HiLoOK")] = np.where(
                    pipe_q.hilo_vs_phi < self.fit_threshold, 1, np.nan)

            # calculate the shift distance away from the point 0, 0 (which is the COG)
            shift_mag = pipe_q.shift_slice.distance(Point(0, 0))

            if self.optimize_phi_at_translations > 0 and shift_mag > 0:
                # in case we have optimized the fit quality by translating over an area around COG
                # and indeed found better match away from the COG, update the Y/Z position of the
                # slide
                radius_new = shift_radial_profile(self.pipe_data_df.index,
                                                  radius=self.pipe_data_df.Ri,
                                                  shift_y=pipe_q.shift_slice.x,
                                                  shift_z=pipe_q.shift_slice.y)
                # we only need to update the yy/zz coordinates if we have been shifting the pipe
                # profile away from the SOG
                yy, zz = polar_to_cartesian(r=radius_new, t=self.pipe_data_df.index)
                self.pipe_data_df.ix[:, (label, side_char, "Y")] = yy
                self.pipe_data_df.ix[:, (label, side_char, "Z")] = zz
                self.pipe_data_df.ix[:, (label, side_char, "Ri")] = radius_new

            self.logger.debug("Store pipedata phi HiLo with statistics: {} {} {}".format(
                np.mean(self.pipe_data_df.ix[:, (label, side_char, "HiLo")]),
                np.min(self.pipe_data_df.ix[:, (label, side_char, "HiLo")]),
                np.max(self.pipe_data_df.ix[:, (label, side_char, "HiLo")])))
        except (KeyError, ValueError, AttributeError):
            try:
                self.pipe_sampled_df.ix[:, (label, side_char, "HiLo")] = pipe_q.hilo_vs_phi
                # calculate the shift distance away from the point 0, 0 (which is the COG)
                shift_mag = pipe_q.shift_slice.distance(Point(0, 0))

                if pipe_q.hilo_vs_phi is not None:
                    self.pipe_sampled_df.ix[:, (label, side_char, "HiLoOK")] = np.where(
                        pipe_q.hilo_vs_phi < self.fit_threshold, 1, np.nan)

                if self.optimize_phi_at_translations > 0 and shift_mag > 0:
                    # in case we have optimized the fit quality by translating over an area around
                    # COG and indeed found better match away from the COG, update the Y/Z position
                    # of the slide
                    radius_new = shift_radial_profile(self.pipe_sampled_df.index,
                                                      radius=self.pipe_sampled_df.Ri,
                                                      shift_y=pipe_q.shift_slice.x,
                                                      shift_z=pipe_q.shift_slice.y)
                    yy, zz = polar_to_cartesian(r=radius_new, t=self.pipe_sampled_df.index)
                    self.pipe_sampled_df.ix[:, (label, side_char, "Y")] = yy
                    self.pipe_sampled_df.ix[:, (label, side_char, "Z")] = zz
                    self.pipe_sampled_df.ix[:, (label, side_char, "Ri")] = radius_new

                self.logger.debug("Store pipe data phi HiLo with statistics: {} {} {}".format(
                    np.mean(self.pipe_sampled_df.ix[:, (label, side_char, "HiLo")]),
                    np.min(self.pipe_sampled_df.ix[:, (label, side_char, "HiLo")]),
                    np.max(self.pipe_sampled_df.ix[:, (label, side_char, "HiLo")])))
            except (KeyError, ValueError, AttributeError) as err:
                self.logger.debug("could not store the hi low array\n{}".format(err))

        self.pipe_info_df.ix[(label, side_char), "HiLo"] = pipe_q.profile_distance_current
        self.pipe_info_df.ix[(label, side_char), "HiLoMax"] = pipe_q.profile_distance_maximum
        self.pipe_info_df.ix[(label, side_char), "HiLoMin"] = pipe_q.profile_distance_minimum

        delta_hilo_ok = pipe_q.set_phi_range_around_optimum(self.fit_threshold)
        self.pipe_info_df.ix[(label, side_char), "PhiOptimal"] = np.rad2deg(pipe_q.phi_optimal)
        self.pipe_info_df.ix[(label, side_char), "PhiRange"] = delta_hilo_ok
        self.pipe_info_df.ix[(label, side_char), "ShiftYOptimal"] = pipe_q.shift_slice.x
        self.pipe_info_df.ix[(label, side_char), "ShiftZOptimal"] = pipe_q.shift_slice.y
        self.pipe_info_df.ix[(label, side_char), "ShiftOptimalInner"] = \
            pipe_q.phi_optimal * self.pipe_info_df.ix[(label, side_char)].RiMean
        try:
            self.pipe_info_df.ix[(label, side_char), "ShiftOptimalOuter"] = \
                pipe_q.phi_optimal * self.pipe_info_df.ix[(label, side_char)].RoMean
        except AttributeError:
            self.logger.debug("Could not use outer diameter to calculate radial shift")

        if phi_cumulative is not None:
            self.pipe_info_df.ix[(label, side_char), "PhiCumulative"] = \
                np.rad2deg(phi_cumulative % (2 * np.pi))

        if set_fit:
            if pipe_q.profile_distance_minimum < self.fit_threshold:
                self.pipe_info_df.ix[(label, side_char), "isFit"] = True
            else:
                self.pipe_info_df.ix[(label, side_char), "isFit"] = False

    def set_shift_origin_matrix(self):
        """
        This function should be called after updating the optimize_phi_at translation
        """
        d_shift = self.optimize_phi_at_translations
        shifts = np.linspace(-d_shift, d_shift, self.n_shift_points, endpoint=True)
        [self.shift_yy, self.shift_zz] = np.meshgrid(shifts, shifts, indexing="ij")

        self.min_hilo_matrix = np.full(shape=(self.n_shift_points, self.n_shift_points),
                                       fill_value=np.nan)

        A1 = np.vander(self.shift_yy.flatten(), self.degree)
        A2 = np.vander(self.shift_zz.flatten(), self.degree)
        self.d_shift_matrix = np.hstack((A1, A2))

    def find_minimal_profile_distance_in_area(self, radius_vs_phi1, radius_vs_phi2,
                                              phi_cumulative=0,
                                              pipe_min=None):
        """
        Create optimization object using the HiLo criterion by shifting the two pipes over an
        area around the COG

        Parameters
        ----------
        radius_vs_phi1: ndarray
            radial profile of first pipe
        radius_vs_phi2: ndarray
            radial profile of first pipe
        phi_cumulative: float, optional
            cumulative phi angle. Default is 0
        pipe_min: :obj:`PipeOptimize`
            The current 'optimal' pipe fit. In case the distance as calculated below has a better fit than 'pipe_min',
            or when 'pipe_min' is None, the 'pipe_min' will be replaced with this optimum



        Returns
        -------
        PipeOptimise
            Class holding the optimized phi slices

        """

        # will the matrix for all shift directions
        self.min_hilo_matrix[:, :] = np.nan
        for ii in range(self.n_shift_points):
            for jj in range(self.n_shift_points):
                delta_y = self.shift_yy[ii, jj]
                delta_z = self.shift_zz[ii, jj]
                shift = Point(delta_y, delta_z)
                pipe_q = PipeOptimise(radius_vs_phi1, radius_vs_phi2,
                                      shift_slice=shift,
                                      phi_cumulative=phi_cumulative,
                                      exclude_phi_range_weld=self.exclude_phi_range_weld,
                                      exclude_phi_range_six_oclock=
                                      self.exclude_phi_range_six_oclock)
                pipe_q.get_profile_distance_extremes()
                min_dist = pipe_q.profile_distance_minimum
                self.min_hilo_matrix[ii, jj] = min_dist
                if pipe_min is None or min_dist < pipe_min.profile_distance_minimum:
                    # store the translation if the minimum profile distance is smaller then the
                    # previous ones
                    self.logger.debug("Found a better match for {} {} : {}"
                                      "".format(delta_y, delta_z, min_dist))
                    pipe_min = pipe_q

        # based on all the values, find the minimum hilo using a least square fit of a parabola
        (coeffs, residuals, rank, sing_vals) = np.linalg.lstsq(self.d_shift_matrix,
                                                               self.min_hilo_matrix.flatten())
        acoeffs = coeffs[0:self.degree]
        bcoeffs = coeffs[self.degree:2 * self.degree]

        y_shift_ls = -acoeffs[1] / (2 * acoeffs[0])
        z_shift_ls = -bcoeffs[1] / (2 * bcoeffs[0])

        # according to the ls fit of a parabola, the minimum profile distance can be found
        # for the shift over y_shift_ls, z_shift_ls. Check that be calculating the fit distance
        shift = Point(y_shift_ls, z_shift_ls)
        pipe_q = PipeOptimise(radius_vs_phi1, radius_vs_phi2,
                              shift_slice=shift,
                              phi_cumulative=phi_cumulative,
                              exclude_phi_range_weld=self.exclude_phi_range_weld,
                              exclude_phi_range_six_oclock=self.exclude_phi_range_six_oclock)
        pipe_q.get_profile_distance_extremes()

        self.logger.debug("Found shift from LS {} {} : {} "
                          "".format(y_shift_ls, z_shift_ls, pipe_q.profile_distance_minimum))

        # check if the least square fit is smaller than the overall fit.
        if pipe_q.profile_distance_minimum < pipe_min.profile_distance_minimum:
            # yes, LS is smaller, so take this translation
            self.logger.info("Taking shift at from LS as it is smaller than:\n {}"
                             " ".format(pipe_min.profile_distance_minimum))
        else:
            # no, the profiles distance stored in pipe_min is smaller. Overwrite the pipe_q
            # with this smaller version.
            pipe_q = pipe_min

        return pipe_q

    def find_minimal_profile_distance(self, radius_vs_phi1, radius_vs_phi2, phi_cumulative=0):
        """
        Create optimization object using the HiLo criterion

        Parameters
        ----------
        radius_vs_phi1: ndarray
            radial profile of first pipe
        radius_vs_phi2: ndarray
            radial profile of first pipe
        phi_cumulative: float, optional
            cumulative phi angle. Default is None

        Returns
        -------
        PipeOptimise
            Class holding the optimized phi slices

        """
        # calculate the fit quality for the profile located at the COG first
        pipe_q = PipeOptimise(radius_vs_phi1, radius_vs_phi2,
                              phi_cumulative=phi_cumulative,
                              exclude_phi_range_weld=self.exclude_phi_range_weld,
                              exclude_phi_range_six_oclock=self.exclude_phi_range_six_oclock)
        pipe_q.get_profile_distance_extremes()

        if self.optimize_phi_at_translations > 0:
            # in case this is larger than 0, try to get a better match by shifting the second slice
            pipe_q = self.find_minimal_profile_distance_in_area(radius_vs_phi1=radius_vs_phi1,
                                                                radius_vs_phi2=radius_vs_phi2,
                                                                phi_cumulative=phi_cumulative,
                                                                pipe_min=pipe_q)

        return pipe_q

    def calculate_fit_quality(self, label1, side_char1, label2, side_char2, phi_cumulative=0,
                              use_sampled_data=True):
        """
        Parameters
        ----------
        label1: str
            Label of the current pipe
        side_char1: str
            Character of the side of the pipe
        phi_cumulative: float
            Cumulative angle
        use_sampled_data: bool
            Use the sampled data in stead of the full data of the profile

        Returns
        -------

        """
        # the fit quality based on the full resolution is requested (or the fit matrix has
        # not yet been calculated)
        try:
            if use_sampled_data and self.have_sampled_data:
                self.logger.debug("calculate fit resampled quality {} {}/ {} {}"
                                  "".format(label1, side_char1, label2, side_char2))
                # Use the sampled data if requested and if available
                df1 = self.pipe_sampled_df[(label1, side_char1)]
                df2 = self.pipe_sampled_df[(label2, side_char2)].copy()
            else:
                self.logger.debug("calculate fit full quality {} {}/ {} {}"
                                  "".format(label1, side_char1, label2, side_char2))
                # use the full resolution data
                df1 = self.pipe_data_df[(label1, side_char1)]
                df2 = self.pipe_data_df[(label2, side_char2)].copy()
        except (TypeError, KeyError):
            self.logger.info("Failed to calculate quality for {} {}/ {} {} ".format(
                label1, side_char1, label2, side_char2))
            pipe_q = None
        else:

            # since the neighbour profile has a phi running into the other direction: turn it around
            # (that is why we made a copy above otherwise you affect the original profile)
            self.logger.debug("turn around the order of phi for df2")
            df2.Ri = df2.Ri[::-1].values

            pipe_q = self.find_minimal_profile_distance(df1.Ri, df2.Ri,
                                                        phi_cumulative=phi_cumulative)

        return pipe_q

    def get_fit_quality(self, label1, side_char1, phi_cumulative=0, use_sampled_data=True,
                        set_fit=False):
        """
        Obtain the quality of the fit either from the fit matrix or, if the fitmatrix
        has not been calculated yet, calculate the fit quality

        Parameters
        ----------
        label1: str
            Label of the current pipe
        side_char1: str
            Character of the side of the pipe
        phi_cumulative: float
            Cumulative angle
        use_sampled_data: bool
            Use the sampled data in stead of the full data of the profile
        set_fit: bool

        Returns
        -------
        float
            The new cumulative angle *phi_cumulative*

        """

        self.logger.debug("Calculate Fit quality {}/{} phi_cum={} use_resampled={} set_fit={}"
                          "".format(label1, side_char1, phi_cumulative, use_sampled_data, set_fit))

        # update the statics based on the current data in the pipe_data_df
        try:
            # the the label and side_char of the neighbour of the current  profile
            # the neighbours are found using the store_current_neighbours
            label2 = self.pipe_info_df.ix[(label1, side_char1), "NeighbourLabel"]
            side_char2 = self.pipe_info_df.ix[(label1, side_char1), "NeighbourSide"]
            if label2 is None or side_char2 is None:
                raise KeyError
        except (KeyError, TypeError):
            # In case we could not get the label of the neighbouring pipe, it means we are at the
            # start or end of the sequency. Just return with zero phi values
            self.logger.info("Failed to determine fit quality: {} {} has no neighbour"
                             "".format(label1, side_char1))
            pipe_q = PipeOptimise(phi_optimal=0)
            self.set_fit_quality(label1, side_char1, pipe_q, set_none=True)
            phi_cumulative = 0.0
        else:
            # we have correctly obtained the neighbour labels. Now try to get the fit quality
            pipe_q = None
            if self.have_pipe_fit_matrix and use_sampled_data:
                # only use the fitting matrix if also use_sampled_data is true, because the fitting
                # matrix is build with sampled data.
                try:

                    fit_properties = self.pipe_fit_matrix_df.ix[(label1, side_char1),
                                                                (label2, side_char2,)]

                    profile_distance_minimum = self.pipe_fit_matrix_df.ix[(label1, side_char1),
                                                                          (label2, side_char2,
                                                                           "FitMinimum")]
                    phi_optimal = self.pipe_fit_matrix_df.ix[(label1, side_char1),
                                                             (label2, side_char2,
                                                              "PhiOptimal")]

                    shift_y = self.pipe_fit_matrix_df.ix[(label1, side_char1),
                                                         (label2, side_char2,
                                                          "ShiftYOptimal")]
                    shift_z = self.pipe_fit_matrix_df.ix[(label1, side_char1),
                                                         (label2, side_char2,
                                                          "ShiftZOptimal")]
                    shift = Point(shift_y, shift_z)

                    pipe_q = PipeOptimise(profile_distance_minimum=profile_distance_minimum,
                                          phi_optimal=phi_optimal,
                                          shift_slice=shift,
                                          exclude_phi_range_weld=self.exclude_phi_range_weld,
                                          phi_cumulative=phi_cumulative,
                                          exclude_phi_range_six_oclock=
                                          self.exclude_phi_range_six_oclock)

                    self.logger.debug("retrieve fit quality from matrix for {} {}/ {} {} : {}"
                                      "".format(label1, side_char1, label2, side_char2,
                                                phi_optimal))

                except (KeyError, TypeError):
                    self.logger.info("Failed to retrieve quality from matrix: {} {}/ {} {} ".format(
                        label1, side_char1, label2, side_char2))

            if pipe_q is None:
                # if pipe_q is not at this point, we did not retrieve it from the fit matrix
                # or failed at doing that. Therefore calculate it now
                pipe_q = self.calculate_fit_quality(label1=label1, side_char1=side_char1,
                                                    label2=label2, side_char2=side_char2,
                                                    phi_cumulative=phi_cumulative,
                                                    use_sampled_data=use_sampled_data)

            if pipe_q is not None:

                if pipe_q.phi_optimal is not None:
                    phi_cumulative += pipe_q.phi_optimal
                    phi_cumulative %= (2 * np.pi)

                try:
                    self.logger.debug("set fit quality {}/{} and {}/{} : phi={} dist={} cumm={}"
                                      "".format(label1, side_char1, label2, side_char2,
                                                np.degrees([pipe_q.phi_optimal])[0],
                                                pipe_q.profile_distance_minimum,
                                                np.degrees([pipe_q.phi_cumulative])[0]))

                    self.set_fit_quality(label1, side_char1, pipe_q, set_fit=set_fit,
                                         phi_cumulative=phi_cumulative)
                    # the back neighbour has the same fit quality, but is defined in opposite
                    # direction
                    pipe_q.phi_optimal = 0
                    self.set_fit_quality(label2, side_char2, pipe_q, set_fit=set_fit)
                except IndexError:
                    self.logger.info("Failed to set the quality for {} {} / {} {}".format(
                        label1, side_char1, label2, side_char2))
            else:
                # some how we still don't have a fit quality here.  Raise and error
                raise AssertionError("Failed to get the fit quality.")

        return phi_cumulative

    def get_phi_cumulative(self):
        """
        routine to update the cumulative phi for the current pipe order
        :return:
        """

        phi_cumulative_degrees = 0
        for (label, side_char) in self.pipe_info_df.index:
            phi_degrees = self.pipe_info_df.ix[(label, side_char)].PhiOptimal
            phi_cumulative_degrees += phi_degrees
            phi_cumulative_degrees %= 360
            self.pipe_info_df.ix[(label, side_char), "PhiCumulative"] = phi_cumulative_degrees
            self.logger.debug("phi cummulative for {}/{} adding {} {}".format(label, side_char,
                                                                              phi_degrees,
                                                                              phi_cumulative_degrees))

    def update_fit_quality_all_pipe(self, use_resampled_data=True):
        """

        Update all the statistics of the currently loaded pipes

        Parameters
        ----------
        use_resampled_data: bool, optional
            If true use resamples data for the fit quality. Default = True

        """

        # loop over all the pipe beginnings and with it with is neighbour one back in the sequence

        self.n_total_fitting_pipes = 0
        label_list = []
        pipe_nr = 0
        phi_cumulative = 0

        if self.optimize_phi_at_translations > 0:
            # in case we want to calculate the optimal rotation over a range of shift around
            # the COG of the slices, calculate the shift matrix used for the least square fit
            self.set_shift_origin_matrix()

        # initialise all phi's at start of loop
        self.pipe_info_df.ix[:, "PhiOptimal"] = 0.0
        self.pipe_info_df.ix[:, "PhiCumulative"] = 0.0

        for (label, side_dummy) in self.pipe_info_df.index:
            if label not in label_list:
                label_list.append(label)
            else:
                continue

            inx_label = self.pipe_info_df.index.labels[0][2 * pipe_nr]
            label2 = self.pipe_info_df.index.levels[0][
                inx_label]  # the level name of the row pipe_nr

            # get the real index by the pipe side belonging to pipe_nr
            inx_side = self.pipe_info_df.index.labels[1][2 * pipe_nr]
            sidechar = self.pipe_info_df.index.levels[1][
                inx_side]  # the level name of the row pipe_nr

            self.logger.debug(
                "update fit quality {} {} {} for phi_cumm={}".format(label, label2, sidechar,
                                                                     phi_cumulative))

            # the cumulative phi is returned
            phi_cumulative = self.get_fit_quality(label, sidechar, set_fit=True,
                                                  phi_cumulative=phi_cumulative,
                                                  use_sampled_data=use_resampled_data)

            if self.pipe_info_df.ix[(label, sidechar), "isFit"]:
                self.n_total_fitting_pipes += 1

            # update the progress bar every pipes
            if (pipe_nr % 1) == 0:
                self.progress_changed.emit(pipe_nr, self.n_pipes,
                                           "Fit HiLo pipe {}/{}".format(pipe_nr + 1,
                                                                        self.n_pipes - 1))
                # this call prevents the GUI from stalling in case the CPU gets too busy
                QApplication.processEvents()

            pipe_nr += 1

        # finally, update the cumulative phi for the current sequence
        self.get_phi_cumulative()

        self.display_finished.emit()

    def update_pipe_statistics(self):
        # update all the statitics of the currenlty loaded pipes

        # loop over all the pipes
        for cnt, (label, pipe_df_one_side) in enumerate(self.pipe_info_df.groupby(level=0)):
            sides_list = pipe_df_one_side.index.get_level_values('Side')
            # loop over the two sides
            for side_char in sides_list:
                self.calculate_pipe_stats(label, side_char)

            # update the progress bar every 20 pipes
            if (cnt % 1) == 0:
                self.progress_changed.emit(cnt, self.n_pipes,
                                           "Stats. pipe {}/{}".format(cnt + 1, self.n_pipes))
                # this call prevents the GUI from stalling in case the CPU gets too busy
                QApplication.processEvents()

        self.display_finished.emit()

    def duplicate_missing_pipe_side(self):
        # after reading the excel files or input data file, I want to have two sides of each pipe.
        # If they are not present, each side is copied to get two sides pipes
        self.logger.debug("duplicating missing pipe sides {}".format(len(self.pipe_info_df.index)))
        for label, pipeDF_oneside in self.pipe_info_df.groupby(level=0):
            sides_list = pipeDF_oneside.index.get_level_values('Side')
            number_of_sides = len(sides_list)
            if number_of_sides == 1:
                # only one side was found. Create the other side by copying this one
                sidechar = sides_list[0]
                if sidechar == "A":
                    new_side = "B"
                else:
                    if sidechar != "B":
                        self.logger.warning(
                            "unrecognised side character (should be A or B). Assuming new side A")
                    new_side = "A"

                self.logger.debug(
                    "adding for {} new side={} to excising = {}".format(label, new_side, sidechar))
                self.pipe_info_df.ix[(label, new_side), :] = self.pipe_info_df.ix[(label, sidechar),
                                                             :]
                self.pipe_info_df.ix[(label, new_side), "filename"] = None
                self.pipe_info_df.ix[(label, new_side), "isFront"] = False

                for col in self.pipe_data_df[label, sidechar].columns:
                    # copy all the data. We assumed that the direction of phi is given by the
                    # direction when we look into the pipe (no matter if it is the A or B side).
                    # Therefore, with duplication we need to revert the direction if we assumed that
                    # the pipe profile remains constant over the pipe
                    self.pipe_data_df[label, new_side, col] = self.pipe_data_df[
                                                                  label, sidechar, col].values[::-1]

        self.logger.debug("after duplication before sort {}".format(len(self.pipe_info_df.index)))
        self.logger.debug("{}".format(self.pipe_info_df.index))
        self.sort_on_pipe_direction()
        self.logger.debug("after duplication after sort {}".format(len(self.pipe_info_df.index)))
        self.logger.debug("{}".format(self.pipe_info_df.index))

        self.logger.debug("is Front before filter {}".format(self.pipe_info_df.isFront))

    def sort_on_pipe_direction(self):
        """
        Sort the pipes on the side character

        Notes
        -----
        after reading a list of files, it may happen dat the back side of a pipe is in the list
        before the front side. In this routine, sort on the sides character

        Returns
        -------

        """

        # the sortlevel is used two times: the first time to sort on the pipe side (level=1).
        # Use inplace to keep the same instance and not create a new one.
        self.logger.debug("Sorting the pipe list")
        self.pipe_info_df.sort_index(level=1, ascending=True, inplace=True)

        # now sort on th first level again to restore the name list. Make sure to set
        # sort_remain=True to prevent the next level to get sorted again as well
        self.pipe_info_df.sort_index(level=0, sort_remaining=False, inplace=True)

        # after sorting you have to update the neighbours (external to this routine), so set them
        # to None for now
        self.pipe_info_df.ix[:, "NeighbourLabel"] = None
        self.pipe_info_df.ix[:, "NeighbourSide"] = None

        self.reset_pipe_direction()

    def reset_pipe_direction(self):

        label_list = []
        pipe_nr = 0
        for (label, side_dummy) in self.pipe_info_df.index:
            if not label in label_list:
                label_list.append(label)
            else:
                continue

            inx_label = self.pipe_info_df.index.labels[0][2 * pipe_nr]
            label2 = self.pipe_info_df.index.levels[0][
                inx_label]  # the level name of the row pipe_nr

            # get the real index by the pipe side belonging to pipe_nr and front side
            inx_side_front = self.pipe_info_df.index.labels[1][2 * pipe_nr]
            sidechar_front = self.pipe_info_df.index.levels[1][
                inx_side_front]  # the level name of the row pipe_nr

            # get the real index by the pipe side belonging to pipe_nr and back side
            inx_side_back = self.pipe_info_df.index.labels[1][2 * pipe_nr + 1]
            sidechar_back = self.pipe_info_df.index.levels[1][
                inx_side_back]  # the level name of the row pipe_nr

            self.logger.debug("back {} {}".format(inx_side_back, sidechar_back))

            # set the first in the list to Front
            self.pipe_info_df.ix[(label, sidechar_front), "isFront"] = True
            self.pipe_info_df.ix[(label, sidechar_back), "isFront"] = False

            pipe_nr += 1

    def sort_on_n_fits(self):
        """
        Sort the pipes on the number of fits per pipe end

        Notes
        -----
        The pipes with the least fits are but in the beginning

        """

        # the sortlevel is used two times: the first time to sort on the pipe side (level=1).
        # Use inplace to keep # the same instance and not create a new one.
        self.logger.debug("Sorting the pipe list BEFORE")
        self.update_status_bar_message.emit("Presorting pipes")

        # after sorting you have to update the neighbours (external to this routine), so set them
        # to None for now
        self.pipe_info_df.ix[:, "NeighbourLabel"] = None
        self.pipe_info_df.ix[:, "NeighbourSide"] = None

        # based on.
        # http://stackoverflow.com/questions/20613844/how-to-sort-a-pandas-dataframe-by-columns
        # -and-a-part-of-multi-index-simultaneous

        # We turn the indices into columns. then apply a sort using first the N_fits_Side (which
        # sets the number of fits per side of the pipe into numerical order) and then using
        # N_fits_Pipe (which sets the number of fits based on the minimum side of a pipe into
        # order). By doing so, we get an ordered list of pipes
        index = self.pipe_info_df.index.names
        # we are going to sort first on N_fits_side, to get the smaller number of fits per pipe at
        # the front, then on the label number to get the side labels paired again, and then on
        # N_fits_Pipe
        tmp = self.pipe_info_df.reset_index().sort_values(["N_fits_Pipe", "Label", "N_fits_Side"])

        # now the index is restored again
        self.pipe_info_df = tmp.set_index(index)

        # finally, find the new neighbours of the pre-sorted list
        self.store_current_neighbours()
        self.logger.debug("Display finished pipes")
        self.display_finished.emit()

    @staticmethod
    def get_data_frame_row_by_index(pipe_info_df, pipe_nr, i_side, cyclic_list=False):
        """
        Get the pipe_info data frame the current row of the data frame (such that the data can be
        accessed and the label and side character.
        
        Since two side characters can be present, it can be selected with i_side
        where i_side=0 give the start and i_side = 1 give the end
        
        Parameters
        ----------
        pipe_info_df: DataFrame
           DataFrame containing the information of all pipes
        pipe_nr: int
            Current pipe number
        i_side: int
            index to give the current side
        cyclic_list: bool
            Use a cylclic list

        Returns
        -------
        int
           Index of the current data frame

        """

        max_nr = len(pipe_info_df.groupby(level=0))
        if pipe_nr < 0 or pipe_nr >= max_nr:
            if cyclic_list:
                # we assume that the list is cyclic, so return the cycled value
                pipe_nr %= max_nr
            else:
                # we are looking for neighbours, we do not want to have a cyclic value. raise an
                # IndexError
                raise IndexError

        #
        # the index of the multiindex data frame has a label (name of the pipe) and then two letter
        # label side
        # bla   A
        #       B

        inx_label = pipe_info_df.index.labels[0][2 * pipe_nr]
        label = pipe_info_df.index.levels[0][inx_label]  # the level name of the row cnt

        # get the real index by the pipe side belonging to pipe_nr
        inx_side = pipe_info_df.index.labels[1][2 * pipe_nr + i_side]
        side = pipe_info_df.index.levels[1][inx_side]  # the level name of the row cnt

        # also return a point to the current data frame of this pipe
        df = pipe_info_df.ix[label]

        return label, side, df

    def move_pipe_in_list(self, label, direction):
        """
        Move the pipe up or down in the info list

        Parameters
        ----------
        label: int
            Index of the pipe to  move up or down
        direction: {"up", "down"}
            Direction to which to move the pipe. Can be "up" or "down"

        """

        label_ref = self.pipe_info_df.index.get_level_values("Label")
        this_pipe_slice = label_ref.get_loc(label)
        try:
            this_pipe_index = this_pipe_slice.start
        except AttributeError:
            this_pipe_index = np.where(this_pipe_slice)[0][0]

        # get the pipe to swap with based on index number such we don't have to have the neighbours
        # first. Note that the first profile  of the pipe (which is at the front) is swapped with
        # the first profile of the next pipe (or previous pipe), such that you have to add 2
        if direction == "up":
            swap_pipe_index = this_pipe_index - 2
        elif direction == "down":
            swap_pipe_index = this_pipe_index + 2
        else:
            raise AssertionError("direction must be either 'up' or 'down'. Found {}"
                                 "".format(direction))

        # now get the label of the the pipe and side of both this pipe and the swap pipe
        this_pipe_info = self.pipe_info_df.loc[label]
        this_pipe_sides = this_pipe_info.index.get_level_values("Side")

        swap_pipe_label = self.pipe_info_df.index.get_level_values("Label")[swap_pipe_index]
        swap_pipe_info = self.pipe_info_df.ix[swap_pipe_label]
        swap_pipe_sides = swap_pipe_info.index.get_level_values("Side")

        # each pipe has two slides (a front and a back). Store both slices in a list for both
        # the current slice and the slice with which we want to swap
        this_pipe_slices = list()
        swap_pipe_slices = list()
        for side in this_pipe_sides:
            this_pipe_slices.append(self.pipe_info_df.ix[(label, side)].copy())
        for side in swap_pipe_sides:
            swap_pipe_slices.append(self.pipe_info_df.ix[(swap_pipe_label, side)].copy())

        # now impose the swap pipe slice to this pipe and the other way around
        for ii, side in enumerate(this_pipe_sides):
            self.pipe_info_df.ix[(label, side)] = swap_pipe_slices[ii]
        for ii, side in enumerate(swap_pipe_sides):
            self.pipe_info_df.ix[(swap_pipe_label, side)] = this_pipe_slices[ii]

        # in order to update the index labels as well we have to recreate the whole multi-index
        labels = self.pipe_info_df.index.get_level_values("Label").tolist()
        sides = self.pipe_info_df.index.get_level_values("Side").tolist()

        for ii in range(2):
            labels[this_pipe_index + ii] = swap_pipe_label
            labels[swap_pipe_index + ii] = label
            sides[this_pipe_index + ii] = swap_pipe_sides[ii]
            sides[swap_pipe_index + ii] = this_pipe_sides[ii]

        # create a new multi index base on the new pipe order
        new_index = list(zip(labels, sides))
        mi_new = pd.MultiIndex.from_tuples(new_index, names=self.pipe_info_df.index.names)
        self.pipe_info_df.index = mi_new

        # finally, update the neighbours after moving the pipe in the list
        first_index = min(this_pipe_index, swap_pipe_index) - 1
        last_index = max(this_pipe_index, swap_pipe_index) + 3
        self.store_current_neighbours(first_index=first_index, last_index=last_index)

    def swap_pipe_direction(self, label):
        """
        Swap the pipe in the info list

        Parameters
        ----------
        label: int
            Index of the pipe to  move up or down
        """

        label_ref = self.pipe_info_df.index.get_level_values("Label")
        this_pipe_slice = label_ref.get_loc(label)
        try:
            this_pipe_index = this_pipe_slice.start
        except AttributeError:
            this_pipe_index = np.where(this_pipe_slice)[0][0]

        # now get the label of the the pipe and side of both this pipe and the swap pipe
        this_pipe_info = self.pipe_info_df.loc[label]
        this_pipe_sides = this_pipe_info.index.get_level_values("Side")

        # the current slice and the slice with which we want to swap
        this_pipe_slices = list()
        for side in this_pipe_sides:
            this_pipe_slices.append(self.pipe_info_df.ix[(label, side)].copy())

        # now impose the swap pipe slice to this pipe and the other way around
        for ii, side in enumerate(this_pipe_sides):
            self.pipe_info_df.ix[(label, side)] = this_pipe_slices[1 - ii]

        # in order to update the index labels as well we have to recreate the whole multi-index
        labels = self.pipe_info_df.index.get_level_values("Label").tolist()
        sides = self.pipe_info_df.index.get_level_values("Side").tolist()

        for ii in range(2):
            sides[this_pipe_index + ii] = this_pipe_sides[1 - ii]

        # create a new multi index base on the new pipe order
        new_index = list(zip(labels, sides))
        mi_new = pd.MultiIndex.from_tuples(new_index, names=self.pipe_info_df.index.names)
        self.pipe_info_df.index = mi_new

        # finally, update the neighbours after moving the pipe in the list
        first_index = this_pipe_index - 1
        last_index = this_pipe_index + 2
        self.store_current_neighbours(first_index=first_index, last_index=last_index)

    def store_current_neighbours(self, first_pipe=None, last_pipe=None, first_index=None,
                                 last_index=None):
        """
        Set the current neighbours to each pipe end

        Parameters
        ----------
        first_pipe: int, optional
            Start setting neighbours from this pipe. Default = None, i.e. start from here
        last_pipe: int, optional
            Stops setting neighbours at this pipe. Default = None, i.e. continue until end
        first_index: int, optional
            Start setting neighbours at this side. Default = None, i.e. start from here
        last_index: int, optional
            Stops setting neighbours at this side. Default = None, i.e. continue until end

        """

        self.logger.debug("Index {}".format(self.pipe_info_df.index))

        n_pipes = len(self.pipe_info_df.index.levels[0])

        for cnt, (label, side) in enumerate(self.pipe_info_df.index):

            # the pipe number is the profile index divided by two (two profiles per pipe
            i_pipe_current = int(cnt / 2)
            i_pipe_neighbour = i_pipe_current

            # we an limit the range of pipes for which we want to find the neighbours, which is
            # more efficient when for instance we already have the neighbours and are moving one
            # pipe up or down. In that case you only want to update the neighbours of the
            # surrounding pipes.  Here we check if we are outside the range (based on pipe number
            # of slice number (we have two time more slices as each pipe has two slices)
            if first_pipe is not None and i_pipe_current < first_pipe:
                continue
            if last_pipe is not None and i_pipe_current >= last_pipe:
                continue
            if first_index is not None and cnt < first_index:
                continue
            if last_index is not None and cnt >= last_index:
                continue

            # this is a pythonic way to check of any all the flags are None. In that case we update
            # the whle range of pipes and then we want to have a progress bar
            if all(v is None for v in [first_pipe, last_pipe, first_index, last_index]):
                # create a status bar only if we do not limit the range
                self.progress_changed.emit(cnt, 2 * n_pipes,
                                           "Finding Neighbours ({}/{})".format(cnt + 1,
                                                                               2 * n_pipes))
                QApplication.processEvents()

            self.logger.debug("proc {} {} {}".format(cnt, label, side))

            if (cnt % 2) == 0:
                # even number: is front side of pipe, so look for back neighbour (which means that
                # the neighbour index is one lower, and the side_index is the last one
                i_pipe_neighbour -= 1
                i_side = 1
                self.pipe_info_df.ix[(label, side), "isFront"] = True
            else:
                i_pipe_neighbour += 1
                i_side = 0
                self.pipe_info_df.ix[(label, side), "isFront"] = False

            label_neighbour = None
            side_neighbour = None

            if (i_pipe_neighbour >= 0) and (i_pipe_neighbour < n_pipes):
                try:
                    # based on the labels list the real index of the label is obtained
                    # (which can be different after a sort)
                    label_neighbour, side_neighbour, df = self.get_data_frame_row_by_index(
                        self.pipe_info_df,
                        i_pipe_neighbour, i_side)

                    self.logger.debug("neighbour pipe {} {} {}: {} {} {}".format(cnt, label, side,
                                                                                 i_pipe_neighbour,
                                                                                 label_neighbour,
                                                                                 side_neighbour))

                except IndexError:
                    self.logger.warning("No neighbour found for {} {} {}".format(cnt, label, side))

            else:
                self.logger.debug(
                    "skipping this one {} {}".format(i_pipe_current, i_pipe_neighbour))

            # store the neighbours for the current profile
            self.pipe_info_df.ix[(label, side), "NeighbourLabel"] = label_neighbour
            self.pipe_info_df.ix[(label, side), "NeighbourSide"] = side_neighbour

    def init_sample_space(self, n_points):
        """
        Re-sample the full radius-phi range to a lower resolution

        Parameters
        ----------
        n_points: int
            Number of point to resample

        Returns
        -------

        """
        self.n_phi_resamples = n_points

        # include the end point in order to make the phi range symmetryic
        self.phi_resample_serie = pd.Series(
            np.linspace(0, 2 * np.pi, self.n_phi_resamples, endpoint=True), name="Phi")
        self.logger.debug("resampling on {} points ({} requested)"
                          "".format(self.n_phi_resamples, self.phi_resample_serie.shape))

        # retrieve the label and side character from the first pipe in the pipeinfo data frame
        i_row = 0
        i_side = 0
        label = self.pipe_info_df.index.levels[0][i_row]  # the level name of the row cnt
        side = self.pipe_info_df.ix[label].index[i_side]

        # create a new multiindex
        mi_level_fields = (label, side, "Ri")
        mi_level_names = ["Label", "Side", "Data"]

        self.pipe_index_resampled = pd.MultiIndex.from_tuples([mi_level_fields],
                                                              names=mi_level_names)

        # create a new time series based on the number of sample points
        self.pipe_sampled_df = pd.DataFrame(columns=self.pipe_index_resampled,
                                            index=self.phi_resample_serie)

    def pipe_resample(self, n_points=32):

        self.init_sample_space(n_points)

        for i, (index, row) in enumerate(self.pipe_info_df.iterrows()):
            data = self.pipe_data_df[index[0], index[1]]

            # the resample routine of python returns sample points at an open interval. The first
            # point starts at the start of the interval, and the next point at Ldomain/N where in
            # our case Ldomain is 2pi and N is # the number of required points. The problem is a
            # closed interval is needed, because we want a symmetric range in phi such that the Ri
            # values can be mirrored. Therefore, first create the sample points at a range with
            # one point less then required, and than copy the first points to the end
            radii, phi = resample(data.Ri.values, self.n_phi_resamples - 1, data.index)

            self.pipe_sampled_df[index[0], index[1], "Ri"] = np.hstack((radii, radii[0]))

        # set this flag true so we can check if we have resampled data later
        self.have_sampled_data = True

    def get_fits_for_this_pipe(self, label_current, side_current):
        """
        For a given pipe end loop over all other pipes to count the number of fits

        Parameters
        ----------
        label_current: str
            Name of the pipe to fit
        side_current: str
            Side of the pipe to fit

        """

        # make a fit of all the pipe to the current pipe. Only rotate the current pipe in Ri
        # direction
        df1 = self.pipe_sampled_df[(label_current, side_current)].copy()
        df1.Ri = df1.Ri[::-1].values

        label_list = []
        for (label, side_dummy) in self.pipe_info_df.index:
            if (label not in label_list) and (label != label_current):
                label_list.append(label)
            else:
                # In case we have had this pipe already, skip it because per pipe we process both
                # side already also skip is the pipe is equal to itself.
                continue

            # get reference to this pipe
            this_pipe_info = self.pipe_info_df.loc[label]

            # get the labels of the two pipe end
            sides_list = this_pipe_info.index.get_level_values('Side')

            # loop over the pipe ends of this pipe 'label'
            for i_side, side in enumerate(sides_list):

                # get the fit minimum of the current pipe with the pipe *label*
                fit_minimum = self.pipe_fit_matrix_df.ix[(label_current, side_current),
                                                         (label, side, "FitMinimum")]
                # only calculate fit in case it was not already calculated before
                if pd.isnull(fit_minimum):
                    try:
                        df2 = self.pipe_sampled_df[(label, side)]
                    except (KeyError, TypeError):
                        self.logger.info("Failed to get fit a for {} {}".format(label, side))
                    else:

                        # Create optimization object using the HiLo criterion
                        pipe_q = self.find_minimal_profile_distance(df1.Ri, df2.Ri)

                        # store the current fit distance in the fitmatrix
                        self.pipe_fit_matrix_df.ix[
                            (label_current, side_current), (label, side, "FitMinimum")] = \
                            pipe_q.profile_distance_minimum
                        self.pipe_fit_matrix_df.ix[
                            (label_current, side_current), (label, side, "PhiOptimal")] = \
                            pipe_q.phi_optimal

                        # also store the transposed version which by definition is the same number
                        self.pipe_fit_matrix_df.ix[
                            (label, side), (label_current, side_current, "FitMinimum")] = \
                            pipe_q.profile_distance_minimum
                        self.pipe_fit_matrix_df.ix[
                            (label, side), (label_current, side_current, "PhiOptimal")] = \
                            pipe_q.phi_optimal
                        self.pipe_fit_matrix_df.ix[
                            (label, side), (label_current, side_current, "ShiftYOptimal")] = \
                            pipe_q.shift_slice.x
                        self.pipe_fit_matrix_df.ix[
                            (label, side), (label_current, side_current, "ShiftZOptimal")] = \
                            pipe_q.shift_slice.y

                        # store the number of pipe fits for the current pipe
                        if pipe_q.profile_distance_minimum < self.fit_threshold:
                            self.pipe_info_df.ix[(label_current, side_current), "N_fits_Side"] += 1
                            self.pipe_info_df.ix[(label, side), "N_fits_Side"] += 1
                            self.pipe_fit_matrix_df.ix[
                                (label, side), (label_current, side_current, "isFit")] = True
                            self.pipe_fit_matrix_df.ix[
                                (label_current, side_current), (label, side, "isFit")] = True

    def update_n_fits_per_pipe(self):

        if not self.have_pipe_fit_matrix:
            self.logger.info("fit matrix not exciting yet. Can not number of fits per side")
            return

        # update the number of fits per side
        self.logger.debug("Get number of fits per side from matrix ")

        self.pipe_info_df.ix[:, "N_fits_Side"] = 0
        self.pipe_info_df.ix[:, "isFit"] = False

        label_list = []
        for (label, side_dummy) in self.pipe_info_df.index:
            if label not in label_list:
                label_list.append(label)
            else:
                continue

            # get the side labels of the current pipe
            pipe_grouped_df = self.pipe_info_df.loc[label]
            sides_list = pipe_grouped_df.index.get_level_values('Side')

            n_fits_per_side = []

            for side in sides_list:

                # set all the value lower than the threshold to True
                is_fit = self.pipe_fit_matrix_df.ix[:,
                         (label, side, "FitMinimum")] < self.fit_threshold
                self.pipe_fit_matrix_df.ix[:, (label, side, "isFit")] = is_fit

                # set both side of the current pipe back to false (as a pipe can never fit with
                # itself
                for side1 in sides_list:
                    self.pipe_fit_matrix_df.ix[(label, side1), (label, side, "isFit")] = False

                # count the number of fits and add to N_fits_Side
                self.pipe_info_df.ix[(label, side), "N_fits_Side"] = \
                    self.pipe_fit_matrix_df.ix[:, (label, side, "isFit")].sum()

                # store the number of profiles which can fit on this side
                n_fits_per_side.append(self.pipe_info_df.ix[(label, side), "N_fits_Side"])

                label_neighbour = self.pipe_info_df.ix[(label, side), "NeighbourLabel"]
                side_neighbour = self.pipe_info_df.ix[(label, side), "NeighbourSide"]

                try:
                    # if of the current pipe side the neighbour was already set to true, also set
                    # this side to true (isFit)
                    if self.pipe_fit_matrix_df.ix[(label, side),
                                                  (label_neighbour, side_neighbour, "isFit")] or \
                            self.pipe_fit_matrix_df.ix[(label_neighbour, side_neighbour),
                                                       (label, side, "isFit")]:
                        self.pipe_info_df.ix[(label, side), "isFit"] = True
                        self.pipe_info_df.ix[(label_neighbour, side_neighbour), "isFit"] = True
                except (KeyError, IndexError):
                    self.logger.debug("skipping {} {} {} {} {}".format(label, side, label_neighbour,
                                                                       side_neighbour,
                                                                       self.fit_threshold))
                    pass

            # for each pipe, set the fit quality based on the minimum number of fits
            min_fits_of_this_pipe = min(n_fits_per_side)

            for side in sides_list:
                self.pipe_info_df.ix[(label, side), "N_fits_Pipe"] = min_fits_of_this_pipe

        # get total number of fitting pipes by counting the total number of fitting profiles. The
        # number of pipes is half of that. Assume that the start and end of the sequence always fit
        # (so add two to the sum)
        self.n_total_fitting_pipes = int((self.pipe_info_df.ix[:, "isFit"].sum() + 2) / 2)

        self.display_finished.emit()

    def build_fit_matrix(self):

        # initialise the dataframes if it does not yet exist
        self.logger.debug("Initialising fit matrix ")
        self.create_fitmatrix_df()

        if self.optimize_phi_at_translations > 0:
            # in case we want to calculate the optimal rotation over a range of shift around
            # the COG of the slices, calculate the shift matrix used for the least square fit
            self.set_shift_origin_matrix()

        # initialise all phi's at start of loop

        label_list = []
        cnt = 0
        for (label_current, side_dummy) in self.pipe_info_df.index:
            if label_current not in label_list:
                label_list.append(label_current)
            else:
                continue

            pipe_grouped_df = self.pipe_info_df.loc[label_current]

            sides_list = pipe_grouped_df.index.get_level_values('Side')

            n_fits_per_side = []

            for i_side, side_current in enumerate(sides_list):
                with Timer("get_fits_for_this_pipe {} {}".format(label_current, side_current)) as t:
                    self.get_fits_for_this_pipe(label_current, side_current)

                # store the number of profiles which can fit on this side
                n_fits_per_side.append(
                    self.pipe_info_df.ix[(label_current, side_current), "N_fits_Side"])

            # for each pipe, set the fit quality based on the minimum number of fits
            min_fits_of_this_pipe = min(n_fits_per_side)
            self.logger.debug("found min fits {} {}".format(n_fits_per_side, min_fits_of_this_pipe))

            for i_side, side_current in enumerate(sides_list):
                self.pipe_info_df.ix[
                    (label_current, side_current), "N_fits_Pipe"] = min_fits_of_this_pipe

            # update the progress bar every something pipes
            if (cnt % 1) == 0:
                self.progress_changed.emit(cnt, self.n_pipes,
                                           "Build Fitting Matrix Pipe {}/{}".format(cnt + 1,
                                                                                    self.n_pipes))
                # this call prevents the GUI from stalling in case the CPU gets too busy
                QApplication.processEvents()

            cnt += 1

        # switch this flag to True so we know we can use it
        self.have_pipe_fit_matrix = True
        self.display_finished.emit()

    @staticmethod
    def get_fit_quality_for_two_profiles(df_last, df_current, exclude_range):

        # because the phi rotation definition is always in the direction of the pipe, we have to
        # turn around the direction of the radius
        df_current.Ri = df_current.Ri.values[::-1]

        # Create optimization object using the HiLo criterion
        pipe_q = PipeOptimise(df_last.Ri, df_current.Ri, exclude_phi_range_weld=exclude_range)
        pipe_q.get_profile_distance_extremes()

        return pipe_q

    def remove_n_zero_fits(self):
        """
        remove all the pipes having a zero fit on either of one or two side and put them in a trashlist
        :return: a list with all the removed pipes
        """
        pipeinfo_trash_list = []
        for (label_current, pipe_grouped_df) in self.pipe_info_df.groupby(level=0):

            sides_list = pipe_grouped_df.index.get_level_values('Side')
            remove_this_pipe = False
            for side_current in sides_list:
                if self.pipe_info_df.ix[(label_current, side_current), "N_fits_Side"] == 0:
                    remove_this_pipe = True

            if remove_this_pipe:
                pipeinfo_trash_list.append(label_current)

                # now remove the pipe from the pool
                self.pipe_info_df_in_pool.drop(label_current, level='Label', inplace=True)
        return pipeinfo_trash_list

    def create_empty_info_dataframe(self):
        """
        Create an empty data frame to hold the info data

        Returns
        DataFrame
            Empty data frame holding the information per pipe end
        """
        # an empty multi  index table with levels based on the pipe_info_df
        pipe_info_index = pd.MultiIndex.from_tuples([tuple([None, None])],
                                                    names=self.pipe_info_df.index.names)

        # create the data frame out of the multiindex table
        pipe_info_df = pd.DataFrame(index=pipe_info_index, columns=[])

        # removed the nan field inmediately, it will mess up things later
        pipe_info_df.drop(np.nan, level='Label', inplace=True)

        return pipe_info_df

    def find_next_pipe_in_sequence(self, sorting_method=0):
        """
        Loop over all the pipe in the pool and get the next fit

        Returns
        -------
        tuple (label_min, side_min, profile_distance_minimum, label_last, side_last)
            - label_min : the label of the fitting pipe
            - side_min :  the side of the fitting pipe that fits to the head
            - profile_distance_minimum: the hilow profile distanc
            - label_last: The last label
            - side_last: The last side
        """

        profile_distance_minimum = None
        label_last = None
        side_last = None
        side_min = None
        label_min = None
        n_sorted_pipes = len(self.pipe_info_df_sorted_list.groupby(level=0))

        if n_sorted_pipes == 0:
            # for the first item in the sorted list just pick the first frame of the pool
            label_min, side_min, df_dummy = self.get_data_frame_row_by_index(
                self.pipe_info_df_in_pool, 0, 0)
        else:
            # current number of pipes in the pool
            label_last, side_last, df_info_last = self.get_data_frame_row_by_index(
                self.pipe_info_df_sorted_list,
                -1, 1, cyclic_list=True)
            if not self.have_pipe_fit_matrix:
                # set the resampled data the the last frame in the list
                df_last = self.pipe_sampled_df[(label_last, side_last)]

            for cnt, (label, side) in enumerate(self.pipe_info_df_in_pool.index):

                self.logger.debug(
                    "Fitting {} {} to last {} {}".format(label, side, label_last, side_last))

                if self.have_pipe_fit_matrix:
                    # retrieve the profile distance from the fit matrix we have already calculated
                    profile_distance_current = self.pipe_fit_matrix_df.ix[(label, side),
                                                                          (label_last, side_last,
                                                                           "FitMinimum")]
                else:
                    # calculate the fit quality
                    pipe_Q = self.get_fit_quality_for_two_profiles(df_last,
                                                                   self.pipe_sampled_df[
                                                                       (label, side)].copy(),
                                                                   self.exclude_phi_range_weld)
                    profile_distance_current = pipe_Q.profile_distance_minimum

                # if the current pipe has a better fit than the previous, store it
                if profile_distance_minimum is None or \
                        profile_distance_current < profile_distance_minimum:
                    profile_distance_minimum = profile_distance_current
                    label_min = label
                    side_min = side
                    self.logger.debug(
                        "updated {} {} {}".format(label_min, side_min, profile_distance_minimum))

                if sorting_method == 0:
                    if profile_distance_minimum <= self.fit_threshold:
                        self.logger.debug(
                            "stop scanning pool since {}. cnt={}".format(label_min, cnt))
                        break

        return label_min, side_min, profile_distance_minimum, label_last, side_last

    def fit_in_trashed_pipes(self, pipe_info_trash_list):
        """
        Try to put all the remaining pipes which were stored in the trashlist into the sequences.
        All that doesn't not fit is put at the end

        Parameters
        ----------
        pipe_info_trash_list: list
            List with the labels of the trashed pipes
        """
        self.logger.debug("putting back {} trashed pipes into the sequence of length {}".format(
            len(pipe_info_trash_list), len(self.pipe_info_df_sorted_list.groupby(level=0))))
        self.logger.debug("initial trashlist {}".format(pipe_info_trash_list))
        pipeinfo_trash_list_new = pipe_info_trash_list
        n_trash_pipes = len(pipe_info_trash_list)
        n_success = 0

        for cnt, label_trash in enumerate(pipe_info_trash_list):

            # create a status bar
            self.progress_changed.emit(cnt, n_trash_pipes,
                                       "Fitting in pipe ({}/{})".format(cnt, n_trash_pipes))
            QApplication.processEvents()

            self.logger.debug("trying to fit in {}".format(label_trash))

            for label, pipe_grouped_df in self.pipe_info_df_sorted_list.groupby(level=0):
                if label not in pipeinfo_trash_list_new:
                    self.logger.debug(
                        "This pipe can be skipped because we found a location already {}".format(
                            label))
                    break

                sides_list = pipe_grouped_df.index.get_level_values('Side')

                # only check on the ends of the pipes in the sorted list
                side = sides_list[1]

                neigh_label = self.pipe_info_df_sorted_list.ix[(label, side), "NeighbourLabel"]
                neigh_side = self.pipe_info_df_sorted_list.ix[(label, side), "NeighbourSide"]

                self.logger.debug(
                    "trashpipe_{} sortlist {}/{} with neigh: {}/{}".format(label_trash,
                                                                           label, side,
                                                                           neigh_label, neigh_side))

                # loop over the two possible starting sides for the trash pipe (you can git it in
                # between the current end and the neighbour in two ways by swapping the pipe ends
                for side1_trash in ["A", "B"]:
                    side2_trash = self.other_side(side1_trash)
                    dist1 = self.pipe_fit_matrix_df.ix[(label, side),
                                                       (label_trash, side1_trash, "FitMinimum")]
                    try:
                        dist2 = self.pipe_fit_matrix_df.ix[(neigh_label, neigh_side),
                                                           (label_trash, side2_trash, "FitMinimum")]
                    except KeyError:
                        # key error indicates that we are at the end of the list, so no neighbours
                        # anymore this releases the requirement that it need to fit at this side,
                        # hence set dist2=0
                        dist2 = 0.0

                    if dist1 <= self.fit_threshold and dist2 <= self.fit_threshold:
                        self.logger.debug("FOUND A BIT FOR {} {}/{} in {}/{} and {}/{}".format(
                            label_trash, side1_trash, side2_trash, label, side, neigh_label,
                            neigh_side))

                        # http://stackoverflow.com/questions/32439853/insert-a-row-in-a-multiindex-data
                        # I want to add a new row to a multindex dataframe, which is not trivial.
                        # See discussion stackoverglow
                        # self.pipe_info_df_sorted_list.unstack()

                        part1df = get_slice(self.pipe_info_df_sorted_list, end_at_label=label_trash)

                        try:
                            # part2df = self.pipe_info_df_sorted_list.loc[neigh_label:].copy()
                            part2df = get_slice(self.pipe_info_df_sorted_list,
                                                start_from_label=neigh_label)
                        except IndexError:
                            part2df = None

                        # addd the trash pipe the the first half
                        part1df.ix[(label_trash, side1_trash), "NeighbourLabel"] = label
                        part1df.ix[(label_trash, side1_trash), "NeighbourSide"] = side

                        # add the other side of the trash pipe. The neighbour may be none in case it
                        # is the end
                        part1df.ix[(label_trash, side2_trash), "NeighbourLabel"] = neigh_label
                        part1df.ix[(label_trash, side2_trash), "NeighbourSide"] = neigh_side

                        # update the the neighbour of the initial end of part1
                        part1df.ix[(label, side), "NeighbourLabel"] = label_trash
                        part1df.ix[(label, side), "NeighbourSide"] = side1_trash

                        if part2df is not None:
                            # append the second part to the end
                            self.pipe_info_df_sorted_list = None
                            self.pipe_info_df_sorted_list = pd.concat([part1df, part2df]).copy()

                            self.logger.debug(
                                "setting new neighbour for start second half {} {} {} {}".format(
                                    neigh_label, neigh_side, label_trash, side2_trash))

                            self.pipe_info_df_sorted_list.ix[
                                (neigh_label, neigh_side), "NeighbourLabel"] = label_trash
                            self.pipe_info_df_sorted_list.ix[
                                (neigh_label, neigh_side), "NeighbourSide"] = side2_trash
                        else:
                            # their are no more pipes to append, so just copy part1df back to the
                            # sorted list
                            self.logger.debug("copy! {}".format(part1df))
                            self.pipe_info_df_sorted_list = part1df.copy()

                        # finally, remove from the trashlist
                        self.logger.debug("remove from list:{}  = {}"
                                          "".format(label_trash, pipeinfo_trash_list_new))

                        pipeinfo_trash_list_new.remove(label_trash)

                        self.logger.debug("remove from list:{}  = {}"
                                          "".format(label_trash, pipeinfo_trash_list_new))

        self.logger.debug("new trashlist {}".format(pipeinfo_trash_list_new))

        return pipeinfo_trash_list_new

    def append_non_fitting_pipes(self, pipeinfo_nonfits):
        """
        Try to put all the remaining pipes which were stored in the trahslist into the sequences.
        All that doesn not fit is put at the end

        Parameters
        ----------
        pipeinfo_nonfits: list
            list with the labels of the trashed pipes
        """
        self.logger.debug("appending {} non-fitting pipes at end of sequence of {}".format(
            len(pipeinfo_nonfits), len(self.pipe_info_df_sorted_list.groupby(level=0))))
        for label in pipeinfo_nonfits:
            for side in ["A", "B"]:
                self.pipe_info_df_sorted_list.ix[(label, side), :] = None

    def get_sorted_pipe_list(self, sorting_method=0, n_pipes_in_pool_minimum=5):
        """
        Return the sorted list of pipes

        Parameters
        ----------
        sorting_method: int , optional
            Pick sorting method. 0=quick sort (oms). 1 = full sort. Default = 0
        n_pipes_in_pool_minimum: int, optional
            Minimum number of pipes required in the pool. Default = 5
        """

        # create a full copy of the pipe_info_df
        pipeinfoDF_copy = self.pipe_info_df.copy()

        # create a empty copy (no columns data, only the index)
        self.pipe_info_df_in_pool = self.pipe_info_df[[]]

        # get the total number of pipes and total number of evaluations for each pipe
        n_pipes = len(self.pipe_info_df_in_pool.groupby(level=0))

        # remove all the pipe in the pool which have a zero number of fits
        pipeinfo_none_fit_list = self.remove_n_zero_fits()

        # start with an empty trash list
        pipeinfo_trash_list = []

        # create an empty dat from to hold a list of the sorted pipes
        self.pipe_info_df_sorted_list = self.create_empty_info_dataframe()

        # start the loop over the pipes in the pool to find a match to the head
        n_pipes_in_pool = n_pipes

        while n_pipes_in_pool > 0:

            # create a status bar
            self.progress_changed.emit((n_pipes - n_pipes_in_pool), n_pipes,
                                       "Sorting pipes ({}/{})".format(n_pipes_in_pool, n_pipes))
            QApplication.processEvents()

            # call the routine to get the next pipe in the sequence
            labelmin, sidemin, normmin, label_last, side_last = self.find_next_pipe_in_sequence(
                sorting_method)

            # a side has been found, find the other side label belong to this pipe
            othersidemin = self.other_side(sidemin)

            if normmin is not None and normmin > self.fit_threshold:
                # this is not the first pipe but the normmin never got below the threshold. Put it
                # in the trash
                self.logger.info("No fit found {} {}/{}".format(normmin, labelmin, sidemin))
                if not (n_pipes_in_pool_minimum < 0) and n_pipes_in_pool > n_pipes_in_pool_minimum:
                    # we still have plenty in the pool, so take out the last from the sorted list
                    self.logger.info("removing last from the list {}".format(label_last))
                    pipeinfo_trash_list.append(label_last)
                    self.pipe_info_df_sorted_list.drop(label_last, level='Label', inplace=True)
                else:
                    # the pool is too small. Eat up the pool (otherwise the last non-fitting pipes
                    # can eat the sorted list
                    self.logger.debug("remove {} from the pool {} pipes"
                                      "".format(labelmin,
                                                len(self.pipe_info_df_in_pool.groupby(level=0))))
                    pipeinfo_trash_list.append(labelmin)
                    self.pipe_info_df_in_pool.drop(labelmin, level='Label', inplace=True)
                    n_pipes_in_pool = len(self.pipe_info_df_in_pool.groupby(level=0))
            else:
                if normmin is None:
                    self.logger.info("Initialise Sequence:")

                self.logger.debug("adding sidemin to sorted list : {} {} with neigh {}/{}".format(
                    labelmin, sidemin, label_last, side_last))
                self.pipe_info_df_sorted_list.ix[(labelmin, sidemin), "NeighbourLabel"] = label_last
                self.pipe_info_df_sorted_list.ix[(labelmin, sidemin), "NeighbourSide"] = side_last

                if label_last is not None and side_last is not None:
                    self.logger.debug("setting last neigh {} {} with neigh {}/{}".format(
                        label_last, side_last, labelmin, sidemin))
                    self.pipe_info_df_sorted_list.ix[
                        (label_last, side_last), "NeighbourLabel"] = labelmin
                    self.pipe_info_df_sorted_list.ix[
                        (label_last, side_last), "NeighbourSide"] = sidemin

                # now copy the back side of the pipe.
                self.logger.debug(
                    "adding the other sideO #to sorted list : {} {}".format(labelmin, othersidemin))
                self.pipe_info_df_sorted_list.loc[(labelmin, othersidemin), "NeighbourLabel"] = None
                self.pipe_info_df_sorted_list.loc[(labelmin, othersidemin), "NeighbourSide"] = None

                self.logger.debug("remove from the pool {} with {} pipes"
                                  "".format(labelmin,
                                            len(self.pipe_info_df_in_pool.groupby(level=0))))
                self.pipe_info_df_in_pool.drop(labelmin, level='Label', inplace=True)
                n_pipes_in_pool = len(self.pipe_info_df_in_pool.groupby(level=0))

        # done with building the pipe list
        self.n_trashed_pipes = len(pipeinfo_trash_list)

        self.logger.debug("Number of trashed before fit-in: {}".format(self.n_trashed_pipes))
        pipeinfo_trash_list = self.fit_in_trashed_pipes(pipeinfo_trash_list)
        self.n_restored_pipes = self.n_trashed_pipes - len(pipeinfo_trash_list)
        self.logger.debug("Number of restored pipes : {}".format(self.n_restored_pipes))

        # now append the pipes in the trahs list that could not be fitted in
        self.append_non_fitting_pipes(pipeinfo_trash_list)

        # finally, append the pipes which had a zero fit already
        self.n_non_fitting_pipes = len(pipeinfo_none_fit_list)
        self.append_non_fitting_pipes(pipeinfo_none_fit_list)

        # clear all the items from pipeinfo
        for label, pipeDF_grouped in self.pipe_info_df.groupby(level=0):
            self.pipe_info_df.drop(label, level='Label', inplace=True)

        # Copy the sorted list back to the clear pipeinfo data frame
        for cnt, (label, side) in enumerate(self.pipe_info_df_sorted_list.index):
            if cnt % 2 == 0:
                self.progress_changed.emit(cnt, 2 * n_pipes,
                                           "Copying sorted pipe ({}/{})".format(int(cnt / 2),
                                                                                n_pipes))
                QApplication.processEvents()
            self.pipe_info_df.ix[(label, side), :] = pipeinfoDF_copy.ix[(label, side), :]


class PipeOptimise(object):
    """

    Class to carry out the optimization of two pipe profiles

    Parameters
    ----------
    Ri1: ndarray, optional
        Radial profile of first pipe
    Ri2: ndarray, optional
        Radial profile of second pipe
    npow: int, optional
        Power used for difference profile
    criterion: int, optional, optional
        Type of difference measurement
    profile_distance_minimum:  float, optional
        Current minimum distance between two profiles. Default = None
    profile_distance_maximum: float, optional
        Current maximum distance between two profiles. Default = None
    profile_distance_current:
        Current distance between profiles
    phi_optimal: float, optional
        Optimal rotation between pipes. Default = 0
    phi_cumulative: float, optional
        Cumulative rotation of current pipe . Default = 0
    exclude_phi_range_weld: float, optional
        Exclude this phi range around the weld for the optimization. Default = 0
    exclude_phi_range_six_oclock: float, optional
        Exclud this phi range around the other side of the weld for the optimization. Default = 0
    shift_slice: Point
        Shift the second slice before matching

    Notes
    -----
    * Get the minimal distance between the radial profiles of two pipes by rotating phi of index
      k and calculating the sum square using the linalg.norm routine which give the norm of the
      difference vector if use hi low is true: use that as a criterion
    * The profile given by Ri2 may be shifted over the *shift_slice* distance before calculating
      the fir quality of the profiles
    """

    def __init__(self,
                 Ri1=None,
                 Ri2=None,
                 n_pow=1,
                 criterion=0,
                 profile_distance_minimum=None,
                 profile_distance_maximum=None,
                 profile_distance_current=None,
                 phi_optimal=0,
                 phi_cumulative=0,
                 exclude_phi_range_weld=0.0,
                 exclude_phi_range_six_oclock=0.0,
                 shift_slice=Point(0, 0)
                 ):

        # get the logger
        self.logger = log.getLogger(__name__)

        self.shift_slice = shift_slice

        # set the profiles
        if Ri1 is not None:
            self.Ri1 = Ri1.copy()
        else:
            self.Ri1 = None
        if Ri2 is not None:
            self.Ri2 = Ri2.copy()
        else:
            self.Ri2 = None

        if shift_slice.distance(Point(0, 0)) > 1e-9 and self.Ri2 is not None:
            self.Ri2.values[:] = shift_radial_profile(Ri2.index, Ri2.values,
                                                      shift_y=shift_slice.x,
                                                      shift_z=shift_slice.y)

        self.phi_cumulative = phi_cumulative
        self.exclude_phi_range_six_oclock = exclude_phi_range_six_oclock
        self.exclude_phi_range_weld = exclude_phi_range_weld

        self.kmin = 0
        self.kmax = 0

        self.phi_optimal = phi_optimal
        self.n_pow = n_pow
        self.criterion = criterion

        self.profile_distance_minimum = profile_distance_minimum
        self.profile_distance_maximum = profile_distance_maximum
        self.profile_distance_current = profile_distance_current

        self.delta_radius_below_threshold = None

        # this array is used to store all the profiles distance vs phi for later usage
        try:
            self.nk = len(self.Ri1.index)
            self.hilo_vs_phi = np.zeros(self.nk)
        except AttributeError:
            self.nk = 0
            self.hilo_vs_phi = None

        try:
            # get the distance phi between two sample points
            delta_phi = self.Ri1.index[1] - self.Ri1.index[0]
        except AttributeError:
            self.logger.debug("no profiles passed at initialisation")
        else:
            # determine the start and end index in order to exclude the phi range
            self.k_start = self.phi_to_k(exclude_phi_range_weld, delta_phi)
            self.k_end = self.phi_to_k(-exclude_phi_range_weld, delta_phi)
            if self.k_end == 0:
                self.k_end = self.nk

            # calculate the start and end of the gap around the six oclock position of the tube and
            # exclude this gap from the allowed phi angles
            self.k_gap_start = self.phi_to_k(-phi_cumulative + np.pi - exclude_phi_range_six_oclock,
                                             delta_phi)
            self.k_gap_end = self.phi_to_k(-phi_cumulative + np.pi + exclude_phi_range_six_oclock,
                                           delta_phi)

            self.logger.debug("phi(k=0): {}  phi(k=1) : {} phi(k=end): {}  and delta_phi {}".format(
                self.Ri1.index[0], self.Ri1.index[1], self.Ri1.index[-1], delta_phi))
            self.logger.debug("k_start : {}   k_end : {} with k_max = {} for {}".format(
                self.k_start, self.k_end, len(self.Ri1.index), exclude_phi_range_weld))
            self.logger.debug("k_gap_start : {}   k_gap_end : {} for {}".format(
                self.k_gap_start, self.k_gap_end, exclude_phi_range_six_oclock))

        # initialise the optimizer
        if Ri1 is not None and Ri2 is not None:
            self.initialise_profile_distance()
            self.logger.debug("Initial profile distance : {}".format(self.profile_distance_current))
        else:
            self.logger.warning(
                "At least one of the two profiles is invalid. Skipping profile optimisation")

    @staticmethod
    def phi_to_k(phi, delta_phi):

        """
        Convert a phi angle (radians) to a k index.

        Parameters
        ----------
        phi: float
            Current angle in radians
        delta_phi: float
            Step size in phie space

        Returns
        -------
        int
            Index of position in array belonging to phi

        Notes
        -----
        First makes sure that the phi is in the range 0~2pi
        """

        phi %= (2 * np.pi)

        k_index = int(round(phi / delta_phi))

        return k_index

    def initialise_profile_distance(self):

        # the initialise current profile distance without any shift over the axis
        self.profile_distance_current = self.get_profile_distance(self.k_start)

        # initialise the current extremes
        self.profile_distance_maximum = self.profile_distance_minimum = \
            self.profile_distance_current
        self.kmin = self.kmax = self.k_start

        # make sure phi is in range 0~2*p
        self.phi_optimal = self.Ri2.index[self.kmin] % (2 * np.pi)

    def get_profile_distance(self, k):
        """
        Get the scalar value representing the distance for two profile under a shift k

        Parameters
        ----------
        k: int
            number of position to shift the profiles

        Returns
        -------
        float:
            The distance between the profiles


        """
        if self.criterion > 0:
            # use the norm ie: sqrt(sum_i (Ri1(i)**2+Ri2(i))
            # increasing n_pow puts more weight to large deviations of the mean
            norm = np.linalg.norm(self.Ri1 - np.roll(self.Ri2, k))

            if self.criterion > 1:
                # for criterion ==2: use the norm squared
                norm *= norm
        else:
            # criterion = 0 -> use HiLow
            norm = np.max(np.abs(self.Ri1 - np.roll(self.Ri2, k)))
        return norm

    def get_profile_distance_extremes(self):
        """
        Shift the second profile of all k's and get the profile distance.

        Notes
        -----
        * The extreme values a range around zero
        * phi (-/+ exclude_phi_range_weld) can be exclude to prevent that two pipe have a
          zero rotation (two weld on top of each other)
        * Also, the position of weld at the bottom is not allowed
        """

        for k in range(self.nk):

            # calculate the current profile distance and store it in the hilo_vs_phi array
            norm = self.get_profile_distance(k)
            self.hilo_vs_phi[k] = norm

            # k values in the gap at phi=zero: skip it. We can also skip the k=k_start because this
            # is already initialised in the method initialise_profile_distance
            if k <= self.k_start or k >= self.k_end:
                continue

            if (k > self.k_gap_start) and (k < self.k_gap_end):
                # this k corresponds with the six o'clock position, so skip it
                continue

            if norm < self.profile_distance_minimum:
                self.kmin = k
                self.phi_optimal = self.Ri2.index[self.kmin] % (2 * np.pi)  # put phi  in  0~2*pi
                self.profile_distance_minimum = norm
            elif norm > self.profile_distance_maximum:
                self.kmax = k
                self.profile_distance_maximum = norm

        self.logger.debug("Found optimal profile at kmin={} distance={}  phi_optimal={}".format(
            self.kmin, self.profile_distance_minimum, self.phi_optimal))

    def set_phi_range_around_optimum(self, threshold):
        """
        Set the range around the phi optimum

        Parameters
        ----------
        threshold: float
            Threshold below which we are condisered to have a fit

        Returns
        -------
        float
            Width of phi around the optimum
        """

        delta_hilo_ok = None

        if self.hilo_vs_phi is not None:
            self.delta_radius_below_threshold = self.hilo_vs_phi <= threshold
            # set the values inside the weld range and the 6-oclock range to false
            self.delta_radius_below_threshold[:self.k_start] = False
            self.delta_radius_below_threshold[self.k_end:] = False
            self.delta_radius_below_threshold[self.k_gap_start:self.k_gap_end] = False

            # roll the array with its best fit to zero
            below_threshold_centered = np.roll(self.delta_radius_below_threshold, self.kmin)
            diff = np.diff(below_threshold_centered)
            # the range of the phi are given by the first True to the left and the first true
            # to the right
            k_start = np.argmax(diff[::-1] > 0)
            k_end = np.argmax(diff > 0)
            if any(self.delta_radius_below_threshold):
                k_width = k_start + k_end
                delta_hilo_ok = 360 * k_width / self.hilo_vs_phi.size
            else:
                delta_hilo_ok = 0

        return delta_hilo_ok
