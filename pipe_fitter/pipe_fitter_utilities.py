__author__ = 'eelco'

import numpy as np
import pandas as pd
from scipy.interpolate import interp1d

from hmc_utils.coordinate_transformations import (polar_to_cartesian, cartesian_to_polar)
from hmc_utils.numerical import extrap1d


def get_slice(dataframe, start_from_label=None, end_at_label=None):
    # create a empty data frame and initialise with rows copy from the dataframe starting from
    # start_from_label and ending at end_at_label
    mi = pd.MultiIndex.from_tuples([tuple([None, None])], names=dataframe.index.names)

    df_new = pd.DataFrame(index=mi, columns=[])
    df_new.drop(np.nan, level='Label', inplace=True)

    insert = False
    for label, df in dataframe.groupby(level=0):
        side_list = df.index.get_level_values('Side')
        if start_from_label is None or label == start_from_label:
            insert = True
        if insert:
            for side in side_list:
                for col in dataframe.columns:
                    df_new.ix[(label, side), col] = dataframe.ix[(label, side), col]

        if end_at_label is not None and label == end_at_label:
            break

    return df_new


def shift_radial_profile(phi, radius, shift_y=0, shift_z=0):
    """
    Shift the radial profile to a new position and return the new radial profile

    Parameters
    ----------
    phi: ndarray
        Array with the original angles phi
    radius: ndarray
        Array with the original radius
    shift_y: float
        Shift in y direction
    shift_z: float
        Shift in z direction

    Returns
    -------
    ndarray
        Radial profile shift over shift_y, shift_z

    Notes
    -----
    * The radial profile is passed as a series of radii and phis. First convert to cartesian
      coordinates to shift the profile, then convert back to polar coorddinates
    * The shifted profile as new phi-positions. In order to get back the old phi-position,
      interpolated the new radial

    """

    # convert the profile to cartesian coordinates and shift over shift_y, shift_z
    yy, zz = polar_to_cartesian(radius, phi)
    yy += shift_y
    zz += shift_z

    # convert the profile back to polar coordinates
    rr, pp = cartesian_to_polar(yy, zz)

    # make sure that we have p in a range 0~2pi (not -pi~pi)
    pp = np.where(pp < 0, pp + np.full(shape=pp.shape, fill_value=2 * np.pi), pp)

    # sort the new phi, radius combinations such that we have a monotonically increasing phi
    # this is because it may happen that the first point right of positive y-axis (we are
    # looking at the y-z plane) has a phi>0, but after the shift it may be located left of the
    # positive y-axis with a phi < 0. Note that y > 0 is pointing upward, z>0 is to the right,
    # because x> 0 is the axis along the pipe is always pointing away from us
    # Because we have ensured phi in the range 0~2pi,
    # this point will have a value of 2pi - delta_phi, but is still the first in the list of
    # phi-radius. By sorting the radii vs phi, this point will move to the end of the list
    df = pd.DataFrame(index=pp, data=rr, columns=["radius"])
    df.index.name = "Phi"
    df.sort_index(inplace=True)
    pp = df.index.values
    rr = df.radius.values

    # interpolate the new phi and radius combination on the old phi locations
    f_inter = interp1d(pp, rr)
    f_extra = extrap1d(f_inter)

    # here we calculate the new radii for the old phi sample points, such that we can calculate
    # the distance between 2 profiles by subtracting r1 - r2 for the same phi value
    radius_new = f_extra(phi)

    return radius_new
