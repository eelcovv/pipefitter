__author__ = 'eelcovv'
# this dialog will plot the slices

import logging
import os
import re
import sys

import pandas as pd

from pipe_fitter.pyqt_import import *

from hmc_utils.misc import get_regex_pattern


class ScanImportDlg(QDialog):

    def __init__(self, fitter, show_batch_table, parent=None):
        super(ScanImportDlg, self).__init__(parent)

        # make sure the dialog is close (not hidden)
        # actually, I want to hide it
        # self.setAttribute(QtCore.Qt.WA_DeleteOnClose)

        settings = QtCore.QSettings()

        # get the last values stored in default settings
        try:
            file_type_index = int(settings.value("ScanImportDlg/file_type_index",
                                                 QtCore.QByteArray()))
        except ValueError:
            file_type_index = 0
        try:
            max_depth = int(settings.value("ScanImportDlg/max_depth", QtCore.QByteArray()))
        except ValueError:
            max_depth = 1

        has_string = settings.value("ScanImportDlg/has_string", QtCore.QTextStream())
        try:
            has_string = has_string.readAll()
        except AttributeError:
            pass

        has_not_string = settings.value("ScanImportDlg/has_not_string", QtCore.QTextStream())
        try:
            has_not_string = has_not_string.readAll()
        except AttributeError:
            pass

        try:
            skip_processed = bool(settings.value("ScanImportDlg/skip_processed",
                                                 QtCore.QByteArray()))
        except ValueError:
            skip_processed = True

        pipe_base_name = settings.value("ScanImportDlg/pipe_base_name", QtCore.QTextStream())
        try:
            pipe_base_name.readAll()
            pipe_base_name = "\w+?_\d+"
        except AttributeError:
            pass

        default_slice_name = settings.value("ScanImportDlg/default_slice_name",
                                            QtCore.QTextStream())
        try:
            default_slice_name.readAll()
            default_slice_name = "slice_p000060"
        except AttributeError:
            pass

        pipe_side_extension = settings.value("ScanImportDlg/pipe_side_extension",
                                             QtCore.QTextStream())
        try:
            pipe_side_extension.readAll()
            pipe_side_extension = "A|B"
        except AttributeError:
            pass

        self.fitter = fitter

        self.show_batch_table = show_batch_table

        self.batchseries = None

        self.import_file_dataframe = None

        # the selected directory for scanning all the stl files
        self.walk_dir = None

        self.logger = logging.getLogger(__name__)

        # a start button
        self.buttonExecute = QPushButton("&Execute")
        # a start button
        self.buttonStop = QPushButton("&Stop")

        # a save button to save the image
        self.buttonScanDir = QPushButton("S&can directory")

        self.buttonApplyFilters = QPushButton("&Apply Filters")

        # a button to read the slices of the current list
        self.buttonGetSlices = QPushButton("&Read Slice Names")

        auto_apply_layout = QHBoxLayout()
        auto_apply_filters = QLabel("&Auto Apply Filters")
        self.auto_apply_checkBox = QCheckBox()
        auto_apply_filters.setBuddy(self.auto_apply_checkBox)
        self.auto_apply_checkBox.setToolTip("Automatically apply the filter settings")
        self.auto_apply_checkBox.setStatusTip(self.auto_apply_checkBox.toolTip())
        self.auto_apply_checkBox.setChecked(False)
        auto_apply_layout.addWidget(auto_apply_filters)
        auto_apply_layout.addWidget(self.auto_apply_checkBox)

        file_type_label = QLabel("&File Type:")
        self.file_type_combobox = QComboBox()
        for file_type in ["xls", "csv"]:
            self.file_type_combobox.addItem(file_type)
        self.file_type_combobox.setCurrentIndex(file_type_index)
        file_type_label.setBuddy(self.file_type_combobox)
        self.file_type_combobox.setToolTip("Type of the scan files to read")
        self.file_type_combobox.setStatusTip(self.file_type_combobox.toolTip())

        max_depth_label = QLabel("&Maximum Search Depth:")
        self.max_depth_spinbox = QSpinBox()
        max_depth_label.setBuddy(self.max_depth_spinbox)
        self.max_depth_spinbox.setRange(0, 999)
        self.max_depth_spinbox.setValue(max_depth)
        self.max_depth_spinbox.setToolTip("Limit the scan depth")
        self.max_depth_spinbox.setStatusTip(self.max_depth_spinbox.toolTip())
        self.max_depth_spinbox.setFocusPolicy(QtCore.Qt.NoFocus)

        has_string_label = QLabel("&Include Pattern:")
        self.has_string_editline = QLineEdit()
        has_string_label.setBuddy(self.has_string_editline)
        self.has_string_editline.setText(has_string)
        self.has_string_editline.setToolTip("The file name should contain")
        self.has_string_editline.setStatusTip(self.has_string_editline.toolTip())

        has_not_string_label = QLabel("&Exclude Pattern:")
        self.has_not_string_editline = QLineEdit()
        self.has_not_string_editline.setText(has_not_string)
        has_not_string_label.setBuddy(self.has_not_string_editline)
        self.has_not_string_editline.setToolTip("The file name should contain")
        self.has_not_string_editline.setStatusTip(self.has_not_string_editline.toolTip())

        skip_checkbox_layout = QHBoxLayout()
        skip_already_processed_label = QLabel("&Skip Processed:")
        self.skip_processed_checkBox = QCheckBox()
        skip_already_processed_label.setBuddy(self.skip_processed_checkBox)
        self.skip_processed_checkBox.setToolTip("Skip cases which are already processed (contain a "
                                                ".xls output")
        self.skip_processed_checkBox.setStatusTip(self.skip_processed_checkBox.toolTip())
        self.skip_processed_checkBox.setChecked(skip_processed)
        skip_checkbox_layout.addWidget(skip_already_processed_label)
        skip_checkbox_layout.addWidget(self.skip_processed_checkBox)

        pipe_base_name_label = QLabel("Pipe &Basename:")
        self.pipe_base_name_edit = QLineEdit()
        self.pipe_base_name_edit.setText(pipe_base_name)
        pipe_base_name_label.setBuddy(self.pipe_base_name_edit)
        self.pipe_base_name_edit.setToolTip(
            "Regular expression to specify the base part of the pipe name")
        self.pipe_base_name_edit.setStatusTip(self.pipe_base_name_edit.toolTip())

        pipe_side_extension_label = QLabel("&Side Extension:")
        self.pipe_side_extension_edit = QLineEdit()
        self.pipe_side_extension_edit.setText(pipe_side_extension)
        pipe_side_extension_label.setBuddy(self.pipe_side_extension_edit)
        self.pipe_side_extension_edit.setToolTip(
            "Regular expression to specify the extension form of the pipe side")
        self.pipe_side_extension_edit.setStatusTip(self.pipe_side_extension_edit.toolTip())

        default_slice_name_label = QLabel("&Default Slice Name:")
        self.default_slice_name_edit = QLineEdit()
        self.default_slice_name_edit.setText(default_slice_name)
        default_slice_name_label.setBuddy(self.default_slice_name_edit)
        self.default_slice_name_edit.setToolTip("Initialise the slices with this slice name")
        self.default_slice_name_edit.setStatusTip(self.default_slice_name_edit.toolTip())

        vertical_line = QFrame()
        vertical_line.setFrameStyle(QFrame.HLine)
        vertical_line.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)

        # a verical layout to contain the spinbox, combo box and two buttons
        buttonLayout = QVBoxLayout()
        buttonLayout.addWidget(self.buttonScanDir)
        buttonLayout.addWidget(self.buttonApplyFilters)
        buttonLayout.addLayout(auto_apply_layout)
        buttonLayout.addWidget(file_type_label)
        buttonLayout.addWidget(self.file_type_combobox)
        buttonLayout.addWidget(max_depth_label)
        buttonLayout.addWidget(self.max_depth_spinbox)
        buttonLayout.addWidget(has_string_label)
        buttonLayout.addWidget(self.has_string_editline)
        buttonLayout.addWidget(has_not_string_label)
        buttonLayout.addWidget(self.has_not_string_editline)
        buttonLayout.addLayout(skip_checkbox_layout)

        buttonLayout.addWidget(vertical_line)
        buttonLayout.addWidget(pipe_base_name_label)
        buttonLayout.addWidget(self.pipe_base_name_edit)
        buttonLayout.addWidget(pipe_side_extension_label)
        buttonLayout.addWidget(self.pipe_side_extension_edit)
        buttonLayout.addWidget(default_slice_name_label)
        buttonLayout.addWidget(self.default_slice_name_edit)
        buttonLayout.addWidget(self.buttonGetSlices)

        buttonLayout.addStretch()
        buttonLayout.addWidget(self.buttonExecute)
        buttonLayout.addWidget(self.buttonStop)

        self.table = QTableWidget()
        self.table.setAlternatingRowColors(True)

        self.table.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.table.customContextMenuRequested.connect(self.handle_table_menu)

        graphLayout = QVBoxLayout()
        graphLayout.addWidget(self.table)

        # a horizontal layout to store the canvas left and button etc right
        # the integer 3 and 1 give the relative sizes of the left and right part
        layout = QHBoxLayout()
        layout.addLayout(graphLayout, 10)
        layout.addLayout(buttonLayout, 1)

        self.setLayout(layout)

        self.buttonScanDir.clicked.connect(self.scan_directory)
        self.buttonGetSlices.clicked.connect(self.get_slices)
        self.buttonApplyFilters.clicked.connect(self.apply_scan_directory)
        self.buttonExecute.clicked.connect(self.execute_batch)
        self.buttonStop.clicked.connect(self.stop_execution_batch)
        self.has_string_editline.textEdited[str].connect(self.update_file_scan)
        self.has_not_string_editline.textEdited[str].connect(self.update_file_scan)
        self.max_depth_spinbox.valueChanged[int].connect(self.update_file_scan)
        self.skip_processed_checkBox.toggled[bool].connect(self.update_file_scan)

        self.restoreGeometry(settings.value("ScanImportDlg/Geometry",
                                            QtCore.QByteArray()))
        self.setWindowTitle("Batch Processing Dialog")

    def set_row_color(self, row):
        item = QTableWidgetItem("Text")
        item.setBackground(QColor(255, 0, 0))
        self.table.setHorizontalHeaderItem(row, item)
        self.logger.debug("coloring row {}".format(row))
        self.updateTable()

    def update_file_scan(self):
        if self.auto_apply_checkBox.isChecked():
            self.scan_directory(update_current_walk_dir=True)

    def apply_scan_directory(self):
        self.scan_directory(update_current_walk_dir=True)

    def handle_table_menu(self):
        """
        handle here the table menu item after a right mouse button clock
        :return:
        """

        menu = QMenu()

        text = menu.addAction("Pipe Actions:")
        text.setEnabled(False)
        menu.addSeparator()

        # two menu items
        action_reload = menu.addAction('Reload Slice Info')
        action_import = menu.addAction('Import Slice Data')

        # chatch the right mouse button action
        action = menu.exec_(QCursor.pos())

        # do the requested action
        if action == action_reload:
            # pop up window with details
            self.reload_slice_info()
        elif action == action_import:
            self.import_this_slice()
        else:
            self.logger.warning("something is wrong in context menu")

    def scan_directory(self, update_current_walk_dir=False):
        """
        Scans the selector directory and retrieves all stl file. Copy the list to a
        import_file_dataframe and filter it
        :return:
        """

        if self.walk_dir is None:
            settings = QtCore.QSettings()
            self.walk_dir = settings.value("BatchProcessDlg/LastDirectory")
            self.logger.debug("setting last dir loading {}".format(self.walk_dir))
        else:
            self.logger.debug("last dir is already -{}-".format(self.walk_dir))

        if not update_current_walk_dir:
            # open the directory dialog only if we are not updating a field

            self.walk_dir = QFileDialog.getExistingDirectory(self, "Select Directory",
                                                                   self.walk_dir)

        self.setWindowTitle("Batch Processing Dialog - Directory {}".format(self.walk_dir))

        self.logger.debug("scanning directory {}".format(self.walk_dir))

        # get the filter strings
        has_string = get_regex_pattern(self.has_string_editline.text())
        self.logger.debug("{} and {}".format(self.has_string_editline.text(), has_string))
        has_not_string = get_regex_pattern(self.has_not_string_editline.text())

        # loop over all directories and sub directories
        file_list = []
        below_scan_depth = False
        for root, subdirs, files in os.walk(self.walk_dir, topdown=True):
            self.logger.debug("root={}  sub={} files={}".format(root, subdirs, files))
            # get the relative path towards the top directory (walk_dir)
            relative_path = os.path.relpath(root, self.walk_dir)

            # count the number of separators in the current path
            number_of_sep = relative_path.count(os.path.sep)

            if (self.max_depth_spinbox.value() == 0) or \
                    (relative_path != "." and number_of_sep == self.max_depth_spinbox.value() - 1):
                # stop scanning as soon as the max depth is either 0 (only current directory or if
                # the current directory is not . (top dir) and the number of separators equals the
                # max depth -1
                subdirs[:] = []

            # loop over the file in the current subdirectory
            for filename in files:

                (filebase, extension) = os.path.splitext(filename)
                self.logger.debug("checking {} {}".format(filename, extension))
                # if the extension matches .stl, add the relative path to the list
                file_extension = ".{}$".format(self.file_type_combobox.currentText())
                if re.match(file_extension, extension):

                    add_file = False

                    if has_string is None or \
                            bool(has_string.search(os.path.join(relative_path, filename))):
                        # if has_string is none, the search pattern was either empty or invalid
                        # (which happens during typing the regex in the edit_box). In this case,
                        # always add the file. If not none, filter on the regex, so only add the
                        # file if the search pattern is in the filename
                        add_file = True

                    # do not add the file in case the has_not string edit has been set (!="") and
                    # if the file contains the pattern
                    if has_not_string is not None:
                        if bool(has_not_string.search(os.path.join(relative_path, filename))):
                            # in case we want to exclude the file, the has_not search pattern must
                            # be valid so may not be None
                            add_file = False

                    # create the full base name file
                    file_name_to_add = os.path.join(self.walk_dir, relative_path, filebase)

                    # get the path to the stl relative to the selected scan directory
                    if add_file:
                        file_list.append(file_name_to_add + extension)

        file_list.sort()
        # copy the scanned file list to a pandas data frame. Apply the string filter
        self.import_file_dataframe = pd.DataFrame(
            [[0, 15, None, None, None, None] for file in file_list],
            columns=["Status", "slice_name", "ExcelObject", "pipe_id", "pipe_mid_section",
                     "pipe_side_id"],
            index=file_list)

        # analyse the pipe names to extract the pipe id from the name
        self.get_pipe_id()

        # call the get slices routine to fill in the slice names
        self.get_slices()

        self.logger.debug("data {}".format(self.import_file_dataframe.head()))
        # update the table widget
        self.updateTable()

    def get_pipe_id(self):
        """
        In this routine the id of the pipe name is obtained by applying the regular expression given
        in dialog
        """
        if self.pipe_base_name_edit.text() == "":
            # in case the pipe_base_name dialog is empty, fill in the default match pattern
            # (assume all )
            base_pattern = ".*"
        else:
            base_pattern = self.pipe_base_name_edit.text()

        # build the regular expression based on the the pipe_base_name and the pipe_side, giving the
        # start of the string and the end of the string. In between, something else may be present,
        # which is given by (.*?)
        reg_ext_str = "({})(.*?)({})$".format(self.pipe_base_name_edit.text(),
                                              self.pipe_side_extension_edit.text())
        self.logger.debug("pat 1 : {}".format(self.pipe_base_name_edit.text()))
        self.logger.debug("pat 2 : {}".format(self.pipe_side_extension_edit.text()))
        self.logger.debug("reg ext string: {}".format(reg_ext_str))

        # build the regular expression
        pipe_regex = get_regex_pattern(reg_ext_str)

        # loop over all the stl_file names and match the name against the regular expression
        for row, stl_file in enumerate(self.import_file_dataframe.index.tolist()):
            path, filename = os.path.split(stl_file)
            filebase, ext = os.path.splitext(filename)
            self.logger.debug("Test file base {}".format(filebase))
            match = False
            try:
                match = pipe_regex.search(filebase)
            except AttributeError:
                self.logger.debug("failed with the regexp on pipe_id")

            # in case a match was found we can extract the pipe_id, middle part, pipe extensio
            if bool(match):
                try:
                    self.import_file_dataframe.ix[stl_file, "pipe_id"] = match.group(1)
                    self.import_file_dataframe.ix[stl_file, "pipe_mid_section"] = match.group(2)
                    self.import_file_dataframe.ix[stl_file, "pipe_side_id"] = match.group(3)
                    self.logger.debug("Set pipe_id = {} mid_id = {} side_id = {}"
                                      "".format(match.group(1), match.group(2), match.group(3)))
                except IndexError:
                    self.logger.warning(
                        "Something went wrong with the match group of file {}".format(filebase))
            else:
                self.logger.debug("no match found")

    def get_slices(self):
        """
        Read all the excel files in the current list to get the slices available.

        Notes
        the slice names are based on the names in the first data file only. The rest of the names
        are copies
        """
        xl = None
        for row, stl_file in enumerate(self.import_file_dataframe.index.tolist()):
            # get the current data frame excel object

            file_base, file_extension = os.path.splitext(stl_file)

            # make sure that this slice is imported again
            self.import_file_dataframe.ix[stl_file, "Status"] = 0

            self.import_file_dataframe.ix[stl_file, "ExcelObject"] = None

            if file_extension == ".xls":
                if xl is None:
                    # in case the excel  object is none or has a list of sheet names of length 1,
                    # read the excel data
                    try:
                        xl = pd.ExcelFile(stl_file)
                        self.logger.debug(xl.sheet_names)
                    except IOError:
                        self.logger.warning("Something went wrong here for {}".format(stl_file))
                self.import_file_dataframe.ix[stl_file, "ExcelObject"] = xl

        self.updateTable()

    def updateTableStatus(self):
        brushes = []
        brushes.append(QBrush(QColor(0, 0, 0)))
        brushes.append(QBrush(QColor(200, 0, 0)))
        brushes.append(QBrush(QColor(0, 200, 0)))
        brushes.append(QBrush(QColor(0, 0, 200)))
        for row, stl_file in enumerate(self.import_file_dataframe.index.tolist()):
            item = QTableWidgetItem(stl_file)
            item.setData(QtCore.Qt.UserRole, id(stl_file))
            item.setForeground(brushes[self.import_file_dataframe.ix[stl_file].Status])
            self.table.setItem(row, 0, item)

    def set_tabel_column_string(self, i_row, col, string):
        # fill in a data field of the table
        try:
            item = QTableWidgetItem(string)
            # align the numbers to the right
            item.setTextAlignment(QtCore.Qt.AlignRight)
            self.table.setItem(i_row, col, item)
        except (AttributeError, KeyError):
            pass

    def updateTable(self, current=None):

        self.table.clear()
        self.table.setRowCount(len(self.import_file_dataframe.index))
        self.table.setColumnCount(4)
        self.table.setHorizontalHeaderLabels(["File", "PipeID", "Side", "Slice"])
        self.table.setAlternatingRowColors(True)
        self.table.setEditTriggers(QTableWidget.NoEditTriggers)
        self.table.setSelectionBehavior(QTableWidget.SelectRows)
        self.table.setSelectionMode(QTableWidget.SingleSelection)
        selected = None
        for row, stl_file in enumerate(self.import_file_dataframe.index.tolist()):
            # get the current row from the table
            item = QTableWidgetItem(stl_file)
            if current is not None and current == id(stl_file):
                selected = item

            # set the stl_file in the table
            item.setData(QtCore.Qt.UserRole, id(stl_file))
            self.table.setItem(row, 0, item)

            self.set_tabel_column_string(row, 1, self.import_file_dataframe.ix[stl_file, "pipe_id"])
            self.set_tabel_column_string(row, 2, self.import_file_dataframe.ix[stl_file,
                                                                               "pipe_side_id"])

            # set the combobox with the slice names in the table:w
            combobox = QComboBox()
            inx = 0
            try:
                for cnt, slice_name in enumerate(
                        self.import_file_dataframe.ix[stl_file, "ExcelObject"].sheet_names):
                    if slice_name == self.default_slice_name_edit.text():
                        # in case there is a slice with the current default name, store its index
                        inx = cnt
                    combobox.addItem(slice_name)
                # set the current slice to its default
                combobox.setCurrentIndex(inx)
                combobox.currentIndexChanged.connect(self.combo_changed)

            except AttributeError:
                self.logger.debug("No list, setting {}".format(self.default_slice_name_edit.text()))
                combobox.addItem(self.default_slice_name_edit.text())

            self.table.setCellWidget(row, 3, combobox)
            self.import_file_dataframe.ix[stl_file, "slice_name"] = combobox.currentText()

        self.table.resizeColumnsToContents()
        if selected is not None:
            selected.setSelected(True)
            self.table.setCurrentItem(selected)
            self.table.scrollToItem(selected)

    def combo_values_to_dataframe(self):
        """
        Loop over all the rows of the table and get for each file the current slice name from the
        combo box
        """
        for row, stl_file in enumerate(self.import_file_dataframe.index.tolist()):
            combobox = self.table.cellWidget(row, 1)
            slice_name = combobox.currentText()
            self.import_file_dataframe.ix[stl_file, "slice_name"] = slice_name

    def combo_changed(self, value):
        """
        Connect to signal of combobox in the table.

        Notes
        -----
        Since I did not find how to add an extra argument, I just call the method
        combo_values_to_dataframe in order to loop over the whole table and copy all values
        """
        self.logger.debug("Calling for slice {} ".format(value))
        self.combo_values_to_dataframe()

    def execute_batch(self):
        self.logger.debug("Execute the batch")

        for row, stl_file in enumerate(self.import_file_dataframe.index.tolist()):
            if self.import_file_dataframe.ix[stl_file, "Status"] != 2:
                # import this file
                self.import_file_dataframe.ix[stl_file, "Status"] = 0

        self.buttonExecute.setEnabled(False)
        self.buttonScanDir.setEnabled(False)
        self.fitter.start_import_data.emit(self.import_file_dataframe)
        QApplication.processEvents()

    def stop_execution_batch(self):
        self.logger.debug("Stop Executing the batch")

        self.buttonExecute.setEnabled(True)
        self.buttonScanDir.setEnabled(True)

        self.fitter.stop_batch_execute.emit()
        QApplication.processEvents()

    def reload_slice_info(self):

        stl_file = self.current_slice()

        try:
            xl = pd.ExcelFile(stl_file)
            self.logger.debug(xl.sheet_names)
            self.import_file_dataframe.ix[stl_file, "ExcelObject"] = xl
        except IOError:
            self.logger.warning("Something went wrong here for {}".format(stl_file))

    def import_this_slice(self):

        current = self.current_slice()
        for row, stl_file in enumerate(self.import_file_dataframe.index.tolist()):
            if stl_file == current:
                # import this file so set status to 0
                self.import_file_dataframe.ix[stl_file, "Status"] = 0
            else:
                # skip this file so set file to 3
                self.import_file_dataframe.ix[stl_file, "Status"] = 3

        self.fitter.start_import_data.emit(self.import_file_dataframe)
        QApplication.processEvents()

    def current_slice(self):
        """
        returns a tubple of the current pipe and side
        """
        row = self.table.currentRow()
        if row > -1:
            filename = self.table.item(row, 0)
            return filename.data(0)
        return None

    def update_logger(self, level):
        self.logger.setLevel(level)

    def closeEvent(self, event):
        # before closing the window store its size and position
        settings = QtCore.QSettings()

        settings.setValue("ScanImportDlg/Geometry", self.saveGeometry())
        settings.setValue("ScanImportDlg/file_type_index",
                          self.file_type_combobox.currentIndex())
        settings.setValue("ScanImportDlg/max_depth", self.max_depth_spinbox.value())
        settings.setValue("ScanImportDlg/has_string", self.has_string_editline.text())
        settings.setValue("ScanImportDlg/has_not_string", self.has_not_string_editline.text())
        settings.setValue("ScanImportDlg/skip_processed", self.skip_processed_checkBox.isChecked())
        settings.setValue("ScanImportDlg/pipe_base_name", self.pipe_base_name_edit.text())
        settings.setValue("ScanImportDlg/default_slice_name", self.default_slice_name_edit.text())
        settings.setValue("ScanImportDlg/pipe_side_extension", self.pipe_side_extension_edit.text())

        if self.walk_dir is not None:
            self.logger.debug("saving dialog dir {}".format(self.walk_dir))
            settings.setValue("BatchProcessDlg/LastDirectory", os.path.dirname(self.walk_dir))

        # uncheck the showSpectraPlot button before closing the dialog
        if self.show_batch_table.isChecked():
            self.show_batch_table.setChecked(False)
            self.show_batch_table.setEnabled(True)


def main():
    # for debugging the dialog, you can run it as a main as well
    app = QApplication(sys.argv)

    logging.basicConfig(format="%(asctime)s - %(name)s - %(levelname)s - %(message)s",
                        level=logging.DEBUG)

    dlg = ScanImportDlg(app, False)
    dlg.show()
    app.exec_()


if __name__ == '__main__':
    main()
