__author__ = 'eelcovv'
# this dialog will plot the slices

import logging

from PyQt4 import QtGui, QtCore

import matplotlib.gridspec as gridspec
from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt4 import NavigationToolbar2QT as NavigationToolbar
import matplotlib.pyplot as plt
import re
from numpy import pi


class SlicePlotDlg(QtGui.QDialog):
    def __init__(self, pipe, show_slice_plot, parent=None):
        super(SlicePlotDlg, self).__init__(parent)

        self.pipe = pipe

        self.show_slice_plot = show_slice_plot

        self.logger = logging.getLogger(__name__)

        # a close button
        self.buttonBox = QtGui.QDialogButtonBox(QtGui.QDialogButtonBox.Close)

        # a save button to save the image
        self.buttonSave = QtGui.QDialogButtonBox(QtGui.QDialogButtonBox.Save)

        # a save button to save the image
        self.buttonEdit = QtGui.QPushButton("Edit Axes...")

        # a widget to contain the image
        self.figure = plt.figure()
        self.canvas = FigureCanvas(self.figure)

        # a tool bar which we don't show, but we only take the save image functionality
        self.toolbar = NavigationToolbar(self.canvas, self)
        self.toolbar.hide()

        # a spinbox to ask for the slice number to plot
        labelSpinbox = QtGui.QLabel(self.tr("Show slice number (0=all):"))
        labelSpinbox.setAlignment(QtCore.Qt.AlignLeft | QtCore.Qt.AlignBottom)
        self.spinBox = QtGui.QSpinBox()

        # the initial value of the spin box (0 means plot all the slices!)
        self.spinBox.setValue(0)

        # set the limits of the spin box based on the number of slices contained in the dataframe
        self.spinBox.setMinimum(0)
        if self.pipe.slice_dataframes is None:
            self.spinBox.setMaximum(0)
        else:
            max = len(self.pipe.slice_dataframes.groupby(level=0))
            self.spinBox.setMaximum(max)

        # a combobox to ask for the plot type
        labelCombo = QtGui.QLabel(self.tr("Plot Type:"))
        labelCombo.setAlignment(QtCore.Qt.AlignLeft | QtCore.Qt.AlignBottom)
        self.plotTypeComboBox = QtGui.QComboBox()
        self.plotTypeComboBox.addItem("Radius Polar")
        self.plotTypeComboBox.addItem("Radius Linear")
        self.plotTypeComboBox.addItem("Thickness Linear")

        # initialise the axis with a polar plot
        self.axis = None
        self.lines = []
        self.line_colors = []
        self.init_colors()
        self.init_plot()

        # a verical layout to contain the spinbox, combo box and two buttons
        buttonLayout = QtGui.QVBoxLayout()
        buttonLayout.addWidget(labelSpinbox)
        buttonLayout.addWidget(self.spinBox)
        buttonLayout.addWidget(labelCombo)
        buttonLayout.addWidget(self.plotTypeComboBox)
        buttonLayout.addWidget(self.buttonEdit)

        buttonLayout.addStretch()
        buttonLayout.addWidget(self.buttonSave)
        buttonLayout.addWidget(self.buttonBox)

        graphLayout = QtGui.QVBoxLayout()
        graphLayout.addWidget(self.canvas)
        # graphLayout.addWidget(self.toolbar)

        # a horizontal layout to store the canvas left and button etc right
        # the integer 3 and 1 give the relative sizes of the left and right part
        layout = QtGui.QHBoxLayout()
        # layout.addWidget(self.canvas,10)
        layout.addLayout(graphLayout, 10)
        layout.addLayout(buttonLayout, 1)

        self.setLayout(layout)

        # add some signals to the buttons and spinboxes
        self.connect(self.buttonBox, QtCore.SIGNAL("rejected()"),
                     self, QtCore.SLOT("close()"))
        self.buttonSave.clicked.connect(self.save_figure)

        self.connect(self.spinBox, QtCore.SIGNAL("valueChanged(int)"),
                     self.update_figure)

        self.connect(self.plotTypeComboBox, QtCore.SIGNAL("currentIndexChanged(int)"),
                     self.reset_axis)

        self.connect(self.buttonEdit, QtCore.SIGNAL("clicked()"),
                     self.edit_axis)

        settings = QtCore.QSettings()

        size = settings.value("SlicePlotDlg/Size",
                              QtCore.QVariant(QtCore.QSize(800, 500))).toSize()
        self.resize(size)
        position = settings.value("SlicePlotDlg/Position",
                                  QtCore.QVariant(QtCore.QPoint(0, 0))).toPoint()
        self.move(position)

        self.setWindowTitle("Pipe Slice Plot")

    def update_logger(self, level):
        self.logger.setLevel(level)

    def edit_axis(self):
        self.toolbar.edit_parameters()

    def save_figure(self):
        self.toolbar.save_figure()

    def reset_axis(self, plot_number=None):
        # reset the axis. In case you want to do this from a batch (without the dialog, pass the init_plot_number
        # as an argument
        self.clear_lines()
        self.figure.clear()
        if plot_number is not None:
            self.init_plot(plot_number)
        elif unicode(self.plotTypeComboBox.currentText()) == "Radius Polar":
            self.init_plot(0)
        elif unicode(self.plotTypeComboBox.currentText()) == "Radius Linear":
            self.init_plot(1)
        else:
            self.init_plot(2)
        self.canvas.draw()
        self.update_figure(plot_number)

    def init_plot(self, plot_type=0):
        # initialise the current plot area. Called every time we change plot type with the combo box

        if plot_type == 0:
            # set up the plot for polar coordinates
            gs = gridspec.GridSpec(1, 1)
            # create some extra space
            gs.update(bottom=0.2)
            self.bbox_to_anchor_pos = (1.4, 1.1)
            self.axis = self.figure.add_subplot(gs[:], polar=True)
            self.axis.set_xlabel("")
            self.axis.set_ylabel("")
        else:
            # set up a normal linear plot
            gs = gridspec.GridSpec(1, 1)
            # create some extra space
            gs.update(right=0.8)
            self.bbox_to_anchor_pos = (1.3, 1.1)
            self.axis = self.figure.add_subplot(gs[:])
            self.axis.set_xlabel("Phi [rad]")
            self.axis.set_xlim((0, 2 * pi))
            self.axis.set_xticks([0, pi / 2, pi, 3 * pi / 2, 2 * pi])
            self.axis.set_xticklabels([0, r'$\frac{1}{2}\pi$', r'$\pi$', r'$\frac{3}{2}\pi$', r'2$\pi$'])

            if plot_type == 1:
                # we are plottig the radius
                self.axis.set_ylabel("Radius [mm]")
            else:
                # we are plottig the thickness
                self.axis.set_ylabel("Thickness [mm]")

    def clear_lines(self):
        # remove the lines from the current plot. We need to store all the lines in a list so we can explicitly
        #  remove it line by line. After that the lines can be send to the garbage collection by setting it to []
        for i, line in enumerate(self.lines):
            self.axis.lines.remove(line)
        self.lines = []

    def update_figure(self, plot_number=None):
        # set the plot
        if self.pipe.slice_dataframes is not None:

            self.clear_lines()

            index = 0
            line_color = self.get_line_color(index)
            index_slice_to_show = self.spinBox.value()
            self.logger.debug("updating the graph to show slice {} with plot_number {}".format(
                index_slice_to_show, plot_number))
            phandles = []
            for key, df in self.pipe.slice_dataframes.groupby(level=0):
                index += 1

                if index_slice_to_show > 0 and index_slice_to_show != index:
                    continue

                label = "N.A."
                match = re.search("_p[0]*(\d+)", key)
                if bool(match):
                    label = match.group(1)
                self.logger.debug("plotting slice {} with label {}".format(key, label))

                if index_slice_to_show == 0:
                    # only update the line color if all lines are plotted
                    line_color = self.get_line_color(index)
                line_solid = "-"
                line_dashed = "--"

                if ((plot_number is None and self.plotTypeComboBox.currentIndex() < 2) or (
                        plot_number is not None and plot_number < 2)):
                    self.logger.debug("Plotting the radius (polar or linear)")
                    ph, = self.axis.plot(df.phi, df.radius, line_solid, color=line_color,
                                         linewidth=1, markersize=1, label="{:5>}".format(label))
                    try:
                        ph2, = self.axis.plot(df.phi, df.radius_out, line_dashed, color=line_color,
                                              linewidth=1, markersize=1)
                        self.lines.append(ph2)
                    except:
                        pass
                else:
                    self.logger.debug("Plotting the wall thickness")
                    try:
                        ph, = self.axis.plot(df.phi, df.Dwall, line_solid, color=line_color,
                                             linewidth=1, markersize=1, label="{:5>}".format(label))
                    except:
                        self.logger.warning("Failed to plot the thickness of the wall")

                self.lines.append(ph)

                phandles.append(ph)

            plt.legend(handles=phandles, title='X-position', loc=1, bbox_to_anchor=self.bbox_to_anchor_pos)
            self.canvas.draw()

    def save_figure_batch(self, filename):
        self.logger.debug("Saving figure to {}".format(filename))
        plt.figure(self.figure.number)
        plt.savefig(filename, bbox_inches='tight', pad_inches=.1)

    def get_line_color(self, index):
        # get the color of a line with the current index, where the index is looped over the color range
        return self.line_colors[(index - 1) % (len(self.line_colors) - 1)]

    def init_colors(self):
        # a long list of color names to be able to distinguis many profiles
        self.line_colors = [
            "red",
            "green",
            "blue",
            "cyan",
            "yellow",
            "black",
            "magenta",
            "firebrick",
            "purple",
            "darkgoldenrod",
            "gray",
            "burlywood",
            "chartreuse",
            "brown",
            "darkcyan",
            "deeppink",
            "gold",
            "orange",
            "greenyellow",
            "midnightblue",
            "olivedrab",
        ]

    def closeEvent(self, event):
        # before closing the window store its size and position
        settings = QtCore.QSettings()

        settings.setValue("SlicePlotDlg/Size", QtCore.QVariant(self.size()))
        settings.setValue("SlicePlotDlg/Position",
                          QtCore.QVariant(self.pos()))

        # uncheck the showSpectraPlot button before closing the dialog
        if self.show_slice_plot.isChecked():
            self.show_slice_plot.setChecked(False)
            self.show_slice_plot.setEnabled(True)
