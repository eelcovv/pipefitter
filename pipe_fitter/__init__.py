import pkg_resources

try:
    # in the [versioneer] section of the setup.cfg file we have set the name this file
    # '_version_source' and not the default '_version' as the latter conflict with the '_version'
    # file in the Anaconda site-packages directory in case we make an executable
    import _version_source
except ImportError:
    try:
        # Failed to get the version_source, which is the case for an executable. No problem:
        # get the version from the installed package
        __version__ = pkg_resources.get_distribution(__name__).version
    except ImportError:
        # in case this also fails, set the __version__ to unknown
        __version__ = 'unknown'
    # in this mode the git sha key is always unknown as the git version belongs to the installed
    # package, whereas the real sha key may be different already
    __git_sha_key__ = "unknown"
else:
    # we have loaded the _version_source file generated by versioneer. Now try to get the
    # git version
    try:
        __version__ = _version_source.get_versions()["version"]
        __git_sha_key__ = _version_source.get_versions()["full-revisionid"]
    except (AttributeError, ImportError):
        # we can not get the git version vrom the _version_source  file. Get it again from
        # the package
        print("failed to get version from {}".format(_version_source.__file__))
        __version__ = pkg_resources.get_distribution(__name__).version
        __git_sha_key__ = "unknown"

