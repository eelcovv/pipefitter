import logging
import re
import sys

import pandas as pd
try:
    from PyQt4 import QtCore, QtGui
    from PyQt4.QtGui import (QDialog, QVBoxLayout, QFrame, QLabel, QLineEdit, QTableWidget, QComboBox, QTableWidgetItem)
except ModuleNotFoundError:
    from PyQt5 import QtCore, QtGui
    from PyQt5.QtWidgets import (QDialog, QVBoxLayout, QFrame, QLabel, QLineEdit, QHBoxLayout, QTableWidget,
                                 QComboBox, QTableWidgetItem)


from hmc_utils.misc import get_regex_pattern


class GenerateDataDlg(QDialog):
    def __init__(self, fitter, show_file_table, parent=None):
        super(GenerateDataDlg, self).__init__(parent)

        # make sure the dialog is close (not hidden)
        # actually, I want to hide it
        # self.setAttribute(QtCore.Qt.WA_DeleteOnClose)

        self.fitter = fitter

        self.show_file_table = show_file_table

        self.batchseries = None

        self.pipe_stats_dataframe_filtered = None

        self.pipe_stats_dataframe = None

        self.pipe_statistics_data_file = None

        self.brushes = []
        self.brushes.append(QtGui.QBrush(QtGui.QColor(0, 0, 0)))
        self.brushes.append(QtGui.QBrush(QtGui.QColor(200, 0, 0)))
        self.brushes.append(QtGui.QBrush(QtGui.QColor(0, 200, 0)))

        # set the number of columns to plot from the imported XLS file
        self.column_numbers_to_plot = range(10)

        self.logger = logging.getLogger(__name__)

        # a start button
        self.buttonExecute = QtGui.QPushButton("&Execute")

        # a start button
        self.buttonStop = QtGui.QPushButton("&Stop")

        # a save button to save the image
        self.buttonOpenFile = QtGui.QPushButton("&Import OMS File")

        self.buttonApplyFilters = QtGui.QPushButton("&Apply Filters")

        auto_apply_layout = QtGui.QHBoxLayout()
        auto_apply_filters = QtGui.QLabel("&Auto Apply Filters")
        self.auto_apply_checkBox = QtGui.QCheckBox()
        auto_apply_filters.setBuddy(self.auto_apply_checkBox)
        self.auto_apply_checkBox.setToolTip("Automatically apply the filter settings")
        self.auto_apply_checkBox.setStatusTip(self.auto_apply_checkBox.toolTip())
        self.auto_apply_checkBox.setChecked(False)
        auto_apply_layout.addWidget(auto_apply_filters)
        auto_apply_layout.addWidget(self.auto_apply_checkBox)

        sheet_number_label = QtGui.QLabel("&Sheet Index:")
        self.sheet_number_spinbox = QtGui.QSpinBox()
        sheet_number_label.setBuddy(self.sheet_number_spinbox)
        self.sheet_number_spinbox.setRange(0, 999)
        self.sheet_number_spinbox.setValue(0)
        self.sheet_number_spinbox.setToolTip("Select the Sheet index number containing the data")
        self.sheet_number_spinbox.setStatusTip(self.sheet_number_spinbox.toolTip())
        self.sheet_number_spinbox.setFocusPolicy(QtCore.Qt.NoFocus)

        header_number_label = QtGui.QLabel("&Header Line Number:")
        self.header_number_spinbox = QtGui.QSpinBox()
        header_number_label.setBuddy(self.header_number_spinbox)
        self.header_number_spinbox.setRange(1, 999)
        self.header_number_spinbox.setValue(12)
        self.header_number_spinbox.setToolTip("Set the line index of the header of the data")
        self.header_number_spinbox.setStatusTip(self.header_number_spinbox.toolTip())
        self.header_number_spinbox.setFocusPolicy(QtCore.Qt.NoFocus)

        has_string_label = QtGui.QLabel("&Include Pattern:")
        self.has_string_editline = QtGui.QLineEdit()
        has_string_label.setBuddy(self.has_string_editline)
        self.has_string_editline.setToolTip("The file name should contain")
        self.has_string_editline.setStatusTip(self.has_string_editline.toolTip())

        has_not_string_label = QtGui.QLabel("&Exclude Pattern:")
        self.has_not_string_editline = QtGui.QLineEdit()
        has_not_string_label.setBuddy(self.has_not_string_editline)
        self.has_not_string_editline.setToolTip("The file name should contain")
        self.has_not_string_editline.setStatusTip(self.has_not_string_editline.toolTip())

        skip_checkbox_layout = QtGui.QHBoxLayout()
        skip_already_processsed = QtGui.QLabel("&Skip Processed:")
        self.skip_processed_checkBox = QtGui.QCheckBox()
        skip_already_processsed.setBuddy(self.skip_processed_checkBox)
        self.skip_processed_checkBox.setToolTip(
            "Skip cases which are already processed (contain a .xls output")
        self.skip_processed_checkBox.setStatusTip(self.skip_processed_checkBox.toolTip())
        self.skip_processed_checkBox.setChecked(True)
        skip_checkbox_layout.addWidget(skip_already_processsed)
        skip_checkbox_layout.addWidget(self.skip_processed_checkBox)

        # a verical layout to contain the spinbox, combo box and two buttons
        buttonLayout = QtGui.QVBoxLayout()
        buttonLayout.addWidget(self.buttonOpenFile)
        buttonLayout.addWidget(sheet_number_label)
        buttonLayout.addWidget(self.sheet_number_spinbox)

        buttonLayout.addWidget(header_number_label)
        buttonLayout.addWidget(self.header_number_spinbox)

        buttonLayout.addWidget(self.buttonApplyFilters)
        buttonLayout.addLayout(auto_apply_layout)

        buttonLayout.addWidget(has_string_label)
        buttonLayout.addWidget(self.has_string_editline)
        buttonLayout.addWidget(has_not_string_label)
        buttonLayout.addWidget(self.has_not_string_editline)
        buttonLayout.addLayout(skip_checkbox_layout)

        buttonLayout.addStretch()
        buttonLayout.addWidget(self.buttonExecute)
        buttonLayout.addWidget(self.buttonStop)

        self.table = QTableWidget()
        self.table.setAlternatingRowColors(True)

        graphLayout = QtGui.QVBoxLayout()
        graphLayout.addWidget(self.table)

        # a horizontal layout to store the canvas left and button etc right
        # the integer 3 and 1 give the relative sizes of the left and right part
        layout = QtGui.QHBoxLayout()
        layout.addLayout(graphLayout, 10)
        layout.addLayout(buttonLayout, 1)

        self.setLayout(layout)

        self.buttonOpenFile.clicked.connect(self.generate_pipe_data)

        self.buttonApplyFilters.clicked.connect(self.apply_scan_directory)

        self.buttonExecute.clicked.connect(self.execute_batch)

        self.buttonStop.clicked.connect(self.stop_execution_batch)

        self.has_string_editline.textEdited[str].connect(self.update_file_scan)

        self.has_not_string_editline.textEdited[str].connect(self.update_file_scan)

        self.sheet_number_spinbox.valueChanged[int].connect(self.update_file_scan)

        self.header_number_spinbox.valueChanged[int].connect(self.update_file_scan)

        self.skip_processed_checkBox.toggled[bool].connect(self.update_file_scan)

        settings = QtCore.QSettings()

        self.restoreGeometry(settings.value("GenerateDataDlg/Geometry", QtCore.QByteArray()))

        self.title_base = "Generate Data Dialog"
        self.setWindowTitle(self.title_base)

    def set_row_color(self, row):
        item = QTableWidgetItem("Text")
        item.setBackground(QtGui.QColor(255, 0, 0))
        self.table.setHorizontalHeaderItem(row, item)
        self.logger.debug("coloring row {}".format(row))
        self.updateTable()

    def update_file_scan(self):
        if self.auto_apply_checkBox.isChecked():
            self.generate_pipe_data(update_current_file_list=True)

    def apply_scan_directory(self):
        self.generate_pipe_data(update_current_file_list=True)

    def generate_pipe_data(self, update_current_file_list=False):
        """
        Scans the selector directory and retrieves all stl file.

        Parameters
        ----------
        update_current_file_list: bool
            If true, update the file list

        Notes
        -----
        the list is imported to *import_file_dataframe* and then filtered
        """

        if self.pipe_statistics_data_file is None:
            settings = QtCore.QSettings()
            self.pipe_statistics_data_file = settings.value("GenerateDataDlg/LastFile")
            self.logger.debug("setting last dir loading {}".format(self.pipe_statistics_data_file))
        else:
            self.logger.debug("last dir is already -{}-".format(self.pipe_statistics_data_file))

        if not update_current_file_list:

            # open the directory dialog only if we are not updating a field
            formats = (["*.{}".format(extension.lower())
                        for extension in ["xls", "xlsx"]])

            self.pipe_statistics_data_file = QtGui.QFileDialog.getOpenFileName(
                self, "Import Pipe Statics", self.pipe_statistics_data_file,
                "Pipe Statistics files ({})".format(" ".join(formats)))

            try:
                self.logger.debug("Importing file {}".format(self.pipe_statistics_data_file))
                self.pipe_stats_dataframe = pd.read_excel(
                    self.pipe_statistics_data_file, sheetname=self.sheet_number_spinbox.value(),
                    skiprows=self.header_number_spinbox.value() - 1)
                self.logger.debug("Done")
            except FileNotFoundError:
                self.logger.warning("Could not find this file : {}".format(
                    self.pipe_statistics_data_file))
            except IndexError:
                self.logger.warning("Sheet Index out of range: {}".format(
                    self.sheet_number_spinbox.value()))

            # simplify the column names by removing all the spaces and take out the second line
            try:
                colums = [re.sub("\s+", "", re.sub("\n.*", "", x))
                          for x in self.pipe_stats_dataframe.columns]
                self.pipe_stats_dataframe.columns = colums
                # add one more column containing the status
                self.pipe_stats_dataframe.ix[:, "Status"] = 0
            except AttributeError as err:
                self.logger.info("No columns can be found yet bceause: {}".format(err))

        self.setWindowTitle("{} - File {}".format(self.title_base, self.pipe_statistics_data_file))

        # get the filter strings
        has_string = get_regex_pattern(self.has_string_editline.text())
        self.logger.debug("{} and {}".format(self.has_string_editline.text(), has_string))
        has_not_string = get_regex_pattern(self.has_not_string_editline.text())

        # init the filtered data list (which is shown in the table) using all data
        self.pipe_stats_dataframe_filtered = self.pipe_stats_dataframe

        # copy the scanned file list to a pandas data frame. Apply the string filter
        if has_string is not None:
            mask = self.pipe_stats_dataframe_filtered["PipeNo"].str.contains(has_string)
            self.pipe_stats_dataframe_filtered = self.pipe_stats_dataframe_filtered[mask]

        if has_not_string is not None:
            mask = self.pipe_stats_dataframe_filtered["PipeNo"].str.contains(has_not_string)
            # use the ~ character to invert the mask
            self.pipe_stats_dataframe_filtered = self.pipe_stats_dataframe_filtered[~mask]

        self.logger.debug("data {}".format(self.pipe_stats_dataframe_filtered.info()))
        self.logger.debug("index  {}".format(self.pipe_stats_dataframe.columns[1]))
        # update the table widget
        self.updateTable()

    def set_tabel_column(self, i_row, col, data_row, name, format="{}"):
        # fill in a data field of the table
        try:
            item = QTableWidgetItem(format.format(data_row[name]))
            # align the numbers to the right
            item.setTextAlignment(QtCore.Qt.AlignRight)
            self.table.setItem(i_row, col, item)
        except (AttributeError, KeyError) as err:
            # self.logger.debug("error setting table :{}".format(err))
            pass

    def update_row(self, row, stl_file, current=None):
        data_row = self.pipe_stats_dataframe.loc[row]

        item = QTableWidgetItem(stl_file)
        selected = None
        if current is not None and current == id(stl_file):
            selected = item
        item.setData(QtCore.Qt.UserRole, id(stl_file))
        item.setForeground(self.brushes[data_row.Status])
        self.table.setItem(row, 0, item)

        for i in self.column_numbers_to_plot[1:]:
            self.set_tabel_column(row, i, data_row, self.pipe_stats_dataframe.columns[i], "{:.1f}")

        return selected

    def updateTable(self, current=None):

        self.table.clear()
        self.table.setRowCount(len(self.pipe_stats_dataframe_filtered.index))

        column_names = []
        for i in self.column_numbers_to_plot:
            column_names.append(self.pipe_stats_dataframe.columns[i])
        self.table.setColumnCount(len(self.column_numbers_to_plot))
        self.table.setHorizontalHeaderLabels(column_names)
        self.table.setAlternatingRowColors(True)
        self.table.setEditTriggers(QTableWidget.NoEditTriggers)
        self.table.setSelectionBehavior(QTableWidget.SelectRows)
        self.table.setSelectionMode(QTableWidget.SingleSelection)
        selected = None
        for row, stl_file in enumerate(self.pipe_stats_dataframe_filtered["PipeNo"].tolist()):
            is_selected = self.update_row(row, stl_file, current)
            if is_selected is not None:
                selected = is_selected

        self.table.resizeColumnsToContents()
        if selected is not None:
            selected.setSelected(True)
            self.table.setCurrentItem(selected)
            self.table.scrollToItem(selected)

    def execute_batch(self):
        self.logger.debug("Start generating the batch")

        self.buttonExecute.setEnabled(False)
        self.buttonOpenFile.setEnabled(False)
        self.fitter.start_generate_data.emit(self.pipe_stats_dataframe_filtered)
        QtGui.QApplication.processEvents()

    def stop_execution_batch(self):
        self.logger.debug("Stop generating the data")

        self.buttonExecute.setEnabled(True)
        self.buttonOpenFile.setEnabled(True)

        self.fitter.stop_generate_data.emit()
        QtGui.QApplication.processEvents()

    def update_logger(self, level):
        self.logger.setLevel(level)

    def closeEvent(self, event):
        # before closing the window store its size and position
        settings = QtCore.QSettings()

        if sys.version_info[0] == 3:
            settings.setValue("GenerateDataDlg/Geometry", self.saveGeometry())
            if self.pipe_statistics_data_file is not None:
                self.logger.debug("saving dialog dir {}".format(self.pipe_statistics_data_file))
                settings.setValue("GenerateDataDlg/LastFile", self.pipe_statistics_data_file)
        else:
            settings.setValue("GenerateDataDlg/Size", QtCore.QVariant(self.size()))
            settings.setValue("GenerateDataDlg/Position",
                              QtCore.QVariant(self.pos()))

        # uncheck the showSpectraPlot button before closing the dialog
        if self.show_file_table.isChecked():
            self.show_file_table.setChecked(False)
            self.show_file_table.setEnabled(True)
