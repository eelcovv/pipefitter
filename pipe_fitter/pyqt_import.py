import sys
if sys.version_info < (3, 6, 0):
    # before python 3.6 the ModuleNotFoundError exception is not yet defined. Do it now
    class ModuleNotFoundError(Exception):
        pass
try:
    # try to import pyqt5 first
    from PyQt5 import (QtCore, QtGui)
    from PyQt5.QtGui import (QKeySequence, QColor, QBrush, QCursor)
    from PyQt5.QtCore import (QObject, pyqtSignal)
    from PyQt5.QtWidgets import (QDialog, QMainWindow, QApplication, QProgressBar, QAction,
                                 QTabWidget, QVBoxLayout,
                                 QHBoxLayout, QWidget, QGridLayout, QLabel, QCheckBox, QSlider,
                                 QDialogButtonBox, QDoubleSpinBox,
                                 QPushButton, QTableWidget, QAbstractItemView, QDockWidget,
                                 QFrame, QShortcut, QSpinBox, QLineEdit, QSizePolicy,
                                 QFileDialog, QMessageBox, QTableWidgetItem, QMenu, QComboBox)
except (ModuleNotFoundError, ImportError):
    # failed importing pyqt5. Try to import Pyqt4. Note that the sub modules have changed location
    from PyQt4 import (QtCore, QtGui)
    from PyQt4.QtCore import (QObject, pyqtSignal)
    from PyQt4.QtGui import (QDialog, QMainWindow, QApplication, QProgressBar, QAction,
                             QTabWidget, QVBoxLayout,
                             QHBoxLayout, QWidget, QGridLayout, QLabel, QCheckBox, QSlider,
                             QDialogButtonBox,
                             QPushButton, QTableWidget, QAbstractItemView, QDockWidget, QFrame,
                             QShortcut, QLineEdit, QSizePolicy, QDoubleSpinBox,
                             QKeySequence, QFileDialog, QMessageBox, QTableWidgetItem, QColor,
                             QBrush, QMenu, QSpinBox,
                             QCursor, QComboBox)
