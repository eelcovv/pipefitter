__author__ = 'eelcovv'


from pipe_fitter.pyqt_import import *

from hmc_utils.misc import get_logger


class PipeManipulateDlg(QDialog):
    """
    Dialog  to communicate all the pre-processing steps
    """

    # this slot connects the apply button to the action to process the pipes
    execute_pre_process = pyqtSignal()

    def __init__(self, process_step, pre_process_action, parent=None):
        super(PipeManipulateDlg, self).__init__(parent)

        self.logger = get_logger(__name__)

        self.logger.debug("process step dictionary: {}".format(process_step))

        # deleting on destroy is required, hiding does not work because you will generate too many
        # events
        self.setAttribute(QtCore.Qt.WA_DeleteOnClose)

        self.preProcessAction = pre_process_action

        self.centraliseProfilesCheckBox = QCheckBox("&Centralise the profiles")
        self.centraliseProfilesCheckBox.setChecked(process_step["centralise"])

        self.duplicateProfilesCheckBox = QCheckBox("&Duplicate Single Pipe Sides")
        self.duplicateProfilesCheckBox.setChecked(process_step["duplicate"])

        self.findNeighboursCheckBox = QCheckBox("&Find the Neighbours")
        self.findNeighboursCheckBox.setChecked(process_step["find_neighbours"])

        self.resampleDataCheckBox = QCheckBox("&Resample Data Profiles")
        self.resampleDataCheckBox.setChecked(process_step["resample_data"])

        n_sample_points_label = QLabel("# Sample Points")
        n_sample_points_label.setAlignment(QtCore.Qt.AlignLeft | QtCore.Qt.AlignBottom)
        self.nSamplePointsSpinBox = QSpinBox()

        fit_threshold_label = QLabel("Fit Threshold [mm]")
        self.fitTresholdValue = QDoubleSpinBox()
        self.fitTresholdValue.setSingleStep(0.1)
        self.fitTresholdValue.setValue(process_step["fit_threshold_value"])
        self.fitTresholdValue.setToolTip(
            "Value below which two pipes are considered to 'match'")

        exclude_weld_range_label = QLabel("Exclude Phi Range Weld (+/-) [deg]")
        self.excludeWeldRangeValue = QDoubleSpinBox()
        self.excludeWeldRangeValue.setSingleStep(1.0)
        self.excludeWeldRangeValue.setMinimum(0.0)
        self.excludeWeldRangeValue.setMaximum(90.0)
        self.excludeWeldRangeValue.setValue(process_step["exclude_phi_range_weld"])
        self.excludeWeldRangeValue.setToolTip(
            "If larger than zero, exclude the range around the weld (phi=0) in determining the "
            "optimal rotation angle")

        exclude_sixoclock_range_label = QLabel("Exclude Phi Range Six O'Clock (+/-) [deg]")
        self.excludeSixOclockRangeValue = QDoubleSpinBox()
        self.excludeSixOclockRangeValue.setSingleStep(1.0)
        self.excludeSixOclockRangeValue.setMinimum(0.0)
        self.excludeSixOclockRangeValue.setMaximum(90.0)
        self.excludeSixOclockRangeValue.setValue(process_step["exclude_phi_range_six_oclock"])
        self.excludeSixOclockRangeValue.setToolTip(
            "If larger than zero, exclude the range around the opposite of the weld (phi=180) in "
            "determining the optimal rotation angle")

        optimize_phi_at_translations_label = QLabel(
            "Translate slices over delta distance [mm]")
        self.optimize_phi_at_translations_value = QDoubleSpinBox()
        self.optimize_phi_at_translations_value.setSingleStep(1.0)
        self.optimize_phi_at_translations_value.setMinimum(0.0)
        self.optimize_phi_at_translations_value.setMaximum(100.0)
        self.optimize_phi_at_translations_value.setValue(
            process_step["optimize_phi_at_translations"])
        self.optimize_phi_at_translations_value.setToolTip(
            "Shift area size over COG L. If larger than zero, determine the optimal fit of the "
            "pipes with translations over an area around the COG of this size L, where L = N x "
            "delta, i.e. the number of points time the delta distance given in this dialog "
            "Makes the determination of the fit quality N^2 slower, so keep value zero unless you "
            "are sure you want this")

        n_shift_points_label = QLabel(
            "Number of shifts N per over the LxL area around the COG for the pipe optimization. "
            "Calculation time increases with N^2! (only if L > 0)")
        self.n_shift_points_value = QSpinBox()
        self.n_shift_points_value.setSingleStep(1)
        self.n_shift_points_value.setMinimum(1)
        self.n_shift_points_value.setMaximum(10)
        self.n_shift_points_value.setValue(
            process_step["n_shift_points"])
        self.n_shift_points_value.setToolTip(
            "Number of shifts N per y/z direction to find the optimal fit. WARNING: your "
            "calculation time increase quadratically as N^2"
        )

        # the initial value of the spin box (0 means plot all the slices)
        self.nSamplePointsSpinBox.setValue(process_step["n_sample_points"])
        self.nSamplePointsSpinBox.setMinimum(8)
        self.nSamplePointsSpinBox.setMaximum(1000)

        self.updateStatisticsCheckBox = QCheckBox("&Update Statistics")
        self.updateStatisticsCheckBox.setChecked(process_step["update_statistics"])

        self.prepare_pipe_fit_matrix = QCheckBox("&Prepare Pipe Fit Matrix")
        self.prepare_pipe_fit_matrix.setChecked(process_step["pipe_fitting_matrix"])

        self.update_n_fits_per_pipe = QCheckBox("&Update NFits per Pipe")
        self.update_n_fits_per_pipe.setChecked(process_step["update_n_fits_per_pipe"])

        self.pre_sort_on_n_fit = QCheckBox("&Pre Sort the Pipes on NFit")
        self.pre_sort_on_n_fit.setChecked(process_step["pre_sort_on_n_fit"])

        self.fitQualityCheckBox = QCheckBox("&Fit Qualitity Analyses")
        self.fitQualityCheckBox.setChecked(process_step["fit_quality"])

        fit_quality_analyse_resolution_label = QLabel("Fit Quality Analyses Resolution")
        self.fitAnalysesResolutionComboBox = QComboBox()
        self.fitAnalysesResolutionComboBox.addItem("Full")
        self.fitAnalysesResolutionComboBox.addItem("Sampled (from Matrix)")
        self.fitAnalysesResolutionComboBox.setCurrentIndex(process_step["fit_quality_resolution"])

        buttonBox = QDialogButtonBox(QDialogButtonBox.Apply | QDialogButtonBox.Close)

        # you have to connect the close button to the closeEvent yourself
        buttonBox.rejected.connect(self.closeEvent)

        self.process_step = process_step

        grid = QGridLayout()
        grid.addWidget(self.centraliseProfilesCheckBox, 0, 0, 1, 2)
        grid.addWidget(self.duplicateProfilesCheckBox, 1, 0, 1, 2)
        grid.addWidget(self.findNeighboursCheckBox, 2, 0, 1, 2)
        grid.addWidget(self.resampleDataCheckBox, 3, 0, 1, 2)
        grid.addWidget(n_sample_points_label, 4, 0, 1, 2)
        grid.addWidget(self.nSamplePointsSpinBox, 5, 0, 1, 1)
        grid.addWidget(fit_threshold_label, 6, 0, 1, 1)
        grid.addWidget(self.fitTresholdValue, 7, 0, 1, 1)

        grid.addWidget(exclude_weld_range_label, 8, 0, 1, 1)
        grid.addWidget(self.excludeWeldRangeValue, 9, 0, 1, 1)

        grid.addWidget(exclude_sixoclock_range_label, 10, 0, 1, 1)
        grid.addWidget(self.excludeSixOclockRangeValue, 11, 0, 1, 1)

        grid.addWidget(optimize_phi_at_translations_label, 12, 0, 1, 1)
        grid.addWidget(self.optimize_phi_at_translations_value, 13, 0, 1, 1)

        grid.addWidget(n_shift_points_label, 14, 0, 1, 1)
        grid.addWidget(self.n_shift_points_value, 15, 0, 1, 1)

        grid.addWidget(self.updateStatisticsCheckBox, 0, 3, 1, 2)

        grid.addWidget(self.prepare_pipe_fit_matrix, 1, 3, 1, 2)

        grid.addWidget(self.update_n_fits_per_pipe, 2, 3, 1, 2)

        grid.addWidget(self.pre_sort_on_n_fit, 3, 3, 1, 2)

        grid.addWidget(self.fitQualityCheckBox, 4, 3, 1, 2)
        grid.addWidget(fit_quality_analyse_resolution_label, 5, 3, 1, 2)
        grid.addWidget(self.fitAnalysesResolutionComboBox, 6, 3, 1, 2)

        grid.addWidget(buttonBox, 11, 3, 1, 2)
        self.setLayout(grid)

        buttonBox.button(QDialogButtonBox.Apply).clicked.connect(self.apply)
        buttonBox.rejected.connect(self.reject)
        self.setWindowTitle("Pre-process Pipe Profiles")

    def update_logger(self, level):
        self.logger.setLevel(level)

    def store_settings(self):
        self.process_step["centralise"] = (
            self.centraliseProfilesCheckBox.isChecked())
        self.process_step["duplicate"] = (
            self.duplicateProfilesCheckBox.isChecked())
        self.process_step["find_neighbours"] = (
            self.findNeighboursCheckBox.isChecked())
        self.process_step["resample_data"] = (
            self.resampleDataCheckBox.isChecked())
        self.process_step["n_sample_points"] = (
            self.nSamplePointsSpinBox.value())
        self.process_step["update_statistics"] = (
            self.updateStatisticsCheckBox.isChecked())
        self.process_step["pipe_fitting_matrix"] = (
            self.prepare_pipe_fit_matrix.isChecked())
        self.process_step["update_n_fits_per_pipe"] = (
            self.update_n_fits_per_pipe.isChecked())
        self.process_step["pre_sort_on_n_fit"] = (
            self.pre_sort_on_n_fit.isChecked())
        self.process_step["fit_quality"] = (
            self.fitQualityCheckBox.isChecked())
        self.process_step["fit_quality_resolution"] = (
            self.fitAnalysesResolutionComboBox.currentIndex())
        self.process_step["fit_threshold_value"] = (
            self.fitTresholdValue.value())
        self.process_step["exclude_phi_range_weld"] = (
            self.excludeWeldRangeValue.value())
        self.process_step["exclude_phi_range_six_oclock"] = (
            self.excludeSixOclockRangeValue.value())
        self.process_step["optimize_phi_at_translations"] = (
            self.optimize_phi_at_translations_value.value())
        self.process_step["n_shift_points"] = (
            self.n_shift_points_value.value())

    def apply(self):
        """
        After clicking the apply button, update all the values to the process_step dictionary and emit a signal that
        we can start processing
        """
        self.store_settings()
        self.execute_pre_process.emit()

    @QtCore.pyqtSlot()
    def closeEvent(self, event=None):
        # uncheck the showSpectraPlot button before closing the dialog
        self.logger.debug("Enabling the pre-process action button")
        self.preProcessAction.setChecked(False)
        self.preProcessAction.setEnabled(True)
        self.store_settings()
