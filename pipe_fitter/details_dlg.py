__author__ = 'eelcovv'
# this dialog will plot the slices

import logging

try:
    from PyQt4 import (QtCore, QtGui)
    from PyQt4.QtCore import (QObject, pyqtSignal)
    from PyQt4.QtGui import (QDialog, QMainWindow, QApplication, QProgressBar, QAction, QTabWidget, QVBoxLayout,
                             QHBoxLayout, QWidget, QGridLayout, QLabel, QCheckBox, QSlider, QDialogButtonBox,
                             QPushButton, QTableWidget, QAbstractItemView, QDockWidget, QFrame, QShortcut,
                             QKeySequence, QFileDialog, QMessageBox, QTableWidgetItem, QColor, QBrush,
                             QDoubleSpinBox, QSpinBox, QPlainTextEdit)
except ModuleNotFoundError:
    from PyQt5 import (QtCore, QtGui)
    from PyQt5.QtGui import QKeySequence, QColor, QBrush
    from PyQt5.QtCore import (QObject, pyqtSignal)
    from PyQt5.QtWidgets import (QDialog, QMainWindow, QApplication, QProgressBar, QAction, QTabWidget, QVBoxLayout,
                                 QHBoxLayout, QWidget, QGridLayout, QLabel, QCheckBox, QSlider, QDialogButtonBox,
                                 QPushButton, QTableWidget, QAbstractItemView, QDockWidget, QFrame, QShortcut,
                                 QFileDialog, QMessageBox, QTableWidgetItem, QDoubleSpinBox, QSpinBox, QComboBox,
                                 QPlainTextEdit)


class DetailDlg(QDialog):
    def __init__(self, current, pipe, parent=None):
        super(DetailDlg, self).__init__(parent)

        # deleting on destroy is required, hiding does not work because you will generate to many
        # events!
        self.setAttribute(QtCore.Qt.WA_DeleteOnClose)

        self.logger = logging.getLogger(__name__)

        buttonBox = QDialogButtonBox(QDialogButtonBox.Close)

        # you have to connect the close button to the closevent yourself
        buttonBox.rejected.connect(self.closeEvent)

        grid = QGridLayout()

        label = [QLabel("") for i in range(2)]
        for row, info in enumerate(pipe.index):
            label[0] = QLabel("{:}".format(info))
            label[1] = QLabel("{:}".format(pipe[info]))
            for i in range(2):
                grid.addWidget(label[i], row, i)

        self.setLayout(grid)

        buttonBox.rejected.connect(self.reject)
        self.setWindowTitle("Details Pipe/Side {}".format(current))

        self.text_edit = QPlainTextEdit()

    def update_logger(self, level):
        self.logger.setLevel(level)
