__author__ = 'eelco'
__author__ = 'eelcovv'

from pipe_fitter.pyqt_import import *

import logging

class PipeSortingDlg(QDialog):
    execute_sort_pipes = pyqtSignal()

    def __init__(self, sort_settings, sortPipesAction, parent=None):
        super(PipeSortingDlg, self).__init__(parent)

        # deleting on destroy is required, hiding does not work because you will generate to many
        # events!
        self.setAttribute(QtCore.Qt.WA_DeleteOnClose)

        self.sortPipesAction = sortPipesAction

        self.logger = logging.getLogger(__name__)

        sortMethodLabel = QLabel("Sort Method")
        self.sortMethodComboBox = QComboBox()
        self.sortMethodComboBox.addItem("Quick Search")
        self.sortMethodComboBox.addItem("Full Search")
        self.sortMethodComboBox.setCurrentIndex(sort_settings["sorting_method"])

        correct_path_label = QLabel("Minimum Pool for correction")
        correct_path_label.setAlignment(QtCore.Qt.AlignLeft | QtCore.Qt.AlignBottom)
        self.nPipesInPoolSpinBox = QSpinBox()
        self.nPipesInPoolSpinBox.setMinimum(-1)
        self.nPipesInPoolSpinBox.setMaximum(9999)
        self.nPipesInPoolSpinBox.setValue(sort_settings["n_pipes_in_pool_minimum"])

        buttonBox = QDialogButtonBox(QDialogButtonBox.Apply | QDialogButtonBox.Close)

        # you have to connect the close button to the closevent yourself
        buttonBox.rejected.connect(self.closeEvent)

        self.sort_settings = sort_settings

        grid = QGridLayout()

        grid.addWidget(sortMethodLabel, 1, 0, 1, 2)
        grid.addWidget(self.sortMethodComboBox, 2, 0, 1, 2)

        grid.addWidget(correct_path_label, 5, 0, 1, 2)
        grid.addWidget(self.nPipesInPoolSpinBox, 6, 0, 1, 2)

        grid.addWidget(buttonBox, 7, 0, 1, 2)
        self.setLayout(grid)

        buttonBox.button(QDialogButtonBox.Apply).clicked.connect(self.apply)
        buttonBox.rejected.connect(self.reject)
        self.setWindowTitle("Sorting Pipe Profiles")

    def update_logger(self, level):
        self.logger.setLevel(level)

    def store_settings(self):
        self.sort_settings["sorting_method"] = (
            self.sortMethodComboBox.currentIndex())

        self.sort_settings["n_pipes_in_pool_minimum"] = (
            self.nPipesInPoolSpinBox.value())

    def apply(self):
        self.store_settings()
        self.execute_sort_pipes.emit()

    @QtCore.pyqtSlot()
    def closeEvent(self, event=None):
        # uncheck the showSpectraPlot button before closing the dialog
        self.logger.debug("Enabling the preproces action button")
        self.sortPipesAction.setChecked(False)
        self.sortPipesAction.setEnabled(True)

        self.store_settings()
