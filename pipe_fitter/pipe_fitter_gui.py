import argparse
import logging
import os
import platform
import sys

try:
    from win32api import LoadResource
except ImportError:
    pass

from pipe_fitter.pyqt_import import *

import yaml
import yamlordereddictloader

import re
import numpy as np

import matplotlib.gridspec as gridspec
from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt4 import NavigationToolbar2QT as NavigationToolbar
import matplotlib.pyplot as plt
from matplotlib import rcParams
from matplotlib.font_manager import FontProperties

from hmc_utils.misc import (create_logger, get_clean_version, PackageInfo, get_logger)

import pipe_fitter.fit_module as fm

from pipe_fitter.scan_import_dlg import ScanImportDlg
from pipe_fitter.generate_data_dlg import GenerateDataDlg
from pipe_fitter.pipe_manipulate_dlg import PipeManipulateDlg
from pipe_fitter.pipe_sorting_dlg import PipeSortingDlg
from pipe_fitter.details_dlg import DetailDlg
from pipe_fitter import resources
import pipe_fitter

BUILD_SETTINGS_FILE = "build_installer.yml"

PACKAGE_INFO = PackageInfo(pipe_fitter)
__version__ = PACKAGE_INFO.package_version

# if a file with this name is found in the working directory, stop the batch process and write
# the results
STOP_FILE_NAME = "stop"

# flush all those ugly warning messages at the start of the script
sys.stderr.flush()

rcParams.update({'font.size': 18})
rcParams['figure.figsize'] = (10, 15)
# this line is only needed to prevent to remove the import line of resources
resource_size = len(resources.qt_resource_data)


class PipeFitterMain(QMainWindow):
    """
    The main GUI window of the application

    Parameters
    ----------
    parent: QtGui.QMainWindow, optional
        Parent window. Default = None
    reset_settings: bool, optional
        If true do not load the stored settings but take all initial values. Default = False
    """

    # @profile
    def __init__(self, parent=None, reset_settings=False):
        super(PipeFitterMain, self).__init__(parent)

        self.reset_settings = reset_settings

        self.logger = get_logger(__name__)
        self.logger.info("Start Pipe Fitter version {}".format(__version__))

        self.current_phi_shift = 0
        self.current_hilo_max = 0

        self.dirty = False
        self.filename = None
        self.export_plotsfilebase = None
        self.export_table_data = None
        self.export_match_up_report = None

        # list of processing steps to carry out if requested
        self.process_steps = dict(centralise=True,
                                  duplicate=True,
                                  find_neighbours=True,
                                  resample_data=True,
                                  n_sample_points=30,
                                  update_statistics=True,
                                  fit_quality=True,
                                  pipe_fitting_matrix=True,
                                  pre_sort_on_n_fit=True,
                                  fit_quality_resolution=0,
                                  optimize_phi_at_translations=0,
                                  n_shift_points=3,
                                  exclude_phi_range_weld=0,
                                  exclude_phi_range_six_oclock=0,
                                  fit_threshold_value=1,
                                  update_n_fits_per_pipe=True)

        self.sort_settings = dict(sorting_method=0, n_pipes_in_pool_minimum=5)

        self.stop_importing_data = False
        self.stop_generating_data = False

        # add a permanent progress bar
        self.progress_bar = QProgressBar(self.statusBar())
        self.statusBar().addPermanentWidget(self.progress_bar)
        self.hide_progress_bar()

        # will contain the plot canvas later
        self.slicePlotDialog = None
        self.scanImportDlg = None
        self.generateDataDlg = None
        self.preProcessDlg = None
        self.sortPipesDlg = None

        # initialize the dialog handles
        self.ScanImportAction = None
        self.generateDataAction = None
        self.preProcessAction = None
        self.preProcessAllAction = None
        self.sortPipesAction = None
        self.aboutAction = None
        self.fileToolbar = None
        self.edit_tool_bar = None
        self.processToolBar = None

        # create the file menu
        self.fileMenu = None
        self.fileMenuActions = None
        self.processMenu = None
        self.helpMenu = None
        self.tabWidget = None
        self.edit_menu = None
        self.edit_menu_actions = None

        # here we create empty list to hold figures and tabs
        self.tabs = []
        # a widget to contain the image
        self.figures = []
        self.canvases = []
        self.toolbars = []
        self.lines = []
        self.annotations = []
        self.axis = []
        self.bbox_to_anchor_pos = []

        # number tabs

        self.fontP = FontProperties()
        self.fontP.set_size('xx-small')
        self.table = None

        self.size_label = None
        self.status_bar = None

        # for each tab I want to add the same widgets, so put them in a list
        self.phi_rotation_slider_format = "{:10.2f}"
        self.hilo_slider_format = "{:10.2f}"
        self.phi_rotation_slider_precision = 10000

        self.show_resample_points_checkBox = []
        self.plot_min_max_checkBox = []
        self.phi_rotation_slider = []
        self.phi_value_label = []
        self.hilo_value_label = []
        self.apply_correction_rotation = []
        self.buttonSave = []
        self.buttonEdit = []
        self.show_delta_r_plot_checkBox = []
        self.show_hilo_plot_checkBox = []

        # loop over tabs
        self.tab_names = ["Polar profiles", "Linear profiles", "Pipe Statistics", "Correlations",
                          "Fit Quality", "Fit Histograms"]
        self.n_tabs = len(self.tab_names)

        # dictionary to store the figure and canvas number per plot
        self.plot_id = {}

        # a flag controlling if we are ScanImporting or not
        self.stop_batch_processing = False

        self.setup_gui()
        self.updateStatus()

        # the class to hold all the data related to the pipe
        self.fitter = fm.FitPipesAnalyse(fit_threshold=self.process_steps["fit_threshold_value"])

        self.init_plots()
        self.fitter.update_status_bar_message.connect(self.show_message_in_status_bar)
        self.table.itemDoubleClicked.connect(self.plot_update_current_tab)

        # QShortcut(QKeySequence("Return"), self.table, self.plot_update_current_tab)

        self.fitter.progress_changed.connect(self.update_progress)
        self.fitter.display_finished.connect(self.hide_progress_bar)
        self.fitter.start_import_data.connect(self.import_data)
        self.fitter.stop_batch_execute.connect(self.stop_import_data)

        self.fitter.start_generate_data.connect(self.generate_data_profiles)
        self.fitter.stop_generate_data.connect(self.stop_generate_data)

        self.tabWidget.currentChanged.connect(self.plot_update_current_tab)

        # the section below is used to store the windows settings  of this application so that with
        # a next start up size and position can be restored
        settings = QtCore.QSettings()
        if not self.reset_settings:
            self.logger.debug("Loading store settings from {}".format(settings.fileName()))
            self.recentFiles = settings.value("RecentFiles") or []
            self.restoreGeometry(settings.value("PipeFitterMainWindow/Geometry",
                                                QtCore.QByteArray()))
            self.restoreState(settings.value("PipeFitterMainWindow/State",
                                             QtCore.QByteArray()))
        else:
            self.logger.debug("Resetting settings")
            self.recentFiles = list()

        self.load_initial_file()

    def setup_gui(self):
        """
        Create the GUI of the main window
        """

        # the file menu actions
        file_open_action = self.createAction(text="&Open...",
                                             slot=self.file_open,
                                             shortcut=QKeySequence.Open,
                                             icon="fileopen",
                                             tip="Open a Pipe Fitter Data file")
        file_save_action = self.createAction(text="&Save",
                                             slot=self.fileSave,
                                             shortcut=QtGui.QKeySequence.Save,
                                             icon="filesave",
                                             tip="Save the current slice data")
        file_save_as_action = self.createAction(text="Save &As...",
                                                slot=self.fileSaveAs,
                                                shortcut=QtGui.QKeySequence.SaveAs,
                                                icon="filesaveas",
                                                tip="Save the current slice data to a new file")
        export_excel_data_action = self.createAction(text="&Export Data...",
                                                     slot=self.export_info_to_excel,
                                                     shortcut="Ctrl+E",
                                                     tip="Export table data to an excel file")

        export_all_action = self.createAction(text="E&xport All Plots...",
                                              slot=self.export_all_plots,
                                              shortcut="Ctrl+X",
                                              tip="Export All Plots to the current directory")

        export_match_up_report = self.createAction(text="Export &Match up Report...",
                                                   slot=self.export_match_up_report,
                                                   shortcut="Ctrl+M",
                                                   tip="Create a match up report of the current "
                                                       "slice with its neighbour")

        file_quit_action = self.createAction(text="&Quit",
                                             slot=self.close,
                                             shortcut="Ctrl+Q",
                                             icon="filequit",
                                             tip="Close the application")

        swap_current_pipe_action = self.createAction(text="&Swap Current Pipe Direction",
                                                     slot=self.swap_current_pipe_direction,
                                                     icon="processall",
                                                     shortcut="Ctrl+U",
                                                     tip="Moves the current pipe up in the table")
        move_current_pipe_up_action = self.createAction(text="Move Current Pipe &Up",
                                                        slot=self.move_current_pipe_up,
                                                        icon="arrow_up-32",
                                                        shortcut="Ctrl+U",
                                                        tip="Moves the current pipe up in the table")
        move_current_pipe_down_action = self.createAction(text="Move Current Pipe &Down",
                                                          slot=self.move_current_pipe_down,
                                                          icon="arrow_do-32",
                                                          shortcut="Ctrl+D",
                                                          tip="Moves the current pipe down in the "
                                                              "table")

        self.ScanImportAction = self.createAction(text="&Scan",
                                                  slot=self.scan_import,
                                                  shortcut="Ctrl+B",
                                                  icon="batch_process",
                                                  tip="Scan for pipes",
                                                  checkable=True,
                                                  signal="toggled",
                                                  disabled=False)

        self.generateDataAction = self.createAction(text="&Generate",
                                                    slot=self.generate_data,
                                                    shortcut="Ctrl+G",
                                                    icon="new_data",
                                                    tip="Generate Slice Data From Distribution",
                                                    checkable=True,
                                                    signal="toggled",
                                                    disabled=False)

        self.preProcessAction = self.createAction(text="Pre-Process &Options",
                                                  slot=self.open_preprocess_dialog,
                                                  shortcut="Ctrl+O",
                                                  icon=None,
                                                  tip="Open Pre-Processing dialog",
                                                  checkable=True,
                                                  signal="toggled",
                                                  disabled=False)

        self.preProcessAllAction = self.createAction(text="&Pre-Process All",
                                                     slot=self.pre_process_all,
                                                     shortcut="Ctrl+P",
                                                     icon="centralise",
                                                     tip="Apply all Pre-Processing steps required "
                                                         "to sort the pipes",
                                                     checkable=True,
                                                     signal="toggled",
                                                     disabled=False)

        self.sortPipesAction = self.createAction(text="&Sort Pipes",
                                                 slot=self.OpenSortPipesDlg,
                                                 shortcut="Ctrl+S",
                                                 icon="generate",
                                                 tip="Sort the pipes",
                                                 checkable=True,
                                                 signal="toggled",
                                                 disabled=True)

        self.aboutAction = self.createAction(text="&About...",
                                             slot=self.OpenAboutDialog,
                                             shortcut="Ctrl+A",
                                             tip="Show info about this tool")

        # create the file tool bar
        self.fileToolbar = self.addToolBar("File")
        self.fileToolbar.setObjectName("FileToolBar")
        self.addActions(self.fileToolbar, (file_open_action, file_save_action))

        self.edit_tool_bar = self.addToolBar("Edit")
        self.edit_tool_bar.setObjectName("EditToolBar")
        self.addActions(self.edit_tool_bar,
                        (swap_current_pipe_action, move_current_pipe_up_action,
                         move_current_pipe_down_action))

        # create the plot tool bar

        # create the plot tool bar
        self.processToolBar = self.addToolBar("Process")
        self.processToolBar.setObjectName("processToolBar")
        self.addActions(self.processToolBar,
                        [self.ScanImportAction, self.generateDataAction, self.preProcessAllAction,
                         self.sortPipesAction])

        # create the file menu
        self.fileMenu = self.menuBar().addMenu("&File")
        self.fileMenuActions = (
            file_open_action, file_save_action, file_save_as_action, export_excel_data_action,
            export_all_action,
            export_match_up_report, file_quit_action)
        self.fileMenu.aboutToShow.connect(self.update_file_menu)

        # create the edit menu with the up and down arrow to manipulate the pipe position
        self.edit_menu = self.menuBar().addMenu("&Edit")
        self.addActions(self.edit_menu, [swap_current_pipe_action, move_current_pipe_up_action,
                                         move_current_pipe_down_action])

        # create the process menu
        self.processMenu = self.menuBar().addMenu("&Process")
        self.addActions(self.processMenu,
                        [self.ScanImportAction, self.generateDataAction,
                         self.preProcessAction, self.preProcessAllAction, self.sortPipesAction])

        # create the help menu
        self.helpMenu = self.menuBar().addMenu("&Help")
        self.addActions(self.helpMenu,
                        [self.aboutAction])

        # a widget containing all the tabs
        self.tabWidget = QTabWidget()
        self.setCentralWidget(self.tabWidget)

        for i_tab in range(self.n_tabs):
            graph_layout = QVBoxLayout()

            self.tabs.append(QWidget())
            self.tabWidget.addTab(self.tabs[-1], self.tab_names[i_tab])

            # each tab can contains one figure and one canvas. A single figure may contain multiple
            # plots
            self.figures.append(plt.figure())
            size = self.figures[-1].get_size_inches()
            self.figures[-1].set_size_inches(size[0] * 2, size[1] * 2, forward=True)
            self.canvases.append(FigureCanvas(self.figures[-1]))
            self.toolbars.append(NavigationToolbar(self.canvases[-1], self))
            self.toolbars[-1].hide()

            graph_layout.addWidget(self.canvases[-1])

            grid_layout1 = QGridLayout()
            grid_layout2 = QGridLayout()

            self.plot_min_max_checkBox.append(None)

            # now add some controls depending on the tab page

            if i_tab < 2:

                # the widgets for tab 0 and 1 which they have in common
                show_resample_points_label = QLabel("&Show resample points")
                self.show_resample_points_checkBox.append(QCheckBox())
                show_resample_points_label.setBuddy(self.show_resample_points_checkBox[-1])
                self.show_resample_points_checkBox[-1].setToolTip(
                    "Show the resampled points on top of the full data")
                self.show_resample_points_checkBox[-1].setStatusTip(
                    self.show_resample_points_checkBox[-1].toolTip())
                self.show_resample_points_checkBox[-1].setChecked(False)
                grid_layout1.addWidget(self.show_resample_points_checkBox[-1], 0, 0)
                grid_layout1.addWidget(show_resample_points_label, 0, 1)

                apply_correction_rotation = QLabel("&Apply Optimal Rotation")
                self.apply_correction_rotation.append(QCheckBox())
                apply_correction_rotation.setBuddy(self.apply_correction_rotation[-1])
                self.apply_correction_rotation[-1].setToolTip(
                    "Apply the rotaion to minimise the profile difference")
                self.apply_correction_rotation[-1].setStatusTip(
                    self.apply_correction_rotation[-1].toolTip())
                self.apply_correction_rotation[-1].setChecked(False)
                grid_layout1.addWidget(self.apply_correction_rotation[-1], 1, 0)
                grid_layout1.addWidget(apply_correction_rotation, 1, 1)

                self.show_resample_points_checkBox[-1].toggled.connect(self.plot_update_current_tab)

                phi_rotation_slider_label1 = QLabel("Rotation [deg] :")
                hi_lo_value_label1 = QLabel("Max HiLo[mm] :")
                self.phi_value_label.append(QLabel(self.phi_rotation_slider_format.format(0)))
                self.phi_value_label[-1].setStyleSheet("font-family:Courier New;")
                self.hilo_value_label.append(QLabel(self.hilo_slider_format.format(0)))
                self.hilo_value_label[-1].setStyleSheet("font-family:Courier New;")
                self.phi_rotation_slider.append(QSlider(QtCore.Qt.Horizontal))
                self.phi_value_label[-1].setBuddy(self.phi_rotation_slider[-1])
                self.phi_rotation_slider[-1].setToolTip("Apply rotation of second profile")
                self.phi_rotation_slider[-1].setStatusTip(self.phi_rotation_slider[-1].toolTip())
                self.phi_rotation_slider[-1].setFocusPolicy(QtCore.Qt.NoFocus)
                # self.phi_rotation_slider[-1].setGeometry(0, 40, 200, 30)
                self.phi_rotation_slider[-1].setRange(0, self.phi_rotation_slider_precision * 360)
                self.phi_rotation_slider[-1].setSingleStep(
                    self.phi_rotation_slider[-1].maximum() / 10000.0)
                self.phi_rotation_slider[-1].setPageStep(
                    self.phi_rotation_slider[-1].maximum() / 20.0)
                self.phi_rotation_slider[-1].setValue(0)
                self.phi_rotation_slider[-1].valueChanged.connect(
                    self.phi_rotation_slider_on_change)

                # multicolumn widht to strech the slider
                grid_layout2.addWidget(phi_rotation_slider_label1, 0, 1)
                grid_layout2.addWidget(hi_lo_value_label1, 1, 1)
                grid_layout2.addWidget(self.phi_value_label[-1], 0, 2)
                grid_layout2.addWidget(self.hilo_value_label[-1], 1, 2)
                grid_layout2.addWidget(self.phi_rotation_slider[-1], 2, 0, 1, 10)
                grid_layout2.setColumnStretch(0, 10)

                self.apply_correction_rotation[-1].toggled.connect(self.update_phi_rotation_slider)

                show_delta_r_plot_label = QLabel("&Show Delta R")
                self.show_delta_r_plot_checkBox.append(QCheckBox())
                show_delta_r_plot_label.setBuddy(self.show_delta_r_plot_checkBox[-1])
                self.show_delta_r_plot_checkBox[-1].setToolTip(
                    "Show the plot of the difference in radius")
                self.show_delta_r_plot_checkBox[-1].setStatusTip(
                    self.show_delta_r_plot_checkBox[-1].toolTip())
                self.show_delta_r_plot_checkBox[-1].setChecked(True)
                grid_layout1.addWidget(self.show_delta_r_plot_checkBox[-1], 0, 2)
                grid_layout1.addWidget(show_delta_r_plot_label, 0, 3)

                show_hilo_plot_label = QLabel("&Show HiLo")
                self.show_hilo_plot_checkBox.append(QCheckBox())
                show_hilo_plot_label.setBuddy(self.show_hilo_plot_checkBox[-1])
                self.show_hilo_plot_checkBox[-1].setToolTip(
                    "Show the plot of the HiLo values vs Phi")
                self.show_hilo_plot_checkBox[-1].setStatusTip(
                    self.show_hilo_plot_checkBox[-1].toolTip())
                self.show_hilo_plot_checkBox[-1].setChecked(True)
                grid_layout1.addWidget(self.show_hilo_plot_checkBox[-1], 1, 2)
                grid_layout1.addWidget(show_hilo_plot_label, 1, 3)

                if i_tab == 0:
                    # the first table gray out the hilo and delta r plot
                    self.show_hilo_plot_checkBox[-1].setDisabled(True)
                    self.show_delta_r_plot_checkBox[-1].setDisabled(True)
                    show_delta_r_plot_label.setDisabled(True)
                    show_hilo_plot_label.setDisabled(True)
                else:
                    # only add the connections of the delta_r and hilo plot at the second tab
                    self.show_delta_r_plot_checkBox[-1].toggled.connect(
                        self.plot_update_current_tab)
                    self.show_hilo_plot_checkBox[-1].toggled.connect(self.plot_update_current_tab)

            elif i_tab == 2 or i_tab == 4:
                plot_min_max_label = QLabel("&Include Min/Max in plot")
                self.plot_min_max_checkBox[-1] = QCheckBox()
                plot_min_max_label.setBuddy(self.show_resample_points_checkBox[-1])
                self.plot_min_max_checkBox[-1].setToolTip(
                    "Show the resampled points on top of the full data")
                self.plot_min_max_checkBox[-1].setStatusTip(
                    self.show_resample_points_checkBox[-1].toolTip())
                self.plot_min_max_checkBox[-1].setChecked(False)
                grid_layout1.addWidget(self.plot_min_max_checkBox[-1], 0, 0)
                grid_layout1.addWidget(plot_min_max_label, 0, 1)
                self.plot_min_max_checkBox[-1].toggled.connect(self.plot_update_current_tab)

            # a save button to save the image
            button_width = 70
            self.buttonSave.append(QDialogButtonBox(QDialogButtonBox.Save))
            self.buttonSave[-1].setFixedWidth(button_width)

            # a save button to save the image
            self.buttonEdit.append(QPushButton("Edit Axes..."))
            self.buttonEdit[-1].setFixedWidth(button_width)
            button_layout = QVBoxLayout()
            button_layout.addWidget(self.buttonEdit[-1])
            button_layout.addWidget(self.buttonSave[-1])

            control_layout = QHBoxLayout()
            control_layout.addLayout(grid_layout1)
            # control_layout.addStretch(1)
            control_layout.addLayout(grid_layout2)
            # control_layout.addStretch(1)

            control_layout.addLayout(button_layout)
            graph_layout.addLayout(control_layout)

            # add them to the tab
            self.tabs[-1].setLayout(graph_layout)

            # add connections

            self.buttonEdit[-1].clicked.connect(self.edit_axis)
            self.buttonSave[-1].clicked.connect(self.save_figure)

        # a widget for the table.
        self.table = QTableWidget()
        self.table.setAlternatingRowColors(True)
        self.table.setSelectionBehavior(QAbstractItemView.SelectRows)

        self.table.setAlternatingRowColors(True)
        self.table.setEditTriggers(QTableWidget.NoEditTriggers)
        self.table.setSelectionBehavior(QTableWidget.SelectRows)
        self.table.setSelectionMode(QTableWidget.ExtendedSelection)

        self.table.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.table.customContextMenuRequested.connect(self.handle_table_menu)

        table_dock_widget = QDockWidget("Slice Scans", self)
        table_dock_widget.setObjectName("TableDockWidget")
        table_dock_widget.setAllowedAreas(
            QtCore.Qt.LeftDockWidgetArea | QtCore.Qt.RightDockWidgetArea)
        table_dock_widget.setWidget(self.table)

        self.addDockWidget(QtCore.Qt.LeftDockWidgetArea, table_dock_widget)

        self.show()

        # activate the status bar
        self.size_label = QLabel()
        self.size_label.setFrameStyle(QFrame.StyledPanel | QFrame.Sunken)
        self.status_bar = self.statusBar()
        self.status_bar.setSizeGripEnabled(False)
        self.status_bar.addPermanentWidget(self.size_label)
        self.status_bar.showMessage("Ready", 5000)

    def phi_rotation_slider_on_change(self, value):

        i_tab = self.tabWidget.currentIndex()
        self.logger.debug("entering phi_rotation_slider_on_change on tab {}".format(i_tab))
        self.logger.debug("slider position: {}".format(value / self.phi_rotation_slider_precision))

        changed_slider = self.sender()
        if changed_slider is self.phi_rotation_slider[i_tab]:
            self.logger.debug("switching off apply_correction_rotation on tab {}".format(i_tab))
            self.apply_correction_rotation[i_tab].setChecked(False)

        self.current_phi_shift = value / self.phi_rotation_slider_precision

        self.phi_value_label[i_tab].setText(
            self.phi_rotation_slider_format.format(self.current_phi_shift))

        self.update_current_hilomax()

        self.plot_update_current_tab()

    def update_current_hilomax(self):
        """
        based on the current_phi position, update the current hilo max value
        """
        i_tab = self.tabWidget.currentIndex()

        try:
            tablerow = self.table.currentRow()
            piped, pipei, piper, label, sidechar = self.get_row_data_frames(tablerow)
            k_phi_shift = np.abs(np.rad2deg(piped.index) - self.current_phi_shift).argmin()
            self.current_hilo_max = piped.HiLo.values[k_phi_shift]

            self.logger.debug("set phi_shift : {} with hilo {}".format(self.current_phi_shift,
                                                                       self.current_hilo_max))

            self.hilo_value_label[i_tab].setText(
                self.hilo_slider_format.format(self.current_hilo_max))
        except (AttributeError, TypeError):
            self.logger.debug("no hilo found")

    def update_phi_rotation_slider(self):
        """
        Update the phi angle on change of the rotation slider
        """

        i_tab = self.tabWidget.currentIndex()
        self.logger.debug("update_phi_rotation_slider on tab{}".format(i_tab))

        tablerow = self.table.currentRow()
        if tablerow > -1:

            piped, pipei, piper, label, sidechar = self.get_row_data_frames(tablerow)

            # in case a neighbour was set: load this data as well and store in piped2, etc
            has_neighbour, piped2, pipei2, piper2, label2, sidechar2 = \
                self.get_neighbour_data_frame(pipei)

            if has_neighbour:
                # always invert the phi direction of the neighbour
                # in case the apply rotation checkbox is activate, perform the rotation on the
                # neighbour profile
                if self.apply_correction_rotation[i_tab].isChecked():
                    try:
                        # the phi rotation for a pair of neighbour is only given once at the front
                        # neighbour the back neighbour is always 0. this means you need to add both
                        # phi's to be able to shift due to either on of the neighbours. The first
                        # neirbour should be shifted in negative direction
                        phi_shift = pipei2.PhiOptimal + pipei.PhiOptimal

                        self.current_phi_shift = phi_shift

                        self.logger.debug("set value phi_rotation_slider {}".format(i_tab))
                        self.phi_rotation_slider[i_tab].setValue(
                            phi_shift * self.phi_rotation_slider_precision)

                        # self.plot_update_current_tab()

                    except AttributeError:
                        self.logger.info("No HiLo has been calculated yet. Setting phi to 0")

    def handle_table_menu(self):
        menu = QMenu()
        # change the color of the menu.
        #     menu.setStyleSheet("""
        #     QMenu::item {
        #         background-color: #264F7D;
        #         color: white;
        #         font-weight:bold;}
        # """)

        indices = self.table.selectionModel().selectedRows()
        n_selected = len(indices)

        text = menu.addAction("Pipe Actions:")
        text.setEnabled(False)
        menu.addSeparator()

        # two menu items
        action_details = menu.addAction('Show Details')
        action_match_up = menu.addAction('Match up')

        if n_selected == 1:
            action_details.setEnabled(True)
        else:
            action_details.setEnabled(False)

        if n_selected == 2:
            action_match_up.setEnabled(True)
        else:
            action_match_up.setEnabled(False)

        # catch the right mouse button action
        action = menu.exec_(QCursor.pos())

        # do the requested action
        if action == action_details:
            # pop up window with details
            self.show_pipe_details()
        elif action == action_match_up:
            self.match_up_pipes(indices)
            self.update_table()
        else:
            self.logger.debug("not yet implemented")

    def match_up_pipes(self, indices):
        """
        Force two pipes to be neighbours and calculate their match up statistics
        """
        labels = []
        sidechars = []
        # loop over the selected indices and get the two pipe ends
        for index in sorted(indices):
            item = self.table.itemFromIndex(index)
            label = self.table.item(item.row(), 0).text()
            side_char = self.table.item(item.row(), 1).text()

            self.logger.debug("Matching {} / {}".format(label, side_char))

            labels.append(label)
            sidechars.append(side_char)

        # now connect the pipe ends to each other
        for i, label1 in enumerate(labels):
            side1 = sidechars[i]
            j = (i + 1) % 2
            label2 = labels[j]
            side2 = sidechars[j]
            self.fitter.pipe_info_df.ix[(label1, side1), "NeighbourLabel"] = label2
            self.fitter.pipe_info_df.ix[(label1, side1), "NeighbourSide"] = side2

            # force to calculate the statistics
            self.fitter.calculate_pipe_stats(label1, side1)
            self.fitter.calculate_pipe_stats(label1, side1)
            # the cumulative phi is returned

            self.fitter.get_fit_quality(label1, side1, use_sampled_data=False)

    def show_message_in_status_bar(self, message=None, timer=0):
        self.statusBar().showMessage(self.tr(message), timer)

    def update_progress(self, n, nrows, message=""):
        self.progress_bar.show()
        self.progress_bar.setRange(0, nrows - 1)
        self.progress_bar.setValue(n)
        self.statusBar().showMessage(self.tr(message))

    def hide_progress_bar(self):
        self.progress_bar.hide()
        self.statusBar().showMessage(self.tr("Ready"), 5000)

    def init_plots(self):
        """
        Initialise the current plot area. Called every time we change plot type with the combo box
        """
        self.axis = []
        self.bbox_to_anchor_pos = []

        # TAB 0 ######################################
        # set up the plot for polar coordinates
        self.axis.append([])
        self.lines.append([])
        self.annotations.append([])
        self.bbox_to_anchor_pos.append([])
        i_tab = 0
        gs = gridspec.GridSpec(1, 1)
        # create some extra space
        gs.update(bottom=0.1)

        # AXIS 0: Polar plot in TAB 1
        self.bbox_to_anchor_pos[-1].append((1.3, 1.1))
        self.axis[-1].append(self.figures[i_tab].add_subplot(gs[:], polar=True))
        self.axis[-1][-1].set_theta_zero_location("N")
        self.axis[-1][-1].set_theta_direction(-1)
        self.axis[-1][-1].set_xlabel("")
        self.axis[-1][-1].set_ylabel("")
        self.axis[-1][-1].grid(True)
        # TODO: make a preference dialog to set the default boundaries to start up the graph
        # self.axis[-1][-1].set_ylim(190, 210)

        # TAB 1 ######################################
        # set up a normal linear plot
        self.axis.append([])
        self.lines.append([])
        self.annotations.append([])
        self.bbox_to_anchor_pos.append([])
        gs = gridspec.GridSpec(2, 1)
        self.bbox_to_anchor_pos[-1].append((1.3, 1.1))

        # create some extra space
        gs.update(right=0.8)

        i_tab = 1
        # AXIS 1: linear plot in TAB 1
        self.axis[-1].append(self.figures[i_tab].add_subplot(gs[0, 0]))
        self.axis[-1][-1].set_xlabel(r"$\phi$ [$^o$]")
        self.axis[-1][-1].set_xlim((0, 360))
        self.axis[-1][-1].grid(True)

        self.axis[-1][-1].set_ylabel("Radius in [mm]")

        self.bbox_to_anchor_pos[-1].append((1.3, 1.1))

        # create some extra space
        gs.update(right=0.8)

        self.axis[-1].append(self.figures[i_tab].add_subplot(gs[1, 0], sharex=self.axis[-1][-1]))
        self.axis[-1][-1].set_xlabel(r"$\phi$ [$^\circ$]")
        self.axis[-1][-1].set_xlim((0, 360))
        self.axis[-1][-1].grid(True)
        self.axis[-1][0].set_xlabel("")
        plt.setp(self.axis[-1][0].get_xlabel(), visible=False)
        plt.setp(self.axis[-1][0].get_xticklabels(), visible=False)

        self.axis[-1][-1].set_ylabel("Delta radius [mm]")

        # create a second y-axis for the hilow-vs-phi plot
        self.axis[-1].append(self.axis[-1][-1].twinx())
        self.axis[-1][-1].set_ylabel("Max HiLo [mm]")

        # TAB 2 ######################################
        i_tab = 2
        self.axis.append([])
        self.lines.append([])
        self.annotations.append([])

        self.bbox_to_anchor_pos.append([])

        # defined a grid of 3 rows and four columns. This can be used to adjust the width of the
        # columns
        gs = gridspec.GridSpec(3, 4)

        # top left figure: radius vs index over 3 columns at first row
        self.bbox_to_anchor_pos[-1].append((1.0, 1.))
        self.axis[-1].append(self.figures[i_tab].add_subplot(gs[0, :-1]))
        self.axis[-1][-1].set_xlabel("")
        self.axis[-1][-1].grid(True)
        self.axis[-1][-1].set_ylabel("Radius [mm]")
        plt.setp(self.axis[-1][-1].get_xlabel(), visible=False)
        plt.setp(self.axis[-1][-1].get_xticklabels(), visible=False)

        # top right figure: radius histogram over 1 (last) column at first row
        self.bbox_to_anchor_pos[-1].append((1.3, 1.1))
        self.axis[-1].append(self.figures[i_tab].add_subplot(gs[0, -1]))
        self.set_histo_axis(self.axis[-1][-1], x_label="Counts [-]", hide_y_labels=True)

        # middle left figure: ovality vs index over 3 columns at first row
        self.bbox_to_anchor_pos[-1].append((1.0, 1.))
        self.axis[-1].append(self.figures[i_tab].add_subplot(gs[1, :-1]))
        self.axis[-1][-1].set_xlabel("")
        self.axis[-1][-1].grid(True)
        self.axis[-1][-1].set_ylabel("Ovality [%]")
        plt.setp(self.axis[-1][-1].get_xlabel(), visible=False)
        plt.setp(self.axis[-1][-1].get_xticklabels(), visible=False)

        # middle right figure: ovality histogram over 1 column at middle row
        self.bbox_to_anchor_pos[-1].append((1.3, 1.1))
        self.axis[-1].append(self.figures[i_tab].add_subplot(gs[1, -1]))
        self.set_histo_axis(self.axis[-1][-1], x_label="Counts [-]", hide_y_labels=True)

        # bottom left figure: asymmetry vs index over 3 columns at first row
        self.bbox_to_anchor_pos[-1].append((1.0, 1.))
        self.axis[-1].append(self.figures[i_tab].add_subplot(gs[2, :-1]))
        self.axis[-1][-1].set_xlabel("pipe index [-]")
        self.axis[-1][-1].grid(True)
        self.axis[-1][-1].set_ylabel("Skewness [x1000]")

        # bottom right figure: ovality histogram over 1 column at middle row
        self.bbox_to_anchor_pos[-1].append((1.3, 1.1))

        self.axis[-1].append(self.figures[i_tab].add_subplot(gs[2, -1]))
        self.set_histo_axis(self.axis[-1][-1], x_label="Counts [-]", hide_y_labels=True)

        # TAB 3 ######################################
        i_tab = 3
        self.axis.append([])
        self.lines.append([])
        self.annotations.append([])
        self.bbox_to_anchor_pos.append([])
        ncol = 20
        nc1 = int(ncol / 2) - 1
        nc2 = int(ncol / 2) + 1
        gs = gridspec.GridSpec(2, ncol)
        self.bbox_to_anchor_pos[-1].append((1.0, 1.))

        self.axis[-1].append(self.figures[i_tab].add_subplot(gs[0, 0:nc1]))
        self.axis[-1][-1].grid(True)
        self.axis[-1][-1].set_xlabel("R_mean [mm]")
        self.axis[-1][-1].set_ylabel("NFit [-]")

        self.axis[-1].append(self.figures[i_tab].add_subplot(gs[0, nc2:]))
        self.axis[-1][-1].grid(True)
        self.axis[-1][-1].set_xlabel("Ovality [%]")
        self.axis[-1][-1].set_ylabel("NFit [-]")

        self.axis[-1].append(self.figures[i_tab].add_subplot(gs[1, 0:nc1]))
        self.axis[-1][-1].grid(True)
        self.axis[-1][-1].set_xlabel("Skewness [10$^{-3}$]")
        self.axis[-1][-1].set_ylabel("NFit [-]")

        self.axis[-1].append(self.figures[i_tab].add_subplot(gs[1, nc2:]))
        self.axis[-1][-1].grid(True)
        self.axis[-1][-1].set_xlabel("Skewness [10$^{-3}$]")
        self.axis[-1][-1].set_ylabel("Asymmetry [10$^{-2}$]")

        # TAB 4 ######################################
        self.axis.append([])
        self.lines.append([])
        self.annotations.append([])
        self.bbox_to_anchor_pos.append([])
        gs = gridspec.GridSpec(1, 1)
        self.bbox_to_anchor_pos[-1].append((1.3, 1.1))

        # create some extra space
        gs.update(right=0.8)

        i_tab = 4
        # AXIS 1: HiLow vs Index
        self.axis[-1].append(self.figures[i_tab].add_subplot(gs[0, 0]))
        self.axis[-1][-1].set_xlabel("Index")
        self.axis[-1][-1].grid(True)

        self.axis[-1][-1].set_ylabel("HiLow [mm]")

        # TAB 5 ######################################
        self.axis.append([])
        self.lines.append([])
        self.annotations.append([])
        self.bbox_to_anchor_pos.append([])
        gs = gridspec.GridSpec(1, 1)
        self.bbox_to_anchor_pos[-1].append((1.3, 1.1))

        # create some extra space
        gs.update(right=0.8)

        # AXIS 1: Histogram for HiLo
        i_tab = 5
        self.axis[-1].append(self.figures[i_tab].add_subplot(gs[0, 0]))
        self.set_histo_axis(self.axis[-1][-1], x_label="HiLo [mm]", y_label="Counts [-]")

    def set_histo_axis(self, ax, x_label="", y_label="", show_grid=True, hide_x_labels=False,
                       hide_y_labels=False):
        """
        Set the axis of the histogram plot

        Parameters
        ----------
        ax: Plot Axis
            Current axis that contain the histogram
        x_label: str, optional
            x label of the histogram. Default = ""
        y_label: str, optional
            y label of the histogram. Default = ""
        show_grid: bool, optional
            Show the grid, default = True
        hide_x_labels: bool
            Hide all the x axis labels. Default = False
        hide_y_labels: bool
            Hide all the y axis labels. Default = False
        """
        ax.set_xlabel(x_label)
        ax.set_ylabel(y_label)
        if show_grid:
            ax.grid(True)
        if hide_x_labels:
            plt.setp(ax.get_xlabel(), visible=False)
            plt.setp(ax.get_xticklabels(), visible=False)
        if hide_y_labels:
            plt.setp(ax.get_ylabel(), visible=False)
            plt.setp(ax.get_yticklabels(), visible=False)

    def get_neighbour_data_frame(self, pipe_info_df):
        """
        Get the data frame belonging to the neighbour of the currently selected pipe

        Parameters
        ----------
        pipe_info_df: DataFrame
            Currently selected pipe

        Returns
        -------
        tuple (has_neighbour, piped, pipei, piper, neighbour_label, neighbour_side)
            Tuple with the information of the pipe end

            - has_neighbour: bool,  if True we have a neighbour
            - piped: pipe data dataframe of the neighbour
            - pipei: pipe information of the data frame
            - piper: pipe resampled data dataframe of the neighbour
            - neighbour_label: label of the neighbour
            - neighbour_side: side label of the neighbour

        """

        try:
            has_neighbour = True
            neighbour_label = pipe_info_df.NeighbourLabel
            neighbour_side = pipe_info_df.NeighbourSide
            piped = self.fitter.pipe_data_df[neighbour_label][neighbour_side]
            pipei = self.fitter.pipe_info_df.loc[neighbour_label, neighbour_side]
        except (KeyError, TypeError):
            has_neighbour = False
            neighbour_label = None
            neighbour_side = None
            piped = None
            pipei = None

        try:
            piper = self.fitter.pipe_sampled_df[neighbour_label][neighbour_side]
        except (KeyError, TypeError):
            piper = None

        return has_neighbour, piped, pipei, piper, neighbour_label, neighbour_side

    def get_row_data_frames(self, row):
        """
        Return data data and information of the pipe slide stored in the 'row' of the table

        Parameters
        ----------
        row: index
            row index

        Returns
        -------
        tuple (pipe_data, pipe_info, pipe_data_sampled, label, side_char)
            Tuple of data frames containing:

             - pipe_data: Profile of data vs phi
             - pipe_data_sampled: Profiles of sampled data vs phi
             - pipe_info: Pipe properties
             - label: the label belonging to this row
             - side_char: The side character belonging to the profile
        """
        # return the data frames belong to the row index of the table 'row'
        label = self.table.item(row, 0).text()
        side_char = self.table.item(row, 1).text()

        pipe_data = self.fitter.pipe_data_df[label][side_char]
        pipe_info = self.fitter.pipe_info_df.loc[label, side_char]
        try:
            pipe_data_sampled = self.fitter.pipe_sampled_df[label][side_char]
        except TypeError:
            pipe_data_sampled = None

        return pipe_data, pipe_info, pipe_data_sampled, label, side_char

    def get_neighbour_index(self, i_row):
        """
        Return the neighbour row of the i_row

        Parameters
        ----------
        i_row: int
            The index of the row we want to get the neighbour from

        Returns
        -------
        Index
            The neighbour slice

        """
        # based on the current index, return the index of the neighbouring pipe end
        # if you are at the start (even row) this is one index back, at the end of a pipe
        # (odd row) this is one index further
        neighbour_row = None
        if 0 < i_row < self.table.rowCount() - 1:
            if i_row % 2 == 0:
                # for even row number your are at a start of a pipe so have to look back
                neighbour_row = i_row - 1
            else:
                neighbour_row = i_row + 1

        return neighbour_row

    def set_table_row_active(self, i_row):
        """
        The the selected row active

        Parameters
        ----------
        i_row: int
            Row number
        """
        # make a table row active. Handy for initialisation of a pre-selected
        if 0 < i_row < self.table.rowCount() - 1:
            label = self.table.item(i_row, 0).text()
            item = QTableWidgetItem(label)
            item.setSelected(True)
            self.table.setCurrentItem(item)
            self.table.scrollToItem(item)

    def update_and_save(self, i_tab, filename=None):
        """
        Update the canvas of this tab  and optionally save to file

        Parameters
        ----------
        i_tab: int
            Number of the tab
        filename: str, optional
            Name of the file to save to. Default = None, which means do not save
        """
        self.canvases[i_tab].draw()

        # if a filename is passed, so the plot to file
        if filename is not None:
            try:
                self.figures[i_tab].savefig(filename)
            except IOError:
                self.logger.warning("could not write this plot {}".format(filename))

    def plot_radius_vs_index_with_histogram(self, i_tab, filename=None):
        """
        Create  a plot of the min, max and mean radius vs position

        Parameters
        ----------
        i_tab: int
            Number of the tab that contains the plot
        filename: str, optional
           Name of the file to save. Default = None, which means do not save
        """

        j_plot = 0

        # link to the data
        df = self.fitter.pipe_info_df

        # First plot at top of canvas: the radius mean/min/max
        phandles = []
        ax = self.axis[i_tab][j_plot]

        try:
            ph, = ax.plot(df.RiMean.values, "o", color='r', markersize=4,
                          label="{:5>}".format("Mean"))
        except AttributeError as err:
            self.logger.warning(err)
            return
        phandles.append(ph)
        self.lines[i_tab].append(ph)

        if self.plot_min_max_checkBox[i_tab].isChecked():
            ph, = ax.plot(df.RiMin.values, "*", color='g', markersize=4,
                          label="{:5>}".format("Min"))
            self.lines[i_tab].append(ph)
            phandles.append(ph)
            ph, = ax.plot(df.RiMax.values, "+", color='b', markersize=4,
                          label="{:5>}".format("Max"))
            self.lines[i_tab].append(ph)
            phandles.append(ph)

        ax.legend(handles=phandles, loc=1, prop=self.fontP,
                  bbox_to_anchor=self.bbox_to_anchor_pos[i_tab][0])

        y_lim = ax.get_ylim()

        # Second plot at top of canvas: the histogram of the radius
        ax = self.axis[i_tab][j_plot + 1]
        ax.clear()

        his, bins = np.histogram(df.RiMean.dropna().values)
        centers = (bins[:-1] + bins[1:]) / 2
        width = (bins[1] - bins[0])
        ax.set_ylim(y_lim)
        # n, bins, patches = ax.hist(data, num_bins, fc='darkred', ec='darkred',
        # orientation='horizontal')
        ph = ax.barh(centers, his, height=width, align='center', alpha=0.7, color='r')
        self.set_histo_axis(ax, "Counts", hide_y_labels=True)
        self.lines[i_tab].append(ph)

        self.update_and_save(i_tab, filename)

        # also try to save all the data of the plot to file
        if filename is not None:
            datafile = re.sub("\.\w+$", ".dat", filename)
            self.logger.info("Writing {}".format(datafile))
            try:
                df.to_csv(datafile, sep=" ",
                          columns=["Rimean", "RiMin", "RiMax", "Ovality", "Rskew", "RiAsym"])
            except IOError:
                self.logger.warning("failed writing data to file {}".format(datafile))

    def plot_ovality_vs_index_with_histogram(self, i_tab, filename=None):
        """
        Plot the ovality vs the index

        Parameters
        ----------
        i_tab: int
            Number of the tab that contains the plot
        filename: str, optional
           Name of the file to save. Default = None, which means do not save
        """
        # this forms contains a grid of 2 by 2 plots, so j_plot is the left bottom plot
        j_plot = 2

        # get filter data (only the fronts of the pipes)
        df = self.fitter.pipe_info_df

        # First plot at top of canvas: the radius mean/min/max
        phandles = []
        ax = self.axis[i_tab][j_plot]

        try:
            ph, = ax.plot(df.Ovality.values, "o", color='g', markersize=4,
                          label="{:5>}".format("Ovality"))
            phandles.append(ph)
            self.lines[i_tab].append(ph)
        except AttributeError as err:
            self.logger.warning(err)
            return

        ax.legend(handles=phandles, loc=1, bbox_to_anchor=self.bbox_to_anchor_pos[i_tab][0])

        y_lim = ax.get_ylim()

        # Second plot at top of canvas: the histogram of the radius
        ax = self.axis[i_tab][j_plot + 1]
        ax.clear()

        his, bins = np.histogram(df.Ovality.dropna().values)
        centers = (bins[:-1] + bins[1:]) / 2
        width = (bins[1] - bins[0])
        ax.set_ylim(y_lim)
        ph = ax.barh(centers, his, height=width, align='center', alpha=0.7, color='g')
        self.set_histo_axis(ax, "Counts", hide_y_labels=True)
        self.lines[i_tab].append(ph)

        self.update_and_save(i_tab, filename)

    def plot_skewness_vs_index_with_histogram(self, i_tab, filename=None):
        """
        Plot the skewness vs the index

        Parameters
        ----------
        i_tab: int
            Number of the tab that contains the plot
        filename: str, optional
           Name of the file to save. Default = None, which means do not save
        """

        # this forms contains a grid of 2 by 2 plots, so j_plot is the left bottom plot
        j_plot = 4

        # get filter data (only the fronts of the pipes)
        df = self.fitter.pipe_info_df

        # First plot at top of canvas: the radius mean/min/max
        phandles = []
        ax = self.axis[i_tab][j_plot]

        try:
            ph, = ax.plot(df.RiSkew.values, "o", color='b', markersize=4,
                          label="{:5>}".format("Skewness"))
            phandles.append(ph)
            self.lines[i_tab].append(ph)
        except AttributeError as err:
            self.logger.warning(err)
            return

        ax.legend(handles=phandles, loc=1, bbox_to_anchor=self.bbox_to_anchor_pos[i_tab][0])

        y_lim = ax.get_ylim()

        # Second plot at top of canvas: the histogram of the radius
        ax = self.axis[i_tab][j_plot + 1]

        his, bins = np.histogram(df.RiSkew.dropna().values)
        centers = (bins[:-1] + bins[1:]) / 2
        width = (bins[1] - bins[0])
        ax.set_ylim(y_lim)
        # n, bins, patches = ax.hist(data, num_bins, fc='darkred', ec='darkred',
        # orientation='horizontal')
        ph = ax.barh(centers, his, height=width, align='center', alpha=0.7,
                     color='b')  # , orientation="horizontal")
        self.lines[i_tab].append(ph)

        self.update_and_save(i_tab, filename)

    def plot_cross_correlations(self, i_tab, filename=None):
        """
        Create a plot of the cross correlations between the pipe ends

        Parameters
        ----------
        i_tab: int
            Current tab number that contains the plot
        filename: str
            Name of the file in case saving the plot is requested
        """

        # clear the curren lines from the plot
        self.clear_lines(self.axis[i_tab], self.lines[i_tab])

        df = self.fitter.pipe_info_df

        n_max_fit = 2 * (self.fitter.n_pipes - 1)

        # First plot at top/left of canvas: the radius vs number of fits
        try:
            ax = self.axis[i_tab][0]
            ph, = ax.plot(df.RiMean.values, df.N_fits_Side, "o", color='r', markersize=4)
            self.lines[i_tab].append(ph)

            # plot a horizontal line at the maximum number of pits
            ph, = ax.plot(ax.get_xlim(), n_max_fit * np.ones(2), "-", color="k")
            self.lines[i_tab].append(ph)

            # plot at top/right of canvas: the radius vs number of fits
            ax = self.axis[i_tab][1]

            ph, = ax.plot(100 * (df.RiMax.values - df.RiMin.values) / df.RiMean.values,
                          df.N_fits_Side, "o", color='g',
                          markersize=4,
                          label="{:5>}".format("Max"))
            self.lines[i_tab].append(ph)
        except AttributeError:
            self.logger.debug("Could not plot ovality vs Nfit")

        try:
            # plot at bottom/left  of canvas: the skewness vs number of fits
            ax = self.axis[i_tab][2]
            ph, = ax.plot(1000 * df.RiSkew.values, df.N_fits_Side.values, "o", color='g',
                          markersize=4,
                          label="{:5>}".format("Max"))
            self.lines[i_tab].append(ph)

            # plot at bottom/right of canvas: the skewness vs the asymmetry
            ax = self.axis[i_tab][3]
            ph, = ax.plot(1000 * df.RiSkew.values, 100 * df.RiAsym.values,
                          "o", color='g', markersize=4,
                          label="{:5>}".format("Max"))
            self.lines[i_tab].append(ph)
        except AttributeError:
            self.logger.debug("Could not skewness vs asym")

        self.update_and_save(i_tab, filename)

    def plot_update_current_tab(self):
        """
        Update the current tab
        """

        # get current tab number
        i_tab = self.tabWidget.currentIndex()
        self.logger.debug("plotting current tab {}".format(i_tab))

        # clear all lines of the current tab
        self.clear_lines(self.axis[i_tab], self.lines[i_tab])

        if i_tab < 2:
            self.update_current_hilomax()

        if i_tab == 0:
            # the polar plot of the currently selected slice
            self.plot_current_slice(i_tab)
        elif i_tab == 1:
            # the linear plot of the currently selected slice + the difference between the
            # neighbouring profiles
            self.plot_current_slice(i_tab, plot_profile_difference=self.show_delta_r_plot_checkBox[
                -1].isChecked(), plot_hilo_vs_phi=self.show_hilo_plot_checkBox[i_tab].isChecked())
        elif i_tab == 2:
            # the statistics vs the index plots
            self.plot_radius_vs_index_with_histogram(i_tab)
            self.plot_ovality_vs_index_with_histogram(i_tab)
            self.plot_skewness_vs_index_with_histogram(i_tab)
        elif i_tab == 3:
            self.plot_cross_correlations(i_tab)
        elif i_tab == 4:
            self.plot_fit_stats_vs_index(i_tab)
        elif i_tab == 5:
            self.plot_fit_stats_histogram(i_tab)
        else:
            self.logger.warning("No plot defined for this tab: {}".format(i_tab))

    def plot_fit_stats_vs_index(self, i_tab, filename=None):

        # plot the fitting quality per pipe vs index

        self.clear_annotations(self.annotations[i_tab])
        # get filter data (only the fronts of the pipes)
        df = self.fitter.pipe_info_df
        try:
            df.dropna()
        except AttributeError:
            pass

        # select only the front of the pipes
        try:
            df = df[df.isFront]
        except AttributeError:
            self.logger.info("Could not select fronts")

        # First plot at top of canvas: the fit criterion  (Current, Min, max) vs index
        phandles = []
        ax = self.axis[i_tab][0]

        pipe_number = np.linspace(1, self.fitter.n_pipes + 1, self.fitter.n_pipes)
        self.logger.debug("fit_thres {}".format(self.fitter.fit_threshold))
        threshold = np.ones(self.fitter.n_pipes) * self.fitter.fit_threshold
        self.logger.debug("hier {} {}".format(np.mean(pipe_number), pipe_number.shape, pipe_number))
        self.logger.debug("hier {} {}".format(np.mean(threshold), threshold.shape, threshold))
        self.logger.debug(
            "hier {} {}".format(df.HiLoMin.min(), df.HiLoMin.values.shape, df.HiLoMin))

        try:
            ph, = ax.plot(pipe_number, df.HiLoMin, "o", color='r', markersize=4,
                          label="{:5>}".format("HiLoMin"))
            phandles.append(ph)
            self.lines[i_tab].append(ph)
        except AttributeError:
            self.logger.info("Could not HiLo vs Index. Perhaps it is not yet calculated?")

        if self.plot_min_max_checkBox[i_tab].isChecked():
            try:
                ph, = ax.plot(pipe_number, df.HiLo, "x", color='g', markersize=4,
                              label="{:5>}".format("HiLo"))
                phandles.append(ph)
                self.lines[i_tab].append(ph)
                ph, = ax.plot(pipe_number, df.HiLoMax, "+", color='b', markersize=4,
                              label="{:5>}".format("HiLoMax"))
                phandles.append(ph)
                self.lines[i_tab].append(ph)
            except AttributeError:
                self.logger.info("Could not HiLoMin vs Index. Perhaps it is not yet calculated?")

        try:
            ph, = ax.plot(pipe_number, threshold, "-", color='c',
                          label="{:5>}".format("Threshold"))
            phandles.append(ph)
            self.lines[i_tab].append(ph)

            self.annotations[i_tab].append(ax.annotate("NFits : {}/{} (failed: {:.1f} %)".format(
                self.fitter.n_total_fitting_pipes, self.fitter.n_pipes,
                100 * (1 - self.fitter.n_total_fitting_pipes / self.fitter.n_pipes)),
                xy=(.025, .975), xycoords='figure fraction'))
            self.annotations[i_tab].append(
                ax.annotate("Ntrashed={}/Ncorrected={}/Nzerofit={}".format(
                    self.fitter.n_trashed_pipes, self.fitter.n_restored_pipes,
                    self.fitter.n_non_fitting_pipes),
                    xy=(.525, .975), xycoords='figure fraction'))
        except (AttributeError, ZeroDivisionError):
            self.logger.info("Could not HiLoMax vs Index. Perhaps it is not yet calculated?")

        ax.legend(handles=phandles, loc=1, bbox_to_anchor=self.bbox_to_anchor_pos[i_tab][0])

        self.update_and_save(i_tab, filename)

        # also try to save all the data of the plot to file
        if filename is not None:
            datafile = re.sub("\.\w+$", ".dat", filename)
            self.logger.info("Wrting {}".format(datafile))
            try:
                df.to_csv(datafile, sep=" ", columns=["HiLoMin", "HiLo", "HiLoMax"])
            except IOError:
                self.logger.warning("failed writing data to file {}".format(datafile))

    def plot_fit_stats_histogram(self, i_tab, filename=None):

        # get filter data (only the fronts of the pipes)
        df = self.fitter.pipe_info_df[self.fitter.pipe_info_df.isFront]

        # First plot at top of canvas: the radius mean/min/max
        phandles = []
        ax = self.axis[i_tab][0]

        try:
            his, bins = np.histogram(df.HiLoMin.dropna().values)
            centers = (bins[:-1] + bins[1:]) / 2
            width = (bins[1] - bins[0])
            # clear the axis the remove the previous boxes
            ax.clear()
            # because of clear, we need to update the axis
            ax.bar(centers, his, width=width, align='center', alpha=0.7, color='b')
            self.set_histo_axis(self.axis[-1][-1], x_label="HiLo [mm]")
        except (KeyError, AttributeError) as err:
            self.logger.info("did not plot because : {}".format(err))

        self.update_and_save(i_tab, filename)

    def plot_current_slice(self, i_tab,
                           plot_profile_difference=False,
                           plot_hilo_vs_phi=None,
                           filename=None):
        """
        Make a plot of the currently selected slice

        Parameters
        ----------
        i_tab: int
            Number of the tab that hold the plot. This can be 0 (for the polar plot) or 1 (for the
            linear plot)
        plot_profile_difference: bool
            In case we make the linear plot (i_tab == 1), we can also add the difference between the
            profiles
        plot_hilo_vs_phi: bool, optional
            In case we make the linear plot (i_tab == 1), we can also add the Hilo profile.
            Default = None
        filename: str, optional
            Name of the file to save the image to. Default = None, which means do not save
        """

        # the index of row of the current plot.
        j_plot = 0

        self.clear_annotations(self.annotations[i_tab])

        tablerow = self.table.currentRow()
        if tablerow > -1:

            pipe_data, pipe_info, pipe_sampled_data, label, sidechar = \
                self.get_row_data_frames(tablerow)

            # in case a neighbour was set: load this data as well and store in pipe_data_2, etc
            has_neighbour, pipe_data_2, pipe_info_2, pipe_sampled_data_2, label2, sidechar2 = \
                self.get_neighbour_data_frame(pipe_info)

            ri_neighbour_mirrored = None
            ri_neighbour_mirrored_sampled = None
            ri_ok = None
            ri_ok_sampled = None
            delta_r = None
            delta_r_sampled = None
            if has_neighbour:
                try:
                    ri_neighbour_mirrored = self.shift_over_phi(pipe_data_2.Ri[::-1].values,
                                                                self.current_phi_shift)
                except (UnboundLocalError, AttributeError)as e:
                    self.logger.info("could not plot because {}".format(e))
                else:
                    delta_r = pipe_data.Ri.values - ri_neighbour_mirrored
                    ri_ok = np.where(abs(delta_r) <= self.fitter.fit_threshold, pipe_data.Ri,
                                     np.nan)

                try:
                    ri_neighbour_mirrored_sampled = self.shift_over_phi(
                        pipe_sampled_data_2.Ri[::-1].values, self.current_phi_shift)
                except AttributeError:
                    self.logger.debug("could get sampled ri")
                else:
                    delta_r_sampled = pipe_sampled_data.Ri.values - ri_neighbour_mirrored_sampled
                    ri_ok_sampled = np.where(delta_r_sampled <= self.fitter.fit_threshold,
                                             pipe_sampled_data.Ri, np.nan)

            # depending on what tab we are we need to convert the phi angle from radians to degrees.
            # The polar plot on i_tab==0 needs radians, whereas the linear plot on i_tab==1 we want
            # degrees.
            if i_tab == 1:
                self.logger.debug("setting phi for linear plots to degrees")
                phid = np.rad2deg(pipe_data.index)
                try:
                    phid2 = np.rad2deg(pipe_data_2.index)
                except AttributeError:
                    pass
                try:
                    phir = np.rad2deg(pipe_sampled_data.index)
                except AttributeError:
                    pass
            else:
                self.logger.debug("setting phi for polar plots to radians")
                phid = pipe_data.index
                try:
                    phid2 = pipe_data_2.index
                except AttributeError:
                    pass
                try:
                    phir = pipe_sampled_data.index
                except AttributeError:
                    pass

            color1 = "r"
            color2 = "b"

            try:
                if pipe_data.HiLo is not None:
                    self.logger.debug("Hi Low found! {}".format(pipe_data.HiLo.values.shape))
                    self.logger.debug("Statistics: {} {} {}"
                                      "".format(pipe_data.HiLo.mean(), pipe_data.HiLo.min(),
                                                pipe_data.HiLo.max()))
                else:
                    self.logger.debug("Hi Low there but none")
            except AttributeError:
                self.logger.debug("No Hi Found in the data")

            self.logger.debug("found phi value: {}".format(self.current_phi_shift))

            # plot this plot in the first plot of tab 0 (the polar plot) is i=0 and the first
            # (out of two) plot of tab 1 (the linear plot)
            ax = self.axis[i_tab][j_plot]

            phandles = []

            if ri_ok is not None:
                ph, = ax.plot(phid, ri_ok, "-", color="green", linewidth=3)
                self.lines[i_tab].append(ph)

            # plot the current active polar plot of the radius
            ph, = ax.plot(phid, pipe_data.Ri, "-", color=color1,
                          linewidth=1, markersize=1,
                          label="{:5>}".format("{}_{}".format(label, sidechar)))
            self.lines[i_tab].append(ph)
            phandles.append(ph)

            # in case a neighbour is present, plot this one too
            if has_neighbour:
                # always plot the neigh in reverse order. This implies that the phi sample points
                # have to be symmetric around 0 and that the  first and last point coincide!
                ph, = ax.plot(phid2, ri_neighbour_mirrored, "-", color=color2,
                              linewidth=1, markersize=1,
                              label="{:5>}".format("{}_{}".format(label2, sidechar2)))

                self.lines[i_tab].append(ph)
                phandles.append(ph)

                philabel = r"$\phi_{rotation}$"
                phiopt = ""
                color = "black"

                # with a neighbour, also add a annotation with the current rotation
                try:
                    phi_shift = pipe_info_2.PhiOptimal + pipe_info.PhiOptimal
                except AttributeError:
                    self.logger.debug("No optimal phi yet")
                else:
                    # In case the current phi equals the optimal phi, turn the label green
                    if np.allclose([self.current_phi_shift], [phi_shift]):
                        color = "green"
                self.annotations[i_tab].append(
                    ax.annotate(
                        "{}={:6.1f}$^\circ$ {}".format(philabel, self.current_phi_shift, phiopt),
                        size=14,
                        xy=(.8, .02), xycoords='figure fraction', color=color)
                )

            if self.fitter.pipe_sampled_df is not None and self.show_resample_points_checkBox[
                i_tab].isChecked():
                ph, = ax.plot(phir, pipe_sampled_data.Ri, "o", color=color1, markersize=4)
                self.lines[i_tab].append(ph)
                if has_neighbour and pipe_sampled_data_2 is not None:
                    ph, = ax.plot(phir, ri_neighbour_mirrored_sampled,
                                  "o", color=color2, markersize=4)
                    self.lines[i_tab].append(ph)

            ax.legend(handles=phandles, title='Pipe', loc=1, prop=self.fontP, fancybox=True,
                      shadow=False,
                      bbox_to_anchor=self.bbox_to_anchor_pos[i_tab][j_plot])

            if has_neighbour and plot_profile_difference and delta_r is not None:
                phandles = []
                # plot the difference between the profiles delta R
                # the second plot of tab 1
                ax = self.axis[i_tab][1]
                # plot the difference between de current pipe with its neighbour
                ph, = ax.plot(phid, delta_r, "-",
                              color="g", linewidth=1, markersize=4, label="Delta R")
                self.lines[i_tab].append(ph)
                phandles.append(ph)

                if pipe_sampled_data is not None and pipe_sampled_data_2 is not None and \
                        self.show_resample_points_checkBox[i_tab].isChecked():
                    try:
                        delta_r_s = pipe_sampled_data.Ri.values - \
                                    ri_neighbour_mirrored_sampled,
                    except (UnboundLocalError, AttributeError)as e:
                        self.logger.info("could not plot because {}".format(e))
                    else:
                        ph, = ax.plot(phir, delta_r_s, "o", color="g", markersize=4)
                        self.lines[i_tab].append(ph)

                if plot_hilo_vs_phi:
                    #  plot the hilo vs phi in the second plot as well.
                    try:
                        ax = self.axis[i_tab][2]
                        # plot the difference between de current pipe with its neighbour
                        ph, = ax.plot(phid, pipe_data.HiLo.values, "-", color="k",
                                      linewidth=1, markersize=4,
                                      label="Max HiLo")
                        self.lines[i_tab].append(ph)
                        phandles.append(ph)

                        # add a sphere at the current phi location

                        ph, = ax.plot([self.current_phi_shift], [self.current_hilo_max], "o",
                                      color="r",
                                      linewidth=1, markersize=5)
                        self.lines[i_tab].append(ph)

                    except (UnboundLocalError, AttributeError)as e:
                        self.logger.info("could not plot because {}".format(e))
                        pass

                if phandles:
                    ax.legend(handles=phandles, loc=1, prop=self.fontP, fancybox=True, shadow=False,
                              bbox_to_anchor=self.bbox_to_anchor_pos[i_tab][1])

            self.update_and_save(i_tab, filename)

    @staticmethod
    def shift_over_phi(radius_array, phi_shift):
        """
        Shift the current radial profile over angle *phi_shift*

        Parameters
        ----------
        radius_array: ndarray
            Radius to be shifted. Must be closed, i.e start and end is the same
        phi_shift: float
            Angle in degrees to shift. Assumes that the radius runs from 0 to 2pi

        Returns
        -------
        ndarray:
            Shifted radius array

        """

        # the number of positions to shift the radius based on the phi_shift and the total length
        # of the array
        k_roll = int(round((radius_array.size - 1) * phi_shift / 360))

        if k_roll != 0:
            # because we have assumed the the radius_array is closed (value at 0 is equal to value
            # at N-1, so we can mirror the data), we only roll only all but the last points
            radius_rolled = np.roll(radius_array[:-1], k_roll)

            # close the radius again by copying the first point to the end
            radius_rolled = np.hstack((radius_rolled, radius_rolled[0]))
        else:
            # we don not roll, so just return the input array
            radius_rolled = radius_array

        return radius_rolled

    def clear_bars(self, axis, bars):
        for i, bar in enumerate(bars):
            for ax in axis:
                try:
                    ax.bars.remove(bar)
                except ValueError:
                    # I don't care which axis this line belonged to. Just remove it in case 'try'
                    # allows
                    pass
        bars = []

    @staticmethod
    def clear_lines(axis, lines):
        """
        Remove the lines from the current plot.

        Parameters
        ----------
        axis: Axis object
        lines: list
            List of lines to remove
        """
        # remove the lines from the current plot. We need to store all the lines in a list so we
        # can explicitly remove it line by line. After that the lines can be send to the garbage
        # collection by setting it to []
        for i, line in enumerate(lines):
            for ax in axis:
                try:
                    ax.lines.remove(line)
                    line.remove()
                    del line
                except ValueError:
                    # I don't care which axis this line belonged to. Just remove it in case 'try'
                    # allows
                    pass
        lines = []

    @staticmethod
    def clear_annotations(annotations):
        """
        remove the annotations from the current plot.

        Parameters
        ----------
        annotations: list
            List of annotation to remove

        """
        for i, annotation in enumerate(annotations):
            try:
                annotation.remove()
            except ValueError:
                # I don't care which axis this line belonged to. Just remove it in case 'try' allows
                pass
        annotations = []

    def scan_import(self):
        """
        Import the current scans
        """
        self.logger.info("start batch processing")
        if self.ScanImportAction.isChecked():
            self.logger.debug("Open the dialog")
            if self.scanImportDlg is None:
                self.scanImportDlg = ScanImportDlg(self.fitter, self.ScanImportAction)
            self.scanImportDlg.update_logger(self.logger.level)
            self.scanImportDlg.show()
            self.ScanImportAction.setEnabled(False)
        else:
            self.close_scan_import()

    # @profile
    def close_scan_import(self):
        if self.scanImportDlg is not None:
            self.scanImportDlg.hide()
        if self.ScanImportAction.isChecked():
            self.ScanImportAction.setChecked(False)

    def show_pipe_details(self):
        """
        show the details of the current slice
        """
        current = self.current_slice()
        pipe = self.fitter.pipe_info_df.ix[current]

        detaildlg = DetailDlg(current, pipe)
        detaildlg.update_logger(self.logger.level)
        detaildlg.exec_()

    def generate_data(self):
        self.logger.info("start data generation")
        if self.generateDataAction.isChecked():
            self.logger.debug("Open the dialog")
            if self.generateDataDlg is None:
                self.generateDataDlg = GenerateDataDlg(self.fitter, self.generateDataAction)
            self.generateDataDlg.update_logger(self.logger.level)
            self.generateDataDlg.show()
            self.generateDataAction.setEnabled(False)
        else:
            self.close_generate_data()

    # @profile
    def close_generate_data(self):
        if self.generateDataDlg is not None:
            self.generateDataDlg.hide()
        if self.generateDataAction.isChecked():
            self.generateDataAction.setChecked(False)

    def update_process_settings(self):
        """
        Update the dialog settings
        """
        # set some the fit parameters
        self.fitter.fit_threshold = self.process_steps["fit_threshold_value"]

        # the exclude range is given in degrees but should be converted to radians
        self.fitter.exclude_phi_range_weld = np.radians([
            self.process_steps["exclude_phi_range_weld"]])[0]
        self.fitter.exclude_phi_range_six_oclock = np.radians([
            self.process_steps["exclude_phi_range_six_oclock"]])[0]
        self.fitter.optimize_phi_at_translations = self.process_steps[
            "optimize_phi_at_translations"]
        self.fitter.n_shift_points = self.process_steps["n_shift_points"]

    def open_preprocess_dialog(self):
        """ Open the pre prossing dialog"""

        if self.preProcessAction.isChecked():
            self.logger.info("Open Pre-process dialog")
            self.preProcessDlg = PipeManipulateDlg(self.process_steps, self.preProcessAction)
            self.preProcessAction.setChecked(True)
            self.preProcessAction.setEnabled(False)

            self.preProcessDlg.update_logger(self.logger.level)
            self.preProcessDlg.execute_pre_process.connect(self.prepare_all_pipes)
            self.preProcessDlg.show()

    def pre_process_all(self):
        self.logger.info("All Pre Processing steps are carried out")

        # transfer all dialog values to the fitter object
        self.update_process_settings()

        self.fitter.centralise_all_profiles()

        self.fitter.duplicate_missing_pipe_side()
        self.update_table()

        self.fitter.store_current_neighbours()
        self.update_table()

        n_sample_points = self.process_steps["n_sample_points"]
        self.fitter.pipe_resample(n_sample_points)

        self.fitter.update_pipe_statistics()
        self.update_table()

        self.fitter.update_fit_quality_all_pipe(use_resampled_data=True)

        self.fitter.build_fit_matrix()

        self.fitter.update_n_fits_per_pipe()

        self.fitter.sort_on_n_fits()

        self.fitter.update_fit_quality_all_pipe(use_resampled_data=False)

        self.update_table()

        # update the plots of the current visible tab
        self.plot_update_current_tab()

    def prepare_all_pipes(self):
        """
        This method is called after pressin the apply butting. Only the checked items are carried out
        """

        self.logger.info("Do the preparation")

        # transfer all dialog values to the fitter object
        self.update_process_settings()

        if self.process_steps["centralise"]:
            self.logger.debug("Calling fitter.centralise_all_profiles()")
            self.fitter.centralise_all_profiles()

        if self.process_steps["duplicate"]:
            self.logger.debug("DUPLICATING")
            self.fitter.duplicate_missing_pipe_side()
            self.update_table()

        if self.process_steps["find_neighbours"]:
            self.logger.debug("Finding the neighbours")
            self.fitter.store_current_neighbours()
            self.update_table()
            self.hide_progress_bar()

        if self.process_steps["resample_data"]:
            n_sample_points = self.process_steps["n_sample_points"]
            self.logger.debug("Resamping the profiles with {} points".format(n_sample_points))
            self.fitter.pipe_resample(n_sample_points)
            self.update_table()

        if self.process_steps["update_statistics"]:
            self.logger.debug("Updating the statistics")
            self.fitter.update_pipe_statistics()
            self.update_table()

        if self.process_steps["pipe_fitting_matrix"]:
            self.logger.debug("Prepare fit matrix the pipes")
            self.fitter.build_fit_matrix()
            self.update_table()

        if self.process_steps["update_n_fits_per_pipe"]:
            self.logger.debug("Updating n fits per pipe")
            self.fitter.update_n_fits_per_pipe()
            self.update_table()

        if self.process_steps["pre_sort_on_n_fit"]:
            self.logger.debug("Pre-sorting the pipes")
            self.fitter.sort_on_n_fits()
            self.update_table()

        if self.process_steps["fit_quality"]:
            if self.process_steps["fit_quality_resolution"] == 0:
                self.logger.debug("Calculating the fit Quality using full resolution")
                self.fitter.update_fit_quality_all_pipe(use_resampled_data=False)
            else:
                self.logger.debug("Calculating the fit Quality using resampled data")
                self.fitter.update_fit_quality_all_pipe(use_resampled_data=True)
            self.update_table()

        # update the plots of the current visible tab
        self.plot_update_current_tab()

    def swap_current_pipe_direction(self):
        """
        Move the pipe in the table down
        """

        table_row = self.table.currentRow()
        if table_row > -1:
            label = self.table.item(table_row, 0).text()
            self.fitter.swap_pipe_direction(label)
            self.update_table()
            self.table.selectRow(table_row)

    def move_current_pipe_up(self):
        """
        Move the pipe in the table down
        """

        table_row = self.table.currentRow()
        if table_row > -1:
            label = self.table.item(table_row, 0).text()
            try:
                self.fitter.move_pipe_in_list(label, "up")
            except IndexError:
                self.logger.debug("Cannot move down. End of table reached ")
            else:
                self.update_table()
                self.table.selectRow(table_row - 2)

    def move_current_pipe_down(self):
        """
        Move the pipe in the table down
        """
        table_row = self.table.currentRow()
        if table_row > -1:
            label = self.table.item(table_row, 0).text()
            try:
                self.fitter.move_pipe_in_list(label, "down")
            except IndexError:
                self.logger.debug("Cannot move down. End of table reached ")
            else:
                self.update_table()
                self.table.selectRow(table_row + 2)

    def OpenSortPipesDlg(self):

        if self.sortPipesAction.isChecked():
            self.logger.info("Open Sorting dialog")
            self.sortPipesDlg = PipeSortingDlg(self.sort_settings, self.sortPipesAction)
            self.sortPipesAction.setChecked(True)
            self.sortPipesAction.setEnabled(False)

            self.sortPipesDlg.update_logger(self.logger.level)

            self.sortPipesDlg.execute_sort_pipes.connect(self.sort_all_pipes)
            self.sortPipesDlg.show()

    def close_sort_pipe_dialog(self):
        try:
            self.sortPipesDlg.hide()
            self.logger.debug(
                "closed sort pipe dlg with thresh {}".format(self.fitter.fit_threshold))
        except (AttributeError, RuntimeError):
            self.logger.debug("Failed to hide sort")
            pass
        if self.sortPipesAction.isChecked():
            self.sortPipesAction.setChecked(False)
        self.sortPipesDlg = None

    def sort_all_pipes(self):
        self.fitter.fit_threshold = self.process_steps["fit_threshold_value"]
        self.logger.debug("Calling full sort  with {}".format(self.fitter.fit_threshold))
        self.fitter.get_sorted_pipe_list(sorting_method=self.sort_settings["sorting_method"],
                                         n_pipes_in_pool_minimum=self.sort_settings[
                                             "n_pipes_in_pool_minimum"])
        self.logger.debug("Finding the neighbours")
        self.fitter.store_current_neighbours()
        self.fitter.update_fit_quality_all_pipe(use_resampled_data=False)
        self.update_table()
        self.plot_update_current_tab()
        self.logger.info("After fit: number of pipes total {}".format(self.fitter.n_pipes))
        self.logger.info("number of trashed pipes {}".format(self.fitter.n_trashed_pipes))
        self.logger.info("number of non fittig pipes {}".format(self.fitter.n_non_fitting_pipes))
        self.logger.info("number of restored pipes {}".format(self.fitter.n_restored_pipes))

    def edit_axis(self):
        i_tab = self.tabWidget.currentIndex()
        self.toolbars[i_tab].edit_parameters()

    def save_figure(self):
        i_tab = self.tabWidget.currentIndex()
        self.toolbars[i_tab].save_figure()

    def set_table_column(self, i_row, col, data, name, format_specifier="{}"):
        """
        Fill in a data fields of the table

        Parameters
        ----------
        i_row: int
            Row to fill
        col:  str
            Column to fill
        data: DataFrame
           DataFrame containing the data
        name: str
            Name of the column to fill
        format_specifier: str, optional
            Format specifier to use. Default = "{}"
        """
        try:
            item = QTableWidgetItem(format_specifier.format(data[name]))
            # align the numbers to the right
            item.setTextAlignment(QtCore.Qt.AlignRight)
            self.table.setItem(i_row, col, item)
        except (AttributeError, KeyError):
            pass

    def update_table(self, current=None):
        """
        Update the current table  properties

        Parameters
        ----------
        current: Pipe
            Current pipe to high light
        """

        self.logger.debug("Updating the table")
        brushes = list()
        brushes.append(QBrush(QColor(0, 0, 0)))
        brushes.append(QBrush(QColor(200, 0, 0)))
        brushes.append(QBrush(QColor(0, 200, 0)))

        self.table.clear()
        self.table.setRowCount(len(self.fitter.pipe_info_df.index))
        self.table.setColumnCount(8)
        self.table.setHorizontalHeaderLabels(
            ["Label", "S", "Neighbour", "HiLo", "Phi", "PhiTot", "NFitPipe", "NFitSide"])

        # the groupby method does not work as it does not take into account any sort. Abandon this
        # and use the method proposed on stackoverflow
        # http://stackoverflow.com/questions/32005508/python-pandas-groupby-after-sorted-multindex
        # -not-correct/32007731#32007731
        # for i_pipe, (label, pipeDF_grouped) in enumerate(self.fitter.pipe_info_df.groupby(level=0)):

        label_list = []
        selected = None
        i_row = 0
        for (label, side_dummy) in self.fitter.pipe_info_df.index:
            if label not in label_list:
                label_list.append(label)
            else:
                continue

            pipe_df_grouped = self.fitter.pipe_info_df.loc[label]
            sides_list = pipe_df_grouped.index.get_level_values('Side')

            # if not self.fitter.pipe_info_df.ix[(label, sides_list[0])].isFront:
            #    sides_list = np.roll(sides_list, 1).tolist()

            # each pipe has two sides, to the row contour should be multiplied with 2

            for i, side in enumerate(sides_list):

                data = self.fitter.pipe_info_df.ix[(label, side)]

                item = QTableWidgetItem(label)

                if current is not None and current == id(label):
                    selected = item
                item.setData(QtCore.Qt.UserRole, id(label))
                self.table.setItem(i_row, 0, item)

                item = QTableWidgetItem(side)
                self.table.setItem(i_row, 1, item)

                try:
                    format = "{}" + "_{}".format(data["NeighbourSide"])
                    self.set_table_column(i_row, 2, data, "NeighbourLabel", format)
                except KeyError:
                    pass

                self.set_table_column(i_row, 3, data, "HiLoMin", "{:.1f}")
                self.set_table_column(i_row, 4, data, "PhiOptimal", "{:.0f}")
                self.set_table_column(i_row, 5, data, "PhiCumulative", "{:.0f}")

                self.set_table_column(i_row, 6, data, "N_fits_Pipe", "{:.0f}")
                self.set_table_column(i_row, 7, data, "N_fits_Side", "{:.0f}")

                i_row += 1

        self.table.resizeColumnsToContents()
        if selected is not None:
            selected.setSelected(True)
            self.table.setCurrentItem(selected)
            self.table.scrollToItem(selected)

        self.dirty = True

        self.sortPipesAction.setEnabled(True)

    def current_slice(self):
        """
        returns a tuple of the current pipe and side
        """
        row = self.table.currentRow()
        if row > -1:
            label = self.table.item(row, 0)
            side_char = self.table.item(row, 1)
            return label.data(0), side_char.data(0)
        return None

    def addActions(self, target, actions):
        for action in actions:
            if action is None:
                target.addSeparator()
            else:
                target.addAction(action)

    def createAction(self,
                     text=None,
                     slot=None,
                     shortcut=None,
                     icon=None,
                     tip=None,
                     checkable=False,
                     signal="triggered",
                     disabled=False):
        """
        Create a connection between a slot and a signal

        Parameters
        ----------
        text: str, optional
            Text to display in the  menu
        slot:  object, optional
            Slot to connect to the button
        shortcut: object, optional
            Short cut key sequence
        icon: object, optional
            image to show
        tip: str, optional
            String to pop up when hoovering above the button
        checkable: bool, optional
            If true this button can be kept checked
        signal: object, optional
            signal to connect to the slot
        disabled: bool, optional
            If true, gray out button to disable it

        Returns
        -------

        object:
            action

        """

        action = QAction(text, self)
        if icon is not None:
            action.setIcon(QtGui.QIcon(":/{}.png".format(icon)))
        if shortcut is not None:
            action.setShortcut(shortcut)
        if tip is not None:
            action.setToolTip(tip)
            action.setStatusTip(tip)
        if slot is not None:
            getattr(action, signal).connect(slot)
        if checkable:
            action.setCheckable(True)
        if disabled:
            action.setEnabled(False)
        return action

    def closeEvent(self, event):
        if self.ok_to_continue():
            # save the current state of the window and files
            settings = QtCore.QSettings()
            settings.setValue("LastFile", self.filename)
            settings.setValue("LastExportPlots", self.export_plotsfilebase)
            settings.setValue("LastExportTable", self.export_table_data)
            settings.setValue("LastMatchUpReport", self.export_match_up_report)
            settings.setValue("process_settings", self.process_steps)
            settings.setValue("sort_settings", self.sort_settings)
            settings.setValue("RecentFiles", self.recentFiles or [])
            settings.setValue("PipeFitterMainWindow/Geometry", self.saveGeometry())
            settings.setValue("PipeFitterMainWindow/State", self.saveState())

            # make sure that the dialog is also closed
            self.close_scan_import()
            self.preProcessDlg = None
            self.close_sort_pipe_dialog()
            self.close_generate_data()
        else:
            event.ignore()

    def ok_to_continue(self):
        if self.dirty:
            reply = QMessageBox.question(self,
                                         "Settings changed",
                                         "Save unsaved changes?",
                                         QMessageBox.Yes | QMessageBox.No |
                                         QMessageBox.Cancel)
            if reply == QMessageBox.Cancel:
                return False
            elif reply == QMessageBox.Yes:
                self.fileSave()
        return True

    # @profile
    def load_initial_file(self):
        settings = QtCore.QSettings()
        fname = settings.value("LastFile")
        if not self.reset_settings:
            process_steps_stored = settings.value("process_settings")
        else:
            process_steps_stored = None

        if process_steps_stored is not None:
            self.process_steps = process_steps_stored
            try:
                self.fitter.fit_threshold = self.process_steps["fit_threshold_value"]
            except KeyError:
                self.logger.info(
                    "could not set fit_threshold_value from previous run. Taking default")
        if not self.reset_settings:
            sort_settings_stored = settings.value("sort_settings")
        else:
            sort_settings_stored = None

        if sort_settings_stored is not None:
            self.sort_settings = sort_settings_stored

        if not self.reset_settings:
            export_fname = settings.value("LastExportPlots")
        else:
            export_fname = None

        if export_fname is not None:
            self.export_plotsfilebase = export_fname

        if not self.reset_settings:
            export_table_fname = settings.value("LastExportTable")
        else:
            export_table_fname = None

        if export_table_fname is not None:
            self.export_table_data = export_table_fname

        if not self.reset_settings:
            export_match_up_report = settings.value("LastMatchUpReport")
        else:
            export_match_up_report = None
        if export_match_up_report is not None:
            self.export_match_up_report = export_match_up_report

        if fname and QtCore.QFile.exists(fname):
            self.logger.debug("loading {}".format(fname))
            self.statusBar().showMessage(
                "Initialising configuration : {} ...".format(os.path.basename(fname)))
            self.filename = fname
            self.updateStatus("Loaded initial File {}".format(self.filename))
            try:
                self.load_file(fname)
            except AttributeError:
                self.logger.warning("Could not load last saved file. Continue with empty")
                self.statusBar().showMessage(
                    "Failed loading last. Now initialising default configuration...")

        else:
            self.logger.debug("setting initial parameters ")
            self.statusBar().showMessage("Initialising default configuration...")

        self.statusBar().showMessage("Ready.", 5000)

    def update_file_menu(self):
        self.fileMenu.clear()
        self.addActions(self.fileMenu, self.fileMenuActions[:-1])
        current = self.filename
        recentFiles = []
        for fname in self.recentFiles:
            if fname != current and QtCore.QFile.exists(fname):
                recentFiles.append(fname)
        if recentFiles:
            self.fileMenu.addSeparator()
            for i, fname in enumerate(recentFiles):
                action = QAction(QtGui.QIcon(":/icon.png"),
                                 "&{} {}".format(i + 1, QtCore.QFileInfo(
                                     fname).fileName()), self)
                action.setData(fname)
                action.triggered.connect(self.load_file)
                self.fileMenu.addAction(action)
        self.fileMenu.addSeparator()
        self.fileMenu.addAction(self.fileMenuActions[-1])

    def file_open(self):
        if not self.ok_to_continue():
            return
        dir = os.path.dirname(self.filename) \
            if self.filename is not None else "."

        # python 3 version
        formats = (["*.{}".format(extension.lower())
                    for extension in ["pfd", "hdf5"]])
        open_file = QFileDialog.getOpenFileName(self,
                                                "Pipe Fitter - Choose Slice Data", dir,
                                                "Slice Data files ({})".format(
                                                    " ".join(formats)))
        if isinstance(open_file, tuple):
            # in pyqt5 getOpenFileName return a tuple, get the
            fname = open_file[0]
        else:
            # in pyqt4 we already got the filename from getOpenFilename
            fname = open_file

        if fname:
            self.load_file(fname)

    def load_file(self, fname=None):
        """
        Load the current file

        Parameters
        ----------
        fname: str
            Name of the file to load
        """
        if fname is None:
            action = self.sender()
            if isinstance(action, QAction):
                try:
                    fname = action.data().toString()
                except AttributeError:
                    fname = action.data()

                if not self.ok_to_continue():
                    return
            else:
                return
        if fname:
            self.filename = None
            try:
                self.fitter.load_slice_data(fname)
            except ImportError:
                filebase, ext = os.path.splitext(fname)

                message = "Failed to load the input file {}. ".format(fname)
                if ext == ".hdf5":
                    message += "Filetype HDF5 is not supported for the executable. "
                    message += "Please take the PFD format"

                self.logger.debug("{}".format(message))
                QMessageBox.warning(self, "PipeFitter Import Error", message)
                return

            self.addRecentFile(fname)
            self.filename = fname
            self.dirty = False

            self.update_table()

            self.sortPipesAction.setEnabled(True)

            message = "Loaded {}".format(os.path.basename(fname))
            self.logger.debug("{}".format(message))

            self.updateStatus(message)

            # resample the data
            # self.fitter.pipe_resample()

            self.set_table_row_active(0)

            # self.replot_all()

            # self.plot_radius_difference()

    def export_all_plots(self):
        self.logger.debug("export all plots")
        fname = self.export_plotsfilebase if self.export_plotsfilebase is not None else "."
        fname, ext = os.path.splitext(fname)
        formats = (["*.{}".format(format.lower()) for format in ["pdf", "eps", "png"]])
        fname = QFileDialog.getSaveFileName(self, "Pipe Fitter - Export all plots", fname,
                                            "Data files ({})".format(" ".join(formats)))
        if isinstance(fname, tuple):
            # in pyqt5 getOpenFileName return a tuple, get the first item
            fname = fname[0]

        if fname:
            if "." in fname:
                fname, ext = os.path.splitext(fname)
            else:
                ext = ".pdf"

            # store for later usage
            self.export_plotsfilebase = fname

            for i_tab in range(self.n_tabs):
                tab_name = re.sub("\s+", "_", self.tab_names[i_tab])
                plotfile = "{}_{}{}".format(fname, tab_name, ext)
                self.logger.debug("Plotting to {}".format(plotfile))
                self.show_message_in_status_bar("Writing plot {}".format(plotfile), timer=2000)
                if i_tab < 2:
                    # the radius plot (polar for i_tab==0 and linear for i_tab == 1)
                    if i_tab == 0:
                        plot_difference = False
                        plot_hilo = False
                    else:
                        plot_difference = self.show_delta_r_plot_checkBox[i_tab].isChecked()
                        plot_hilo = self.show_hilo_plot_checkBox[i_tab].isChecked()

                    self.plot_current_slice(i_tab, plot_profile_difference=plot_difference,
                                            plot_hilo_vs_phi=plot_hilo,
                                            filename=plotfile)

                elif i_tab == 2:
                    self.plot_radius_vs_index_with_histogram(i_tab, filename=plotfile)
                    self.plot_ovality_vs_index_with_histogram(i_tab, filename=plotfile)
                    self.plot_skewness_vs_index_with_histogram(i_tab, filename=plotfile)
                elif i_tab == 3:
                    self.plot_cross_correlations(i_tab, filename=plotfile)
                elif i_tab == 4:
                    self.plot_fit_stats_vs_index(i_tab, filename=plotfile)
                elif i_tab == 5:
                    self.plot_fit_stats_histogram(i_tab, filename=plotfile)
                else:
                    self.logger.warning("no plot defined for tab {}".format(i_tab))

    def fileSave(self):
        if self.filename is None:
            self.fileSaveAs()
        else:
            try:
                self.fitter.save_slice_data(self.filename)
                self.updateStatus("Saved as %s" % self.filename)
                self.dirty = False
            except (IOError, ImportError):
                filebase, ext = os.path.splitext(self.filename)

                message = "Failed to save the input file {}. ".format(self.filename)
                if ext == ".hdf5":
                    message += "Filetype HDF5 is not supported for the executable. "
                    message += "Please take the PFD format"

                self.logger.warning("{}".format(message))
                QMessageBox.warning(self, "PipeFitter Export Error", message)

                self.updateStatus("Failed to save %s" % self.filename)

    def fileSaveAs(self):
        fname = self.filename if self.filename is not None else "."

        formats = (["*.{}".format(format.lower())
                    for format in ["pfd", "hdf5"]])
        fname = QFileDialog.getSaveFileName(self,
                                            "Pipe Fitter - Save Slice Data", fname,
                                            "Data files ({})".format(" ".join(formats)))
        if isinstance(fname, tuple):
            # in pyqt5 getOpenFileName return a tuple, get the first item
            fname = fname[0]

        if fname:
            if "." not in fname:
                fname += ".xls"
            self.addRecentFile(fname)
            self.filename = str(fname)
            self.fileSave()

    def export_info_to_excel(self):
        """
        Write the current file list to + a statistics overall sheet to excel file 
        """

        self.logger.debug("export table data")
        open_file = self.export_table_data if self.export_table_data is not None else "."
        open_file, ext = os.path.splitext(open_file)
        formats = (["*.{}".format(file_format.lower()) for file_format in ["xls"]])
        open_file = QFileDialog.getSaveFileName(self, "Pipe Fitter - EXport Table Data", open_file,
                                                "Excel File ({})".format(" ".join(formats)))
        if isinstance(open_file, tuple):
            # in pyqt5 getOpenFileName return a tuple, get the
            fname = open_file[0]
        else:
            # in pyqt4 we already got the filename from getOpenFilename
            fname = open_file

        if fname:

            # store for later usage
            self.export_table_data = fname

            try:
                self.fitter.export_info_to_excel(self.export_table_data)
                self.updateStatus("Saved as %s" % self.export_table_data)
                self.dirty = False

            except (IOError, ImportError):
                filebase, ext = os.path.splitext(self.export_table_data)

                message = "Failed to save the input file {}. ".format(self.export_table_data)

                self.logger.warning("{}".format(message))
                QMessageBox.warning(self, "PipeFitter Export Error", message)

                self.updateStatus("Failed to save %s" % self.export_table_data)

    def export_match_up_report(self):
        """
        Create a report of the pipe that has been selected + its neighbour. Two pipes can be forced to
        be neighbours by selecting two pipes in the table and select Match Up in the right mouse button menu
        """

        tablerow = self.table.currentRow()
        if tablerow > -1:
            # get the label of the selected row
            piped, pipei, piper, label, sidechar = self.get_row_data_frames(tablerow)

            # get the labels of the neighbour as well
            has_neighbour, piped2, pipei2, piper2, label2, sidechar2 = self.get_neighbour_data_frame(
                pipei)
            if not has_neighbour:
                # if there is no neighbour: go back
                message = "This pipe does not have a neighbour {} {}. ".format(label, sidechar)
                self.logger.warning("{}".format(message))
                QMessageBox.warning(self, "PipeFitter Export Matchup Error", message)
                return

            default_name = "match_up_{}_{}_{}_{}".format(label, sidechar, label2, sidechar2)

            if self.export_match_up_report is not None:
                export_path = os.path.split(self.export_match_up_report)[0]
            else:
                export_path = "."

            fname = os.path.join(export_path, default_name)

            self.logger.debug(
                "export match up report {}/{} {}/{}".format(label, sidechar, label2, sidechar2))

            # build a file name fo the xls file
            fname, ext = os.path.splitext(fname)
            formats = (["*.{}".format(file_format.lower()) for file_format in ["xls"]])
            fname = QFileDialog.getSaveFileName(self, "Pipe Fitter - Export Match Up Report",
                                                fname,
                                                "File ({})".format(" ".join(formats)))
            if fname:

                # store for later usage
                self.export_match_up_report = fname

                try:
                    self.fitter.create_match_up_report(self.export_match_up_report, label, sidechar,
                                                       label2, sidechar2)
                    self.updateStatus("Saved as %s" % self.export_match_up_report)
                    self.dirty = False
                except (IOError, ImportError):
                    filebase, ext = os.path.splitext(self.export_match_up_report)

                    message = "Failed to save the input file {}. ".format(
                        self.export_match_up_report)
                    self.logger.warning("{}".format(message))
                    QMessageBox.warning(self, "PipeFitter Export Error", message)

                    self.updateStatus("Failed to save %s" % self.export_match_up_report)

                # build a filename of the plot as well
                fname = os.path.splitext(self.export_match_up_report)[0]
                formats = (
                    ["*.{}".format(file_format.lower()) for file_format in ["png", "pdf", "jpg"]])
                fname = QFileDialog.getSaveFileName(self,
                                                    "Pipe Fitter - Export Match Up Plot",
                                                    fname,
                                                    "File ({})".format(" ".join(formats)))

                for i_tab in range(2):
                    filebase, ext = os.path.splitext(fname)
                    tab_name = re.sub("\s+", "_", self.tab_names[i_tab])
                    plotfile = "{}_{}{}".format(filebase, tab_name, ext)
                    self.logger.debug("Plotting to {}".format(plotfile))
                    self.show_message_in_status_bar("Writing plot {}".format(plotfile), timer=2000)
                    # the radius plot (polar for i_tab==0 and linear for i_tab == 1)
                    if i_tab == 0:
                        plot_difference = False
                        plot_hilo = False
                    else:
                        plot_difference = self.show_delta_r_plot_checkBox[i_tab].isChecked()
                        plot_hilo = self.show_hilo_plot_checkBox[i_tab].isChecked()

                    self.clear_lines(self.axis[i_tab], self.lines[i_tab])
                    self.plot_current_slice(i_tab, plot_profile_difference=plot_difference,
                                            plot_hilo_vs_phi=plot_hilo,
                                            filename=plotfile)

        else:
            message = "Please select a slice with a neighbour first."
            QMessageBox.warning(self, "PipeFitter Export Match up", message)

    def OpenAboutDialog(self):
        """
        Open about dialog
        """
        QMessageBox.about(self, "About PipeFitter",
                          """<b>PipeFitter %s</b> (Build tag %s)
                          <p>Copyright &copy; 2015-2018 HMC Heerema Marine Contractors.
                          All rights reserved.
                          <p>This application can be used to sort a list of Pipes based on the 
                          radial profiles generated by PipeSlicer.
                          <p>Python %s - Qt %s - PyQt %s on %s
                          <p>Author: Eelco van Vliet (evanvliet@hmc-heerema.com)""" % (
                              get_clean_version(__version__), __version__,
                              platform.python_version(),
                              QtCore.QT_VERSION_STR, QtCore.PYQT_VERSION_STR,
                              platform.system()))

    def updateStatus(self, message="Initialising..."):
        self.statusBar().showMessage(message, 5000)
        windows_title = "Pipe Fitter {}".format(get_clean_version(__version__))
        if self.filename is not None:
            windows_title += " - {}".format(os.path.basename(self.filename))

        windows_title += "[*]"
        self.setWindowTitle(windows_title)
        self.setWindowModified(self.dirty)

    def addRecentFile(self, fname):
        if fname is None:
            return

        if fname not in self.recentFiles:
            self.recentFiles = [fname] + self.recentFiles[:8]

    def updateFileMenu(self):
        self.fileMenu.clear()
        self.addActions(self.fileMenu, self.fileMenuActions[:-1])
        current = self.filename
        recentFiles = []
        for fname in self.recentFiles:
            if fname != current and QtCore.QFile.exists(fname):
                recentFiles.append(fname)
        if recentFiles:
            self.fileMenu.addSeparator()
            for i, fname in enumerate(recentFiles):
                action = QAction("&{} {}".format(i + 1, QtCore.QFileInfo(fname).fileName()),
                                 self)
                action.setIcon(QtGui.QIcon(":/pipe.png"))
                action.setData(fname)
                action.triggered.connect(self.load_file)
                self.fileMenu.addAction(action)
        self.fileMenu.addSeparator()
        self.fileMenu.addAction(self.fileMenuActions[-1])

    def stop_import_data(self):
        self.logger.debug("Stop Importing data Execution")
        self.stop_importing_data = True

    # @profile
    def import_data(self, process_file_batch=None):
        self.stop_importing_data = False
        self.logger.debug("Import data the batch")
        self.show_message_in_status_bar("Executing batch...", timer=5000)

        process_file_batch.ix[:, "Status"] = 0
        self.fitter.clear_data_frames()
        n_file_to_read = len(process_file_batch.index)
        if process_file_batch is not None:
            for i, current_file in enumerate(process_file_batch.index):
                if process_file_batch.ix[current_file, "Status"] >= 2:
                    # status 0 means that this file still needs to be imported
                    # status 1 should not happen because then we are currently importing
                    # status 2 means that the file has been imported already and should be skipped
                    # status 3 means that the file is to be skipped regardless if we have it already
                    # or not
                    continue

                if not self.stop_importing_data:
                    # extract the current file from the batch data frame

                    self.logger.info("Import file # {}: {}".format(i, current_file))

                    self.update_progress(i, n_file_to_read, "Reading file {} ({}/{})".format(
                        current_file, i, n_file_to_read))

                    # set the status of the current file to 1 (processing) and update the table
                    process_file_batch.ix[current_file, "Status"] = 1
                    if self.scanImportDlg is not None:
                        self.logger.debug("try to turn this row green {}".format(i))
                        self.scanImportDlg.updateTableStatus()
                        QApplication.processEvents()

                    frame_ext = process_file_batch.ix[current_file].slice_name
                    pipe_id = process_file_batch.ix[current_file].pipe_id
                    pipe_side_id = process_file_batch.ix[current_file].pipe_side_id

                    # import the STL file
                    self.logger.debug("Importing {} {}".format(current_file, frame_ext))
                    self.fitter.import_scan_file(current_file, pipe_id, pipe_side_id, frame_ext)

                    # set the status of the current file to 2 (done)
                    process_file_batch.ix[current_file, "Status"] = 2

                    # finish with one last table update
                    if self.scanImportDlg is not None:
                        self.logger.debug("Turning it green {}".format(current_file))
                        self.scanImportDlg.updateTableStatus()

                else:
                    break

            # update the number of pipes
            self.fitter.n_pipes = len(self.fitter.pipe_info_df.groupby(level=0))

            self.update_table()

            self.hide_progress_bar()

            # finish with one last table update
            if self.scanImportDlg is not None:
                self.scanImportDlg.updateTableStatus()

            self.sortPipesAction.setEnabled(True)

            # make sure that you can not overwrite anyfile when pressing Save
            self.dirty = True

            # resample the data
            # self.fitter.pipe_resample()
            #
            # self.replot_all()

            self.show_message_in_status_bar("Ready", timer=5000)
            self.logger.info("Done with the batch")

        # when leaving, reactivate the execute button
        if self.scanImportDlg is not None:
            self.scanImportDlg.buttonExecute.setEnabled(True)
            self.scanImportDlg.buttonScanDir.setEnabled(True)

    def stop_generate_data(self):
        self.logger.debug("Stop Generating data ")
        self.stop_generating_data = True

    # @profile
    def generate_data_profiles(self, generate_profiles_dataframe=None):
        self.stop_generating_data = False
        self.logger.debug("Generating slice data from statistics file ")
        self.show_message_in_status_bar("Executing batch...", timer=5000)
        # delete the previous data
        self.fitter.clear_data_frames()
        n_file_to_create = len(generate_profiles_dataframe.index)
        if generate_profiles_dataframe is not None:
            for i in generate_profiles_dataframe.index:

                if not self.stop_generating_data:

                    current_file = generate_profiles_dataframe.loc[i].PipeNo
                    self.logger.info("Create file # {}: {}".format(i, current_file))

                    self.update_progress(i, n_file_to_create, "Creating slice {} ({}/{})".format(
                        current_file, i + 1, n_file_to_create))

                    # set the status of the current file to 1 (processing) and update the table
                    generate_profiles_dataframe.loc[i, "Status"] = 1
                    if self.generateDataDlg is not None:
                        self.logger.debug("try to turn this row green {}".format(i))
                        self.generateDataDlg.update_row(i, current_file)
                        QApplication.processEvents()

                    # create a new slice based on the statistics values
                    self.fitter.create_slice_profile(generate_profiles_dataframe.loc[i])

                    # set the status of the current file to 2 (done)
                    generate_profiles_dataframe.ix[i, "Status"] = 2

                    # finish with one last table update
                    if self.generateDataDlg is not None:
                        self.logger.debug("Turning it green {}".format(current_file))
                        self.generateDataDlg.update_row(i, current_file)

                else:
                    break

        # update the number of pipes
        self.fitter.n_pipes = len(self.fitter.pipe_info_df.groupby(level=0))

        self.hide_progress_bar()
        self.logger.debug("done with with {} pipes".format(n_file_to_create))
        self.generateDataDlg.stop_execution_batch()
        self.update_table()


def _parse_the_command_line_arguments():
    """
    Parse the command line to set some options

    Returns
    -------
    tuple (args, parser)
        The parsed arguments are stored in the args bjec

    """

    parser = argparse.ArgumentParser(description='Start the PipeFitter Gui',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    # set the verbosity level command line arguments
    parser.add_argument('-d', '--debug', help="Print lots of debugging statements",
                        action="store_const", dest="log_level", const=logging.DEBUG,
                        default=logging.INFO)
    parser.add_argument('-v', '--verbose', help="Be verbose", action="store_const",
                        dest="log_level", const=logging.INFO)
    parser.add_argument('-q', '--quiet', help="Be quiet: no output", action="store_const",
                        dest="log_level", const=logging.WARNING)
    parser.add_argument('--log_file_debug', help="Print lots of debugging statements to file",
                        action="store_const", dest="log_level_file", const=logging.DEBUG,
                        default=logging.INFO)
    parser.add_argument('--log_file_verbose', help="Be verbose to file", action="store_const",
                        dest="log_level_file", const=logging.INFO)
    parser.add_argument('--log_file_quiet', help="Be quiet: no output to file",
                        action="store_const", dest="log_level_file", const=logging.WARNING)
    parser.add_argument("--version", help="Show the current version", action="store_true")
    parser.add_argument("--version_long", help="Show the full version info including git tag",
                        action="store_true")
    parser.add_argument("--full_revision_id", help="Show the full git sha1 revision id",
                        action="store_true")
    parser.add_argument("--write_log_to_file", action="store_true",
                        help="Write the logging information to file")
    parser.add_argument("--log_file_base", default="log", help="Default name of the logging output")
    parser.add_argument("--reset_settings", action="store_true",
                        help="Do not load the settings from a previous run but start with the "
                             "defaults")
    parser.add_argument("--width_window_initial", default=1100, type=int,
                        help="Initial width of the gui at start up")
    parser.add_argument("--height_window_initial", default=700, type=int,
                        help="Initial height of the gui at start up")

    # note that the following option are not parsed via the parser but directly extracted from
    # the sys.argv list. You have to write them out on the command line completely
    if set(sys.argv).intersection(["--version", "--version_long", "--full_revision_id"]):

        # if --version or versionlong is passed on the command line, show the version and exit
        if "--version" in sys.argv:
            # full version is not requested, so clean it
            version = get_clean_version(__version__)
        else:
            version = __version__
        print("version : {}".format(version))
        if "--full_revision_id" in sys.argv:
            print("git SHA        : {}".format(PACKAGE_INFO.git_sha))
            print("Python version : {}".format(PACKAGE_INFO.python_version))
            print("Build date     : {}".format(PACKAGE_INFO.build_date))
        sys.exit(0)

    # parse the command line
    args = parser.parse_args()

    return args, parser


def main():
    args, parser = _parse_the_command_line_arguments()

    if args.write_log_to_file:
        # http://stackoverflow.com/questions/29087297/
        # is-there-a-way-to-change-the-filemode-for-a-logger-object-that-is-not-configured
        log_file_base = args.log_file_base
        sys.stderr = open(log_file_base + ".err", 'w')
    else:
        log_file_base = None

    logger = create_logger(file_log_level=args.log_level_file,
                           console_log_level=args.log_level,
                           log_file=log_file_base)

    # get the application settings file name and read it
    settings_file = os.path.join(os.path.split(__file__)[0], os.path.split(BUILD_SETTINGS_FILE)[1])
    try:
        with open(settings_file, "r") as stream:
            settings = yaml.load(stream=stream, Loader=yamlordereddictloader.Loader)
    except FileNotFoundError:
        logger.info("Could not open properties file with the program info. Assuming defaults")
        settings = dict(general=dict())
        settings["general"]["organisation_name"] = "Heerema Marine Contractors"
        settings["general"]["organisation_domain"] = "hmc-heerema.com"
        settings["general"]["application_name"] = "PipeFitter"

    # Create a GL View widget to display data
    app = QApplication(sys.argv)
    app.setOrganizationName(settings["general"]["organisation_name"])
    app.setOrganizationDomain(settings["general"]["organisation_domain"])
    app.setApplicationName(settings["general"]["application_name"])
    app.setWindowIcon(QtGui.QIcon(":pipe-128.png"))
    win = PipeFitterMain(reset_settings=args.reset_settings)
    # win.setWindowTitle("Pipe Fitter {}".format(get_clean_version(__version__)))
    win.resize(args.width_window_initial, args.height_window_initial)
    win.show()

    app.exec_()


if __name__ == '__main__':
    main()
